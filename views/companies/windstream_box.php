        <!-- COMPANY BOX 5 --> 
        <div class="container clearfix" data-animate="fadeInUp">
    <div class="pricing-box pricing-extended bottommargin clearfix">
        <div class="pricing-desc">
            <div class="pricing-title">
                <?php
                $services = "";
                    if (isset($_SESSION["windstream_tv"])) {
                        $services .= " TV ";
                    }
                    if (isset($_SESSION["windstream_internet"])) {
                        if($services == "" ){ $services .= " INTERNET";} else {$services .= "+ INTERNET";}   
                    }
                    if (isset($_SESSION["windstream_phone"])) {
                        if($services == "" ){ $services .= " PHONE";} else {$services .= " + PHONE";}
                    }                    
                    
                    echo "<h3>WINDSTREAM ($services)*</h3>";
//                    session_destroy();
                ?>                
            </div>
            <div class="pricing-features">
                <img src="images/later/logos/wind.png" width="150" class="alignleft allmargin-sm">
                <!--<br>-->
                <ul class="iconlist-color clearfix center align-items-center">
                    <li><i class="icon-check"></i>Enjoy 4k picture resolution, the next big thing in visual technology</li>
                    <li><i class="icon-check"></i>Get access to 50,000 shows and movies on demand</li>
                    <!--<li><i class="icon-check"></i> For the Antic Sports Entertainment Channels</li>-->
                </ul>
            </div>
        </div>
        <div class="pricing-action-area">
            <div class="topmargin-sm">
                <br>
                <a href="tel:18889309001" class="button button-rounded button-reveal button-large button-red tright"><i class="icon-angle-right"></i><span>CONTINUE...</span></a>
                <!--<a href="#" class="button button-3d button-large btn-block">View Plans</a>-->
            </div>
        </div>
    </div>
</div>    