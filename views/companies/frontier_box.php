<!-- COMPANY BOX 2 -->
<div class="container clearfix" data-animate="fadeInUp">
    <div class="pricing-box pricing-extended bottommargin clearfix">
        <div class="pricing-desc">
            <div class="pricing-title">
                <?php
                $services = "";
                    if (isset($_SESSION["front_tv"])) {
                        $services .= " TV ";
                    }
                    if (isset($_SESSION["front_internet"])) {
                        if($services == "" ){ $services .= " INTERNET";} else {$services .= "+ INTERNET";}                        
                    }
                    if (isset($_SESSION["front_phone"])) {
                        if($services == "" ){ $services .= " PHONE";} else {$services .= " + PHONE";}
                    }                    
                    
                    echo "<h3>FRONTIER ($services)*</h3>";
                    
                ?>
                
            </div>
            <div class="pricing-features">
                <img src="images/later/logos/logo-frontier.svg" width="150" class="alignleft allmargin-sm">
                <br>
                <ul class="iconlist-color clearfix center">
                    <li><i class="icon-check"></i> A network that offers both cable and fiber optic connections.</li>
                    <li><i class="icon-check"></i> FiOS the extra fast cable-powered network.</li>
                </ul>
            </div>
        </div>
        <div class="pricing-action-area">
            <div class="topmargin-sm">
                <br>
                <a href="tel:1888930-9001" class="button button-rounded button-reveal button-large button-red tright"><i class="icon-angle-right"></i><span>CONTINUE...</span></a>
                <!--<a href="#" class="button button-3d button-large btn-block">View Plans</a>-->
            </div>
        </div>
    </div>
</div>   