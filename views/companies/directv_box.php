<!-- COMPANY BOX 1 -->                                            
<div class="container clearfix">
    <div class="pricing-box pricing-extended bottommargin clearfix" data-animate="fadeInUp">
        <div class="pricing-desc">
            <div class="pricing-title">
                <?php
                $services = "";
                    if (isset($_SESSION["direct_tv"])) {
                        $services .= " TV";
                    }
                    if (isset($_SESSION["direct_internet"])) {
                        if($services == "" ){ $services .= " INTERNET";} else {$services .= " + INTERNET";}   
                    }
                    if (isset($_SESSION["direct_phone"])) {
                        if($services == "" ){ $services .= " PHONE";} else {$services .= " + PHONE";}
                    }
                    
                    echo "<h3>DIRECTV ($services)*</h3>";
                    
//                    session_destroy();
                ?>
                
            </div>
            <div class="pricing-features">
                <img src="images/later/logos/logo-directv.svg" width="150" class="alignleft allmargin-sm">
                <!--<br>-->
                <ul class="iconlist-color clearfix center align-items-center">
                    <li><i class="icon-check"></i> Witness out-of-routine games via NFL Sunday Ticket.</li>
                    <li><i class="icon-check"></i> Save a significant amount coupled with AT&T wireless plans.</li>
                    <!--<li><i class="icon-check"></i> For the Antic Sports Entertainment Channels</li>-->
                </ul>
            </div>
        </div>
        <div class="pricing-action-area">
            <div class="topmargin-sm">
                <br>
                <a href="tel:18889309001" class="button button-rounded button-reveal button-large button-red tright"><i class="icon-angle-right"></i><span>CONTINUE...</span></a>
                <!--<a href="#" class="button button-3d button-large btn-block">View Plans</a>-->
            </div>
        </div>
    </div>
</div>     