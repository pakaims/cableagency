        <!-- COMPANY BOX 5 --> 
        <div class="container clearfix" data-animate="fadeInUp">
    <div class="pricing-box pricing-extended bottommargin clearfix">
        <div class="pricing-desc">
            <div class="pricing-title">
                <?php
                $services = "";
                    if (isset($_SESSION["centurylink_tv"])) {
                        $services .= " TV ";
                    }
                    if (isset($_SESSION["centurylink_internet"])) {
                        if($services == "" ){ $services .= " INTERNET";} else {$services .= "+ INTERNET";}   
                    }
                    if (isset($_SESSION["centurylink_phone"])) {
                        if($services == "" ){ $services .= " PHONE";} else {$services .= " + PHONE";}
                    }                    
                    
                    echo "<h3>CENTURYLINK ($services)*</h3>";
//                    session_destroy();
                ?>                
            </div>
            <div class="pricing-features">
                <img src="images/later/logos/century-link.png" width="150" class="alignleft allmargin-sm">
                <!--<br>-->
                <ul class="iconlist-color clearfix center align-items-center">
                    <li><i class="icon-check"></i>The reliability with CenturyLink factors into every package we create and put up on our website.</li>
                    <li><i class="icon-check"></i>We thrive on innovation where technology is a staple.</li>
                    <!--<li><i class="icon-check"></i> For the Antic Sports Entertainment Channels</li>-->
                </ul>
            </div>
        </div>
        <div class="pricing-action-area">
            <div class="topmargin-sm">
                <br>
                <a href="tel:18889309001" class="button button-rounded button-reveal button-large button-red tright"><i class="icon-angle-right"></i><span>CONTINUE...</span></a>
                <!--<a href="#" class="button button-3d button-large btn-block">View Plans</a>-->
            </div>
        </div>
    </div>
</div>    