        <!-- COMPANY BOX 4 --> 
<div class="container clearfix" data-animate="fadeInUp">
    <div class="pricing-box pricing-extended bottommargin clearfix">
        <div class="pricing-desc">
            <div class="pricing-title">
                <?php
                $services = "";
                   if (isset($_SESSION["viasat_satellite"])) {
                       $services .= " SATELLITE ";
                   }
                   if (isset($_SESSION["viasat_internet"])) {
                       if($services == "" ){ $services .= " INTERNET";} else {$services .= "+ INTERNET";}   
                   }
                //    if (isset($_SESSION["viasat_internet"])) {
                //        if($services == "" ){ $services .= " PHONE";} else {$services .= " + PHONE";}
                //    }                    
                    
                    echo "<h3>VIASAT ($services)*</h3>";
//                    session_destroy();
                ?>                
            </div>
            <div class="pricing-features">
                <img src="images/later/logos/viasat-logo.png" width="150" class="alignleft allmargin-sm">
                <!--<br>-->
                <ul class="iconlist-color clearfix center align-items-center">
                    <li><i class="icon-check"></i> In case you wish to talk to a representative directly, give us a call.</li>
                    <li><i class="icon-check"></i>your favorite digital TV and phone that is equipped with modern features.</li>
                    <!--<li><i class="icon-check"></i> For the Antic Sports Entertainment Channels</li>-->
                </ul>
            </div>
        </div>
        <div class="pricing-action-area">
            <div class="topmargin-sm">
                <br>
                <a href="tel:18889309001" class="button button-rounded button-reveal button-large button-red tright"><i class="icon-angle-right"></i><span>CONTINUE...</span></a>

                <!--<a href="#" class="button button-rounded button-reveal button-large button-red tright"><i class="icon-angle-right"></i><span>CONTINUE...</span></a>-->
                <!--<a href="#" class="button button-3d button-large btn-block">View Plans</a>-->
            </div>
        </div>
    </div>
</div>    