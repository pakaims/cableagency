<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-body">
            <div class="modal-content">
                <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Connection Request Form</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <h2>Please leave your contact details and a company representative will get back to you shortly.</h2>
                    <form action="mail.php" method="post" style="max-width: 35rem;">
                            <div class="form-group">
                                    <label for="namebox">Name</label>
                                    <input class="form-control" name="name" id="namebox" placeholder="Name" required>
                            </div>
                            <div class="form-group">
                                    <label for="emailbox">Email address</label>
                                    <input type="email" name="email" class="form-control" id="emailbox" placeholder="name@example.com" required>
                            </div>

                            <div class="form-group">
                                    <label for="numberbox">Number</label>
                                    <input type="tel" name="num" class="form-control" id="numberbox" placeholder="Contact Number" required>
                            </div>

                            <div class="form-group">
                                <label for="companybox">Company</label>
                                <select name="company" id="companybox" required>
                                  <option value="">Select</option>
                                  <option value="Frontier">Frontier</option>
                                  <option value="Charter">Charter</option>
                                  <option value="DIRECTV">Directv</option>
                                  <option value="VIASAT">VIASAT</option>
                                  <option value="Other">Other</option>
                                </select>
                            </div>                        
                        
                        <button type="submit" name="submit" class="btn btn-primary">Send</button>                        
                    </form>                    
                </div>
            </div>
        </div>
    </div>
</div>