		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">

			<div class="container">

				<!-- Footer Widgets
				============================================= -->
				<div class="footer-widgets-wrap pb-4 clearfix">

					<div class="row">
					<div class="col-md-4 col-sm-6 mt-5 mt-sm-0">

<div style="background: url('images/world-map.png') no-repeat left center; background-size: auto 100%;" class="footer_widgets___p">
<h2 style="margin-bottom:0px;">About Us</h2>
	<p>Cable Agency offers a multitude of options for TV, the Internet, and phone. A variety of quality channels are showcased in customer-friendly packages speak to the consumer directly about the stories they love. We are privileged to have service providers like Spectrum, DIRECTV, and AT&T on board. </p>
</div>

</div>

						<div class="col-md-3 col-sm-6 mb-0 mb-sm-4 mb-md-0">

							<div class="widget clearfix">
								<h2 style="margin-bottom:0px;">Contact Us</h2>
								<!-- <p>Everything About Us is About You.</p> -->

								<div>
									<!-- <address>
										<strong>Headquarters:</strong><br>
										795 Folsom Ave, Suite 600<br>
										San Francisco, CA 94107<br>
									</address> -->
									<abbr title="Phone Number"><strong style="font-size:22px;">Phone: <br> </strong></abbr> (888) 930-9001<br>
									<abbr title="Email Address"><strong style="font-size:22px;" >Email: <br> </strong></abbr> info@CableAgency.com
								</div>

							</div>

						</div>

						<!-- <div class="col-md-2 col-sm-6 mt-5 mt-sm-0">



						<div class="widget clearfix">

<h4>Social</h4>

<ul class="list-unstyled iconlist">
	<li><i class="icon-facebook"></i><a href="#" target="_blank"> Facebook</a></li>
	<li><i class="icon-twitter"></i><a href="#" target="_blank"> Twitter</a></li>
	<li><i class="icon-instagram"></i><a href="#" target="_blank"> Instagram</a></li>
	<li><i class="icon-youtube"></i><a href="#" target="_blank"> YouTube</a></li>
	<li><i class="icon-xing"></i><a href="#"> Xing</a></li>
	<li><i class="icon-github"></i><a href="#"> Github</a></li>
</ul>

</div>
							<div class="widget clearfix">

								<h4>Features</h4>

								<ul class="list-unstyled iconlist ml-0">
									<li><a href="#">Documentation</a></li>
									<li><a href="#">Feedback</a></li>
									<li><a href="#">Plugins</a></li>
									<li><a href="#">Support Forums</a></li>
									<li><a href="#">Themes</a></li>
									<li><a href="#">WordPress Blog</a></li>
									<li><a href="#">WordPress Planet</a></li>
								</ul>

							</div>

						</div> -->
						<div class="col-md-4 col-sm-6 mt-4 mt-sm-0">
							<h2 style="margin-bottom:0px; ">Find Zip Code</h2>
						<form action="include/zip-processor.php" method="get" class="topmargin-lg landing-wide-form-footer  dark nobottommargin clearfix">
						<!-- <div class="heading-block nobottommargin nobottomborder center align-items-center">
							<h2>Search for Cable TV and Internet Providers in Your Area</h2>
							<span>Enter your zip code to view providers available in your area.</span>
						</div> -->
						<div class="col_full">
                  <input type="number" name="zipcode" maxlength="5" pattern="[0-9]{5}" required="true" class="form-control required form-control-lg not-dark input-number–noSpinners" value="" placeholder="Zip Code*">
                                                        <!--<input type="text" name="zipcode">-->
						</div>
						<div class="margin_________top15 col_full nobottommargin">
							<button class="btn btn-lg btn-danger btn-block nomargin" value="submit" type="submit" style="">FIND</button>
                                                        <!--<button type="submit">submit</button>-->
						</div>
					</form>

					<!-- <div class="text-center">
	<a href="#" class="social____link_____padding"><i class="icon-facebook"></i></a>
	<a href="#" class="social____link_____padding"><i class="icon-twitter"></i> </a>
	<a href="#" class="social____link_____padding"><i class="icon-instagram"></i> </a>
	<a href="#" class="social____link_____padding"> <i class="icon-youtube"></i></a>
	<li><i class="icon-xing"></i><a href="#"> Xing</a></li>
	<li><i class="icon-github"></i><a href="#"> Github</a></li>
					</div> -->


<!-- 
							<div class="widget clearfix">

								<h4>Support</h4>

								<ul class="list-unstyled iconlist ml-0">
									<li><a href="#">Help Center</a></li>
									<li><a href="#">Paid with Mollie</a></li>
									<li><a href="#">Status</a></li>
									<li><a href="#">Changelog</a></li>
									<li><a href="#">Contact Support</a></li>
								</ul>

							</div> -->

						</div>

					</div>

				</div><!-- .footer-widgets-wrap end -->

				<div class="line line-sm m-0"></div>

			</div>


			<!-- Copyrights
			============================================= -->
			<div id="copyrights" class="nobg">

				<div class="container clearfix">

					<div class="col_half">
						Copyrights &copy; 2020 All Rights Reserved by CableAgency.com Inc.<br>
						<!-- <div class="copyright-links"><a href="termncondition.php">Terms of Use</a> / <a href="privacypolicy.php">Privacy Policy</a></div> -->
					</div>

					<div class="col_half col_last tright">
						<div class="copyrights-menu copyright-links clearfix">
							<a href="index.php">HOME</a>/<a href="best-providers.php">PROVIDERS</a>/<a href="about-us.php">ABOUT US</a>
						</div>
					</div>
				</div>
			</div><!-- #copyrights end -->
		</footer><!-- #footer end -->
	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script src="js/jquery.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/jquery.hotspot.js"></script>
	<script src="js/components/rangeslider.min.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="js/functions.js"></script>
	<script>
		jQuery(document).ready( function() {
			var pricingCPU = 1,
				pricingRAM = 1,
				pricingStorage = 10,
				elementCPU = $(".range-slider-cpu"),
				elementRAM = $(".range-slider-ram"),
				elementStorage = $(".range-slider-storage");

			elementCPU.ionRangeSlider({
				grid: false,
				values: [1,2,4,6,8],
				postfix: ' Core',
				onStart: function(data){
					pricingCPU = data.from_value;
				}
			});

			elementCPU.on( 'change', function(){
				pricingCPU = $(this).prop('value');
				calculatePrice( pricingCPU, pricingRAM, pricingStorage );
			});

			elementRAM.ionRangeSlider({
				grid: false,
				step: 1,
				min: 1,
				from:1,
				max: 32,
				postfix: ' GB',
				onStart: function(data){
					pricingRAM = data.from;
					console.log(data);
				}
			});

			elementRAM.on( 'onStart change', function(){
				pricingRAM = $(this).prop('value');
				calculatePrice( pricingCPU, pricingRAM, pricingStorage );
			});

			elementStorage.ionRangeSlider({
				grid: false,
				step: 10,
				min: 10,
				max: 100,
				postfix: ' GB',
				onStart: function(data){
					pricingStorage = data.from;
				}
			});

			elementStorage.on( 'change', function(){
				pricingStorage = $(this).prop('value');
				calculatePrice( pricingCPU, pricingRAM, pricingStorage );
			});

			calculatePrice( pricingCPU, pricingRAM, pricingStorage );

			function calculatePrice( cpu, ram, storage ) {
				var pricingValue = ( Number(cpu) * 10 ) + ( Number(ram) * 8 ) + ( Number(storage) * 0.5 );
				jQuery('.cpu-value').html(pricingCPU);
				jQuery('.ram-value').html(pricingRAM);
				jQuery('.storage-value').html(pricingStorage);
				jQuery('.cpu-price').html('$'+pricingCPU * 10);
				jQuery('.ram-price').html('$'+pricingRAM * 8);
				jQuery('.storage-price').html('$'+pricingStorage * 0.5);
				jQuery('.pricing-price').html( '$'+pricingValue );
			}
		});

		jQuery(window).on( 'load', function(){
			$('#hotspot-img').hotSpot();
		});
	</script>
        
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144827880-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-144827880-1');
</script>
        
        
</body>
</html>