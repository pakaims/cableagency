<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,400i,700|Istok+Web:400,700" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />

	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="css/components/ion.rangeslider.css" type="text/css" />

	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<meta name='viewport' content='initial-scale=1, viewport-fit=cover'>

	<!-- Hosting Demo Specific Stylesheet -->
	<link rel="stylesheet" href="css/colors.php?color=44aaac" type="text/css" /> <!-- Theme Color -->
	<link rel="stylesheet" href="demos/hosting/css/fonts.css" type="text/css" />
	<link rel="stylesheet" href="demos/hosting/hosting.css" type="text/css" />
        
        
	<!-- / -->

	<!-- Document Title
	============================================= -->
        <title>Cable | Terms &amp; Conditions</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar
		============================================= -->
<!--		<div id="top-bar" class="center dark" style="background-color: #15888a">
			<p class="mb-0 text-white" style="font-size: 14px;">Holisticly cultivate multifunctional quality vectors after Mobile SDK.<a href="#" class="ml-2 font-primary t700 text-white"><u>Learn More</u> &#8250;</a></p>
		</div>-->

		<!-- Header
		============================================= -->
		<header id="header">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="index.php" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="images/logo.png" alt="Canvas Logo"></a>
						<a href="index.php" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="images/logo@2x.png" alt="Canvas Logo"></a>
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu" class="sub-title">

						<ul>
							<li><a href="index.php"><div>Home</div><span>Lets Start</span></a></li>
							<li><a href="best-providers.php"><div>Providers</div><span>Awesome deals</span></a></li>
                                                        <li><a href="about-us.php"><div>About us</div><span>what we do</span></a></li>
						</ul>

					</nav><!-- #primary-menu end -->

				</div>

			</div>

		</header>
                

		<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-parallax page-title-dark" style="padding: 250px 0; background-image: url('images/later/terms.jpg'); background-size: cover; background-position: center center;" data-bottom-top="background-position:0px 400px;" data-top-bottom="background-position:0px -500px;">

			<div class="container clearfix">
                            <h1><strong>Terms & Conditions</strong></h1>
                            <h3 class="text-white">For a transparent and a non-eventful visit</h3>
<!--				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Pages</a></li>
					<li class="breadcrumb-item active" aria-current="page">About Us</li>
				</ol>-->
			</div>
                    
		</section><!-- #page-title end -->


                
		<!-- Content
		============================================= -->
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            
            <div class="heading-block center">
                <h3>Terms &amp; Conditions</h3>
                <!--<p align="justify">-->
                This page contains all the information related to use of this site. If you’re not willing to accept all the terms and conditions, we may not be able to help you. If you have any other agreement with CableAgency.com, the new contract will replace the former one. We as a sole authority may change the content of this page for better customer support without any offense to any party.  So visit our page on a regular basis. 
            <!--</p>-->
            </div>
            <div id="side-navigation" class="tabs customjs">
                <div class="col_one_third nobottommargin">
                    <ul class="sidenav">
                        <li><a href="#snav-content1"><i class="icon-copy"></i>Copyright Policy<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content2"><i class="icon-tumblr"></i>Trademark Policy<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content3"><i class="icon-user4"></i>User Submission Policy<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content4"><i class="icon-newspaper"></i>Recommendation Policy<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content5"><i class="icon-file-broken"></i>Disclaimer of Damages<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content6"><i class="icon-line-command"></i>Limitation of Liability<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content7"><i class="icon-line-link"></i>Site Linking Policy<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content8"><i class="icon-line-loader"></i>Indemnity Policy<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content9"><i class="icon-line-paper"></i>Privacy Policy<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content10"><i class="icon-newspaper2"></i>Dispute Resolution Policy<i class="icon-chevron-right"></i></a></li>
                    </ul>
                </div>

                <div class="col_two_third col_last nobottommargin">
                    
                    <div id="snav-content1">
                        <h3>Copyright Policy</h3>
                        <p align="justify">
                            Under U.S law commission, this site’s content that is, images, text, and graphics are protected copyrights.
                            Whatever the material is, you’re not allowed to use the content for business purposes. However, you can view and download a single copy of the site content with a restriction to use it solely for non-commercial personal use.
                            Under the law, you are abiding to not modify or distribute content. Otherwise, CableAgency.com has all intellectual rights for this site and retains to act against any person, misusing our information.
                        </p>
                    </div>

                    <div id="snav-content2">
                        <h3>Trademark Policy</h3>
                        <p align="justify">
                        CableAgency.com owns the license to all the marks and logos displayed on this site, otherwise the noted trademarks. Any unauthorized person, using trademarks against the policy is strictly prohibited. We also fair use the trademarks owned by third parties and hold no claim for them.
                        </p>
                    </div>

                    <div id="snav-content3">
                        <h3>User Submission Policy</h3>
                        <p align="justify">                        
                        We are always up for your suggestions and review on your site. However, for all the information you send to us in the form of reviews, comments, ideas, or suggestions, we hold irreversible and perpetual rights to manipulate that information. Manipulation of information means to use, delete, or publish them as required without any recompense to the information provider. Nevertheless, CableAgency.com is not responsible for any review or comment by the user on this site.
                        CableAgency.com encourages its users to post information as per the following guidelines:
                        </p>
                    <div class="acc_content clearfix">
                        <ul class="iconlist iconlist-color nobottommargin">   
                            <li><i class="icon-plus-sign"></i>Don’t transmit copyrighted material on this site unless you hold rights for it or have permission to do so.</li>
                            <li><i class="icon-plus-sign"></i>Stay true to your identity and don’t be an impersonator and misguide us or other users via our platform.</li>
                            <li><i class="icon-plus-sign"></i>Don’t share trade secrets on this site unless you have the permission to do so or hold copyrights for it.</li>
                            <li><i class="icon-plus-sign"></i>Don’t send material that infringes any intellectual property, public, or private rights of others.</li>
                            <li><i class="icon-plus-sign"></i>Don’t send any advertisement or solicitation of business.</li>
                            <li><i class="icon-plus-sign"></i>Don’t send harassing, obscene, threatening, embarrassing, abusive, or hateful material to any user.</li>
                            <li><i class="icon-plus-sign"></i>Don’t try to hijack the site’s security.</li>
                            <li><i class="icon-plus-sign"></i>Don’t violate any federal, local, or law intentionally or unintentionally.</li>
                        </ul>
                    </div>
                        <p align="justify">
                        If you misconduct any of the above-mentioned terms & conditions, we have rights to block you from this site. Further, you’ll not be able to use the site material with the immediate cancellation of any rights for this site and will be abided to destroy all the site content.
                        </p>
                    </div>

                    <div id="snav-content4">
                        <h3>Recommendation Policy</h3>
                        <p align="justify">
                        CableAgency.com is an online forum to recommend its users with the best and the most reliable TV-service providers nationwide. We provide TV service ranking as per their quality, performance, cost-effectivity, and special offers and only for information purpose. 
                        The information is purely analytical based on subject matter. We recommend our users, despite the service ranking on our site, do your own research as well and be sure of the service.CableAgency.com is not responsible for any kind of error or omission of any information on this site afterward.
                        We don’t guarantee TV-service and the factual accuracy of the ranking. As per the federal law, we update information (charges) on weekly basis. We use innovative techniques for our analysis and evaluation process. We work as a private organization, however; we receive a commission from the ranked TV-service providers.
                        The provided information is purely for general educational purpose and should not be treated as legal expert advice for consultation to any professional for personal needs. We are certain of our information to be true from reliable sources but don’t guarantee the accuracy and completeness of the prices, taxes and service description.
                        In case of any complaint, you can direct your query to the authorized dealer or salesperson of the service. Any information on this site can be changed without notice and CableAgency.com is not responsible for your any action based on the information available on this site.
                        </p>
                    </div>

                    <div id="snav-content5">
                        <h3>Disclaimer of Damages</h3>
                        <p align="justify">
                            CableAgency.com and its stakeholders including affiliates can’t be liable for any damage directly or indirectly with reference to the use or inability to use information on our site or the linked sites, based on the warranty, contract, or other legal theories.
                        </p>
                    </div>
                        
                    <div id="snav-content6">
                        <h3>Limitation of Liability</h3>
                        <p align="justify">
                            CableAgency.com is not protected from computer viruses and can operate error fully. If you come across with any inconvenience regarding any matter as the expense endured due to the replacement of any information, we’ll not be responsible for any compensation.
                        </p>
                    </div>

                    <div id="snav-content7">
                        <h3>Site Linking Policy</h3>
                        <p align="justify">
CableAgency.com contains links of third-party sites. The linked sites are merely for your convenience and not endorsements.  We reserve our right as a private informational site and not as their representative for the accuracy of information.  You’ll consult the linked sites on your risk.
However, we are a member of Amazon Services LLC Associates Program, an affiliation-advertising program to provide a chance of increasing revenue via Amazon.com and other related sites.
                        </p>
                    </div>

                    <div id="snav-content8">
                        <h3>Indemnity Policy</h3>
                        <p align="justify">
                            For the usage of this site, you agree to protect CableAgency.com and its partners against any damage, claims, losses, and expenses for your misuse of information or by breaching our terms & conditions. Nevertheless, we reserve the right to be a part of any legal inquiry or settlement on your behalf at your expense.
                        </p>
                    </div>

                    <div id="snav-content9">
                        <h3>Privacy Policy</h3>
                        <p align="justify">
                            Review CableAgency.com privacy policy page, Privacy Policy.
                        </p>
                    </div>


                    <div id="snav-content10">
                        <h3>Dispute Resolution Policy</h3>
                        <p align="justify">
Without any conflict with the law principles, all terms and conditions are governed by the state of Utah. Invalidity of any provision by the competent jurisdiction will not affect the remaining provisions of the terms & conditions.
With respect to the use of this site, these terms and conditions constitute the contract between CableAgency.com and the user without any waiver of any policy rule. 
In case of dispute, this agreement binds CableAgency.com and you to settle the issue outside the court without any trial via binding arbitration. By the full extent permitted by law, you’ll withdraw from the right of participating as a class member, class arbitration or consolidation of individual arbitrations any class against CableAgency.com.
                        </p>
                    </div>
                  
                </div>

            </div>

        </div>

    </div>

</section><!-- #content end -->                


<!-- PROMOBOX START -->                
<div class="section clear-bottommargin-sm" style="background-color: #44AAAC">
    <div class="container">
        <div class="row center justify-content-center">
                <div class="col-md-auto">
                        <h1 class="t700 text-white mb-3">Agreement’s got it all!</h1>
                        <!--<h4 class="text-white">We provide you with plenty of information about the epic TV-services, So that, you make an educated choice.</h4>-->
                </div>
<!--            <div class="subscribe-widget clear-bottommargin align-items-center center">
                <div class="widget-subscribe-form-result"></div>
                <form id="zipform2" action="include/subscribe.php" method="get">
                    <input class="input-number–noSpinners form-control-lg float-left" type="text" name="zipcode" placeholder="Enter Zip Code">
                    <a style="margin-top:1px;" href="javascript:{}" onclick="document.getElementById('zipform2').submit();" class="button button-rounded button-reveal button-large button-dirtygreen"><i class="icon-map-marker2"></i><span>Find Area</span></a>
                </form>
            </div>-->

        </div>
    </div>
</div>
<!-- PROMOBOX END   --> 


<!-- Footer
============================================= -->
<?php require_once 'views/footer.php'; ?>

<script>
        $(function() {
                $( "#side-navigation" ).tabs({ show: { effect: "fade", duration: 400 } });
        });
</script>