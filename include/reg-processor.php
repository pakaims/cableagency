<?php require_once("functions.php"); ?>
<?php

unset($_SESSION["already_email"]);

 if ( isset( $_POST['register-form-submit'] ) ) {
     $name       = $_POST['register-form-name'];
     $email      = $_POST['register-form-email'];
     $password   = $_POST['register-form-password'];
     $repassword = $_POST['register-form-repassword'];

     if(strcmp($password,$repassword)==0) {
         echo "<script type='text/javascript'>alert('Password is OK!');</script>";
         $email_flag = find_email($email);
         if ($email_flag == 0) {
            register_new_user($name,$email,$password);
            echo "<script type='text/javascript'>alert('User successfully registered!');</script>";
            redirect_to("../login.php");
         }
         else
         {  
             
            $_SESSION[already_email] = 1; 
            echo "<script type='text/javascript'>alert('Email already exist please try another!');</script>";
            redirect_to("../register.php");
         }

     }
      else{echo "<script type='text/javascript'>alert('Password does not match!');</script>";redirect_to("../register.php");}
  }
//  // This is probably a GET request


?>