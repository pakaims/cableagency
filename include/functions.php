<?php include 'db_connection.php'; ?>
<?php session_start(); ?>

<?php

	function redirect_to($new_location) {
	  header("Location: " . $new_location);
	  exit;
	}

	function mysql_prep($string) {
		global $connection;
		
		$escaped_string = mysqli_real_escape_string($connection, $string);
		return $escaped_string;
	}
	
	function confirm_query($result_set) {
		if (!$result_set) {
			die("Database query failed.");
		}
	}

	function form_errors($errors=array()) {
		$output = "";
		if (!empty($errors)) {
		  $output .= "<div class=\"error\">";
		  $output .= "Please fix the following errors:";
		  $output .= "<ul>";
		  foreach ($errors as $key => $error) {
		    $output .= "<li>";
				$output .= htmlentities($error);
				$output .= "</li>";
		  }
		  $output .= "</ul>";
		  $output .= "</div>";
		}
		return $output;
	}
	



	
	function find_admin_by_id($admin_id) {
		global $connection;
		
		$safe_admin_id = mysqli_real_escape_string($connection, $admin_id);
		
		$query  = "SELECT * ";
		$query .= "FROM user ";
		$query .= "WHERE id = {$safe_admin_id} ";
		$query .= "LIMIT 1";
		$admin_set = mysqli_query($connection, $query);
		confirm_query($admin_set);
		if($admin = mysqli_fetch_assoc($admin_set)) {
			return $admin;
		} else {
			return null;
		}
	}

	function find_admin_by_username($username) {
		global $connection;
		
//		$safe_username = mysqli_real_escape_string($connection, $username);
		
		$query  = "SELECT * ";
		$query .= "FROM user ";
		$query .= "WHERE email = '{$username}' ";
		$query .= "LIMIT 1";
		$admin_set = mysqli_query($connection, $query);
		confirm_query($admin_set);
		if($admin = mysqli_fetch_assoc($admin_set)) {
			return $admin;
		} else {
			return null;
		}
	}
	


	function password_encrypt($password) {
  	$hash_format = "$2y$10$";   // Tells PHP to use Blowfish with a "cost" of 10
	  $salt_length = 22; 					// Blowfish salts should be 22-characters or more
	  $salt = generate_salt($salt_length);
	  $format_and_salt = $hash_format . $salt;
	  $hash = crypt($password, $format_and_salt);
		return $hash;
	}
	
	function generate_salt($length) {
	  // Not 100% unique, not 100% random, but good enough for a salt
	  // MD5 returns 32 characters
	  $unique_random_string = md5(uniqid(mt_rand(), true));
	  
		// Valid characters for a salt are [a-zA-Z0-9./]
	  $base64_string = base64_encode($unique_random_string);
	  
		// But not '+' which is valid in base64 encoding
	  $modified_base64_string = str_replace('+', '.', $base64_string);
	  
		// Truncate string to the correct length
	  $salt = substr($modified_base64_string, 0, $length);
	  
		return $salt;
	}
	
	function password_check($password, $existing) {
		// existing hash contains format and salt at start
//	  $hash = crypt($password, $existing_hash);
	  if ($password === $existing) {
	    return true;
	  } else {
	    return false;
	  }
	}

	function attempt_login($username, $password) {
		$admin = find_admin_by_username($username);
		if ($admin) {
			// found admin, now check password
			if (password_check($password, $admin["password"])) {
				// password matches
				return $admin;
			} else {
				// password does not match
				return false;
			}
		} else {
			// admin not found
			return false;
		}
	}

	function logged_in() {
		return isset($_SESSION['admin_id']);
	}
	
	function confirm_logged_in() {
		if (!logged_in()) {
			redirect_to("login.php");
		}
	}


/////////////////////////////FETCHING FRONTIER ZIP//////////////////////////////     
        function find_front_zip($front_zip) {
            $flag1 = find_front_t_zip($front_zip);
            $flag2 = find_front_i_zip($front_zip);
            $flag3 = find_front_p_zip($front_zip);
            
    if ($flag1 == 0 && $flag2 == 0 && $flag3 == 0) {
        return 0;
    } else {
        return 1;
    }
	}          
        
////////////////////////////////////////////////////////////////////////////////        
/////////////////////////////FRONTIER FUNCTIONS///////////////////////////////////
       
         function find_front_t_zip($front_zip) {
		global $connection;
		$safe_front_zip = mysqli_real_escape_string($connection, $front_zip);
		$query  = "SELECT zip ";
		$query .= "FROM frontier_tv ";
		$query .= "WHERE zip = {$safe_front_zip} ";
		$query .= "LIMIT 1";
		$front_zip_set = mysqli_query($connection, $query);
		confirm_query($front_zip_set);
                $flag = 0;
                unset($_SESSION['front_tv']);
		if($front_flag = mysqli_fetch_assoc($front_zip_set))
                {   $_SESSION["front_tv"] = 1;
                    $flag = 1;
                } 
                return $flag;
//                mysqli_close($connection);                
	}          
        

         function find_front_i_zip($front_zip) {
		global $connection;
		$safe_front_zip = mysqli_real_escape_string($connection, $front_zip);
		$query  = "SELECT zip ";
		$query .= "FROM frontier_internet ";
		$query .= "WHERE zip = {$safe_front_zip} ";
		$query .= "LIMIT 1";
		$front_zip_set = mysqli_query($connection, $query);
		confirm_query($front_zip_set);
                $flag = 0;
                unset($_SESSION['front_internet']);
		if($front_flag = mysqli_fetch_assoc($front_zip_set))
                {   $_SESSION["front_internet"] = 1;
                    $flag = 1;
                } 
                return $flag;
//                mysqli_close($connection);                
	}          


         function find_front_p_zip($front_zip) {
		global $connection;
		$safe_front_zip = mysqli_real_escape_string($connection, $front_zip);
		$query  = "SELECT zip ";
		$query .= "FROM frontier_phone ";
		$query .= "WHERE zip = {$safe_front_zip} ";
		$query .= "LIMIT 1";
		$front_zip_set = mysqli_query($connection, $query);
		confirm_query($front_zip_set);
                $flag = 0;
                unset($_SESSION['front_phone']);
		if($front_flag = mysqli_fetch_assoc($front_zip_set))
                {   $_SESSION["front_phone"] = 1;
                    $flag = 1;
                } 
                return $flag;
//                mysqli_close($connection);                
	}




////////////////////////////////////////////////////////////////////////////////  
/////////////////////////////viasat FUNCTIONS///////////////////////////////////
function find_via_zip($via_zip) {
	$flag1 = find_via_t_zip($via_zip);
    $flag2 = find_via_i_zip($via_zip);
	// $flag3 = find_spec_p_zip($spec_zip);

	if ($flag1 == 0 && $flag2 == 0) {
		return 0;
	} else {
		return 1;
	}
}    

function find_via_t_zip($via_zip) {
	global $connection;
	$safe_via_zip = mysqli_real_escape_string($connection, $via_zip);
	$query  = "SELECT zip ";
	$query .= "FROM viasat_satellite ";
	$query .= "WHERE zip = {$safe_via_zip} ";
	$query .= "LIMIT 1";
	$via_zip_set = mysqli_query($connection, $query);
	confirm_query($via_zip_set);
			$flag = 0;
			unset($_SESSION["viasat_satellite"]);
			if($via_flag = mysqli_fetch_assoc($via_zip_set)) 
			{   $_SESSION["viasat_satellite"] = 1; 
				$flag = 1;
			} 
			return $flag;
//                mysqli_close($connection);
}

function find_via_i_zip($via_zip) {
	global $connection;
	$safe_via_zip = mysqli_real_escape_string($connection, $via_zip);
	$query  = "SELECT zip ";
	$query .= "FROM viasat_internet ";
	$query .= "WHERE zip = {$safe_via_zip} ";
	$query .= "LIMIT 1";
	$via_zip_set = mysqli_query($connection, $query);
	confirm_query($via_zip_set);
			$flag = 0;
			unset($_SESSION["viasat_internet"]);
			if($via_flag = mysqli_fetch_assoc($via_zip_set)) 
			{   $_SESSION["viasat_internet"] = 1; 
				$flag = 1;
			} 
			return $flag;
//                mysqli_close($connection);
}


////////////////////////////////////////////////////////////////////////////////

/////////////////////////////SPECTRUM FUNCTIONS///////////////////////////////////
        function find_spec_zip($spec_zip) {
            $flag1 = find_spec_t_zip($spec_zip);
            $flag2 = find_spec_i_zip($spec_zip);
            $flag3 = find_spec_p_zip($spec_zip);

            if ($flag1 == 0 && $flag2 == 0 && $flag3 == 0) {
                return 0;
            } else {
                return 1;
            }
	}    
        
        
        
        function find_spec_t_zip($spec_zip) {
		global $connection;
		$safe_spec_zip = mysqli_real_escape_string($connection, $spec_zip);
		$query  = "SELECT zip ";
		$query .= "FROM spectrum_tv ";
		$query .= "WHERE zip = {$safe_spec_zip} ";
		$query .= "LIMIT 1";
		$spec_zip_set = mysqli_query($connection, $query);
		confirm_query($spec_zip_set);
                $flag = 0;
                unset($_SESSION["spectrum_tv"]);
                if($spec_flag = mysqli_fetch_assoc($spec_zip_set)) 
                {   $_SESSION["spectrum_tv"] = 1; 
                    $flag = 1;
                } 
                return $flag;
//                mysqli_close($connection);
	}




        function find_spec_i_zip($spec_zip) {
		global $connection;
		$safe_spec_zip = mysqli_real_escape_string($connection, $spec_zip);
		$query  = "SELECT zip ";
		$query .= "FROM spectrum_internet ";
		$query .= "WHERE zip = {$safe_spec_zip} ";
		$query .= " LIMIT 1";
		$spec_zip_set = mysqli_query($connection, $query);
		confirm_query($spec_zip_set);
                $flag = 0;
                unset($_SESSION["spectrum_internet"]);
                if($spec_flag = mysqli_fetch_assoc($spec_zip_set)) 
                {   $_SESSION["spectrum_internet"] = 1; 
                    $flag = 1;    } 
                return $flag;
//                mysqli_close($connection);
	}



        function find_spec_p_zip($spec_zip) {
		global $connection;
		$safe_spec_zip = mysqli_real_escape_string($connection, $spec_zip);
		$query  = "SELECT zip ";
		$query .= "FROM spectrum_phone ";
		$query .= "WHERE zip = {$safe_spec_zip} ";
		$query .= "LIMIT 1";
		$spec_zip_set = mysqli_query($connection, $query);
		confirm_query($spec_zip_set);
                $flag = 0;
                unset($_SESSION["spectrum_phone"]);
                if($spec_flag = mysqli_fetch_assoc($spec_zip_set)) 
                {   $_SESSION["spectrum_phone"] = 1; 
                    $flag = 1;
                } 
                return $flag;
//                mysqli_close($connection);
	}







//////////////////////////////////////////////////////////////////////////////// 

/////////////////////////////WINDSTREAM FUNCTIONS///////////////////////////////////
function find_wind_zip($wind_zip) {
	$flag1 = find_wind_t_zip($wind_zip);
	$flag2 = find_wind_i_zip($wind_zip);
	$flag3 = find_wind_p_zip($wind_zip);

	if ($flag1 == 0 && $flag2 == 0 && $flag3 == 0) {
		return 0;
	} else {
		return 1;
	}
}    



function find_wind_t_zip($wind_zip) {
global $connection;
$safe_wind_zip = mysqli_real_escape_string($connection, $wind_zip);
$query  = "SELECT zip ";
$query .= "FROM windstream_tv ";
$query .= "WHERE zip = {$safe_wind_zip} ";
$query .= "LIMIT 1";
$wind_zip_set = mysqli_query($connection, $query);
confirm_query($wind_zip_set);
		$flag = 0;
		unset($_SESSION["windstream_tv"]);
		if($wind_flag = mysqli_fetch_assoc($wind_zip_set)) 
		{   $_SESSION["windstream_tv"] = 1; 
			$flag = 1;
		} 
		return $flag;
//                mysqli_close($connection);
}




function find_wind_i_zip($wind_zip) {
	global $connection;
	$safe_wind_zip = mysqli_real_escape_string($connection, $wind_zip);
	$query  = "SELECT zip ";
	$query .= "FROM windstream_internet ";
	$query .= "WHERE zip = {$safe_wind_zip} ";
	$query .= "LIMIT 1";
	$wind_zip_set = mysqli_query($connection, $query);
	confirm_query($wind_zip_set);
			$flag = 0;
			unset($_SESSION["windstream_internet"]);
			if($wind_flag = mysqli_fetch_assoc($wind_zip_set)) 
			{   $_SESSION["windstream_internet"] = 1; 
				$flag = 1;
			} 
			return $flag;
	//                mysqli_close($connection);
	}



	function find_wind_p_zip($wind_zip) {
		global $connection;
		$safe_wind_zip = mysqli_real_escape_string($connection, $wind_zip);
		$query  = "SELECT zip ";
		$query .= "FROM windstream_phone ";
		$query .= "WHERE zip = {$safe_wind_zip} ";
		$query .= "LIMIT 1";
		$wind_zip_set = mysqli_query($connection, $query);
		confirm_query($wind_zip_set);
				$flag = 0;
				unset($_SESSION["windstream_phone"]);
				if($wind_flag = mysqli_fetch_assoc($wind_zip_set)) 
				{   $_SESSION["windstream_phone"] = 1; 
					$flag = 1;
				} 
				return $flag;
		//                mysqli_close($connection);
		}






////////////////////////////////////////////////////////////////////////////////

/////////////////////////////WINDSTREAM FUNCTIONS///////////////////////////////////
function find_link_zip($link_zip) {
	$flag1 = find_link_t_zip($link_zip);
	$flag2 = find_link_i_zip($link_zip);
	$flag3 = find_link_p_zip($link_zip);

	if ($flag1 == 0 && $flag2 == 0 && $flag3 == 0) {
		return 0;
	} else {
		return 1;
	}
}    



function find_link_t_zip($link_zip) {
global $connection;
$safe_link_zip = mysqli_real_escape_string($connection, $link_zip);
$query  = "SELECT zip ";
$query .= "FROM centurylink_tv ";
$query .= "WHERE zip = {$safe_link_zip} ";
$query .= "LIMIT 1";
$link_zip_set = mysqli_query($connection, $query);
confirm_query($link_zip_set);
		$flag = 0;
		unset($_SESSION["centurylink_tv"]);
		if($link_flag = mysqli_fetch_assoc($link_zip_set)) 
		{   $_SESSION["centurylink_tv"] = 1; 
			$flag = 1;
		} 
		return $flag;
//                mysqli_close($connection);
}




function find_link_i_zip($link_zip) {
	global $connection;
	$safe_link_zip = mysqli_real_escape_string($connection, $link_zip);
	$query  = "SELECT zip ";
	$query .= "FROM centurylink_internet ";
	$query .= "WHERE zip = {$safe_link_zip} ";
	$query .= "LIMIT 1";
	$link_zip_set = mysqli_query($connection, $query);
	confirm_query($link_zip_set);
			$flag = 0;
			unset($_SESSION["centurylink_internet"]);
			if($link_flag = mysqli_fetch_assoc($link_zip_set)) 
			{   $_SESSION["centurylink_internet"] = 1; 
				$flag = 1;
			} 
			return $flag;
	//                mysqli_close($connection);
	}



	function find_link_p_zip($link_zip) {
		global $connection;
		$safe_link_zip = mysqli_real_escape_string($connection, $link_zip);
		$query  = "SELECT zip ";
		$query .= "FROM centurylink_phone ";
		$query .= "WHERE zip = {$safe_link_zip} ";
		$query .= "LIMIT 1";
		$link_zip_set = mysqli_query($connection, $query);
		confirm_query($link_zip_set);
				$flag = 0;
				unset($_SESSION["centurylink_phone"]);
				if($link_flag = mysqli_fetch_assoc($link_zip_set)) 
				{   $_SESSION["centurylink_phone"] = 1; 
					$flag = 1;
				} 
				return $flag;
		//                mysqli_close($connection);
		}






////////////////////////////////////////////////////////////////////////////////
        
/////////////////////////////DIRECTV FUNCTIONS///////////////////////////////////
        function find_direct_zip($direct_zip) {
            $flag1 = find_direct_t_zip($direct_zip);
            $flag2 = find_direct_i_zip($direct_zip);
            $flag3 = find_direct_p_zip($direct_zip);

            if ($flag1 == 0 && $flag2 == 0 && $flag3 == 0) {
                return 0;
            } else {
                return 1;
            }
	}    
        
        
        
        function find_direct_t_zip($spec_zip) {
		global $connection;
		$safe_spec_zip = mysqli_real_escape_string($connection, $spec_zip);
		$query  = "SELECT zip ";
		$query .= "FROM direct_tv ";
		$query .= "WHERE zip = {$safe_spec_zip} ";
		$query .= "LIMIT 1";
		$spec_zip_set = mysqli_query($connection, $query);
		confirm_query($spec_zip_set);
                $flag = 0;
                unset($_SESSION["direct_tv"]);
                if($spec_flag = mysqli_fetch_assoc($spec_zip_set)) 
                {   $_SESSION["direct_tv"] = 1; 
                    $flag = 1;    } 
                return $flag;
//                mysqli_close($connection);
	}




        function find_direct_i_zip($spec_zip) {
		global $connection;
		$safe_spec_zip = mysqli_real_escape_string($connection, $spec_zip);
		$query  = "SELECT zip ";
		$query .= "FROM direct_internet ";
		$query .= "WHERE zip = {$safe_spec_zip} ";
		$query .= "LIMIT 1";
		$spec_zip_set = mysqli_query($connection, $query);
		confirm_query($spec_zip_set);
                $flag = 0;
                unset($_SESSION["direct_internet"]);
                if($spec_flag = mysqli_fetch_assoc($spec_zip_set)) 
                {   $_SESSION["direct_internet"] = 1; 
                    $flag = 1;    } 
                return $flag;
//                mysqli_close($connection);
	}



        function find_direct_p_zip($spec_zip) {
		global $connection;
		$safe_spec_zip = mysqli_real_escape_string($connection, $spec_zip);
		$query  = "SELECT zip ";
		$query .= "FROM direct_phone ";
		$query .= "WHERE zip = {$safe_spec_zip} ";
		$query .= "LIMIT 1";
		$spec_zip_set = mysqli_query($connection, $query);
		confirm_query($spec_zip_set);
                $flag = 0;
                unset($_SESSION["direct_phone"]);
                if($spec_flag = mysqli_fetch_assoc($spec_zip_set)) 
                {   $_SESSION["direct_phone"] = 1; 
                    $flag = 1;
                } 
                return $flag;
//                mysqli_close($connection);
	}
        
        
////////////////////////////////////////////////////////////////////////////////        
/////////////////////////////DATATABLES FUNCTIONS///////////////////////////////////
////////////////////////////////////////////////////////////////////////////////        
        
        function find_zip_for_datatable($tabquery){
		global $connection;
		$query   = "SELECT * FROM ".$tabquery;
		$zip_set = mysqli_query($connection, $query);
		confirm_query($zip_set);
		return $zip_set;
	}
        
        
        function delete_zip_from_datatable($zipcode){
		global $connection;
                $current_table = $_SESSION['thistable'];
		$query   = "DELETE FROM ".$current_table." WHERE zip=".$zipcode;
		$del_zip = mysqli_query($connection, $query);
//		confirm_query($zip_set);
//		return $zip_set;
                if ($del_zip) {
                    redirect_to("../database-table.php");
            }
	}
        
        
        function find_email_password($email,$pass)
        {
              global $connection;      
              $myusername = mysqli_real_escape_string($connection,$_POST['login-form-email']);
              $mypassword = mysqli_real_escape_string($connection,$_POST['login-form-password']); 

              $sql = "SELECT * FROM user_login WHERE email = '$myusername' and password = '$mypassword'"." LIMIT 1";
              $result = mysqli_query($connection,$sql);
              $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
        //      $active = $row['email'];

              $count = mysqli_num_rows($result);

              // If result matched $myusername and $mypassword, table row must be 1 row

              if($count == 1) {
        //         session_register("myusername");
                 $_SESSION['login_email'] = $myusername;
                 $_SESSION['login_name'] = $row['name'];
                 return TRUE;
              }
              return FALSE;            
        }
        
        
        function not_found(){
                echo "<script type='text/javascript'>alert('not found called!');</script>";
            $_SESSION["login_flag"] = 1;
            redirect_to("../login.php");              
        }
        

        function find_email($email)
        {
              global $connection;      
              $safe_email = mysqli_real_escape_string($connection,$email);

              $sql = "SELECT email FROM user_login WHERE email = '$safe_email' LIMIT 1";
              $result = mysqli_query($connection,$sql);
              $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
              $count = mysqli_num_rows($result);

              if($count == 1) {
                 return 1;
              }
              
              return 0;            
        }


        function register_new_user($name,$email,$password){
            global $connection;
            
            $sql = "INSERT INTO user_login(name, email , password) VALUES ( '$name' , '$email' , '$password' )";
            
            if (mysqli_query($connection,$sql)) {
                echo "New record created successfully";
            } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($connection);
        }
            }        
?>