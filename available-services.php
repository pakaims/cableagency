<?php require_once 'include/functions.php'; ?>
<?php 
if (!isset($_SESSION["servicepage"])) {
    redirect_to("index.php");
}
  ?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,400i,700|Istok+Web:400,700" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
	<link rel="stylesheet" href="css/components/ion.rangeslider.css" type="text/css" />
	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<meta name='viewport' content='initial-scale=1, viewport-fit=cover'>

	<!-- Hosting Demo Specific Stylesheet -->
	<link rel="stylesheet" href="css/colors.php?color=44aaac" type="text/css" /> <!-- Theme Color -->
	<link rel="stylesheet" href="demos/hosting/css/fonts.css" type="text/css" />
	<link rel="stylesheet" href="demos/hosting/hosting.css" type="text/css" />
        
        
	<!-- / -->
<?php
//        $di = "zip-processor.php";
//        redirect_to($di);
?>
	<!-- Document Title
	============================================= -->
        <title>Available Services in <?php if (isset($_GET['zipcode'])) { echo $_GET['zipcode'];}  ?></title>

</head>

<body class="stretched">
<?php require_once 'views/modalbox.php'; ?>
	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">
	<header>
        <div class="menu-toggle" id="hamburger">
            <i class="fas fa-bars"></i>
        </div>
        <div class="overlay"></div>
        <div class="container">
            <nav>
			<div id="logo">
						<a href="index.php" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="images/logo.png" alt="Canvas Logo"></a>
						<a href="index.php" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="images/logo@2x.png" alt="Canvas Logo"></a>
	</div>
                <ul>
                    <li class="current"><a href="index.php">Home</a></li>
                    <li><a href="best-providers.php">Providers</a></li>
					<li><a href="about-us.php">About Us</a></li>
					<div style="padding-left:25px;" ><a href="tel:18889309001"> (888) 930-9001</a></div>
                </ul>
            </nav>
        </div>
</header>
	<script>var open = document.getElementById('hamburger');
var changeIcon = true;

open.addEventListener("click", function(){

    var overlay = document.querySelector('.overlay');
    var nav = document.querySelector('nav');
    var icon = document.querySelector('.menu-toggle i');

    overlay.classList.toggle("menu-open");
    nav.classList.toggle("menu-open");

    if (changeIcon) {
        icon.classList.remove("fa-bars");
        icon.classList.add("fa-times");

        changeIcon = false;
    }
    else {
        icon.classList.remove("fa-times");
        icon.classList.add("fa-bars");
        changeIcon = true;
    }
});</script>
		<!-- Top Bar
		============================================= -->
<!--		<div id="top-bar" class="center dark" style="background-color: #15888a">
			<p class="mb-0 text-white" style="font-size: 14px;">Holisticly cultivate multifunctional quality vectors after Mobile SDK.<a href="#" class="ml-2 font-primary t700 text-white"><u>Learn More</u> &#8250;</a></p>
		</div>-->

		<!-- Header
		============================================= -->
		<!-- <header id="header">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div> -->

					<!-- Logo
					============================================= -->
					<!-- <div id="logo">
						<a href="index.html" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="images/logo.png" alt="Canvas Logo"></a>
						<a href="index.html" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="images/logo@2x.png" alt="Canvas Logo"></a>
					</div> -->
					<!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<!-- <nav id="primary-menu" class="sub-title">

						<ul>
							<li><a href="index.php"><div>Home</div><span>Lets Start</span></a></li>
							<li><a href="best-providers.php"><div>Providers</div><span>Awesome deals</span></a></li>
                                                        <li><a href="about-us.php"><div>About us</div><span>what we do</span></a></li>
						</ul> -->

						<!-- Top Cart
						============================================= -->
<!--						<div id="top-cart">
							<a href="#" id="top-cart-trigger"><i class="icon-shopping-cart"></i><span>5</span></a>
							<div class="top-cart-content">
								<div class="top-cart-title">
									<h4>Shopping Cart</h4>
								</div>
								<div class="top-cart-items">
									<div class="top-cart-item clearfix">
										<div class="top-cart-item-image">
											<a href="#"><img src="images/shop/small/1.jpg" alt="Blue Round-Neck Tshirt" /></a>
										</div>
										<div class="top-cart-item-desc">
											<a href="#">Blue Round-Neck Tshirt</a>
											<span class="top-cart-item-price">$19.99</span>
											<span class="top-cart-item-quantity">x 2</span>
										</div>
									</div>
									<div class="top-cart-item clearfix">
										<div class="top-cart-item-image">
											<a href="#"><img src="images/shop/small/6.jpg" alt="Light Blue Denim Dress" /></a>
										</div>
										<div class="top-cart-item-desc">
											<a href="#">Light Blue Denim Dress</a>
											<span class="top-cart-item-price">$24.99</span>
											<span class="top-cart-item-quantity">x 3</span>
										</div>
									</div>
								</div>
								<div class="top-cart-action clearfix">
									<span class="fleft top-checkout-price">$114.95</span>
									<button class="button button-3d button-small nomargin fright">View Cart</button>
								</div>
							</div>
						</div> #top-cart end -->

						<!-- Top Search
						============================================= -->
<!--						<div id="top-search">
							<a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
							<form action="search.html" method="get">
								<input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter..">
							</form>
						</div> #top-search end -->

					<!-- </nav> -->
					<!-- #primary-menu end -->

				<!-- </div>

			</div>

		</header> -->
                <!-- #header end -->

<!--		 Page Title
		============================================= 
		<section id="page-title">

			<div class="container clearfix">
				<h1>Services</h1>
				<span>We provide Amazing Solutions</span>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Pages</a></li>
					<li class="breadcrumb-item active" aria-current="page">Services</li>
				</ol>
			</div>

		</section> #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

<!--				<div class="container clearfix">

					<div class="col_one_third nobottommargin">
						<div class="feature-box media-box">
							<div class="fbox-media">
								<img src="images/services/1.jpg" alt="Why choose Us?">
							</div>
							<div class="fbox-desc">
								<h3>Why choose Us.<span class="subtitle">Because we are Reliable.</span></h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi rem, facilis nobis voluptatum est voluptatem accusamus molestiae eaque perspiciatis mollitia.</p>
							</div>
						</div>
					</div>

					<div class="col_one_third nobottommargin">
						<div class="feature-box media-box">
							<div class="fbox-media">
								<img src="images/services/2.jpg" alt="Why choose Us?">
							</div>
							<div class="fbox-desc">
								<h3>Our Mission.<span class="subtitle">To Redefine your Brand.</span></h3>
								<p>Quos, non, esse eligendi ab accusantium voluptatem. Maxime eligendi beatae, atque tempora ullam. Vitae delectus quia, consequuntur rerum molestias quo.</p>
							</div>
						</div>
					</div>

					<div class="col_one_third nobottommargin col_last">
						<div class="feature-box media-box">
							<div class="fbox-media">
								<img src="images/services/3.jpg" alt="Why choose Us?">
							</div>
							<div class="fbox-desc">
								<h3>What we Do.<span class="subtitle">Make our Customers Happy.</span></h3>
								<p>Porro repellat vero sapiente amet vitae quibusdam necessitatibus consectetur, labore totam. Accusamus perspiciatis asperiores labore esse ab accusantium ea modi ut.</p>
							</div>
						</div>
					</div>

				</div>-->
<div class="clear clearfix clear-bottommargin-lg"></div>
<div class="clear clearfix clear-bottommargin-sm"></div>
<div class="clear clearfix clear-bottommargin-sm"></div>
<div class="section clear-bottommargin-lg">
     <div class="container clearfix">

        
        <div class="heading-block center">
            <h1>The Best Cable TV and Internet Deals In <?php if (isset($_GET['zipcode'])) { echo $_GET['zipcode']; } ?> </h1>
                <span>We will be your guide to your destination network for everyday TV, Internet, and phone needs</span>
        </div>

                                       
<!--//////////////////////////COMPANY BOXES////////////////////////////////////////////////-->
<?php        
        $spectrum  = $_SESSION["spectrum"];
        $directv   = $_SESSION["directv"];
        $frontier  = $_SESSION["frontier"];
		$viasat	   = $_SESSION["viasat"];
		$windstream	   = $_SESSION["windstream"];
		$centurylink	   = $_SESSION["centurylink"];
		
		if ($spectrum == 1)
		{require_once"views/companies/spectrum_box.php";}
		if ($directv  == 1)
		{require_once"views/companies/directv_box.php";}
		if ($frontier == 1)
		{require_once"views/companies/frontier_box.php";}
		 if ($viasat == 1)
		{require_once"views/companies/viasat_box.php";}
		if ($windstream == 1)
		{require_once"views/companies/windstream_box.php";}
		if ($centurylink == 1)
		{require_once"views/companies/centurylink_box.php";}
        
//        session_unset();
//        session_destroy(); 
?>
<!--///////////////////////////////////////////////////////////////////////////////////////-->

                                


        <div class=" center clear-bottommargin">
                <span>Our Recommended TV service providers weigh heavy on customer satisfaction with attractive deals to satisfy your daily entertainment dose. We rate these providers for popularity and innovation in technology with ample knowledge. Trust our years of thorough research and choose only the best for your homes. </span>
        </div>
        <br>
        <br>
        
    </div> 
    
</div>

<!-- PROMOBOX 1 START -->
<div class="section clear-bottommargin-lg" style="background-color: #AEBC4A">
    <div class="row center justify-content-center">
        <div class="col-md-auto">
                <h2 class="t700 text-white mb-3">Be at ease with a great deal at CableAgency.com regardless of your monthly earnings.</h2>
        </div>
    </div>
</div>
<!-- PROMOBOX 1 END   -->

<br>
<br>
<br>
<section id="content">
        <div class="content-wrap nopadding">
                <div class="container clearfix"> 
                <div class="nobottommargin topmargin-lg">

        <div class=" center bottommargin-sm">
                <h2>Experience Value for Money Entertainment by the Best Brands in Town</h2>
<!--                <span>Everybody deserves a full entertainment package via HD TV service at home, waiting areas, or lounges. Nevertheless, what’s worse when you have a so-called best television connection and still can’t enjoy unlimited channel streaming with advanced features, and a comfortable agreement? We got you covered and bring the best of the best TV service providers with specialties and ratings to limit your search with CableAgency.com </span>-->
        </div>                    

<!-- COMPANY DETAIL 1 -->                    
<div class="col_one_third">
    <br>
    <img class="alignright img-responsive" src="images/later/logos/logo-directv.svg" alt="NO image">	
</div>
<div class="col_two_third  col_last">
    <div class="team-title"><h3>1. DIRECTV</h3><span>Sports channels that never let you miss any event</span></div>
	Are you a sports lover and don’t want to miss any game? DIRECTV is just what you need! With multiple premium channels and an exclusive sports package, get ready to indulge in an entertainment haven. In addition, you get prized channels to give a balance to your system.     <br>
    <br>
    <a href="#" class="button button-rounded button-reveal button-large button-red tright"><i class="icon-angle-right"></i><span>VIEW PLANS</span></a>
</div>                 
<div class="divider divider-short divider-border divider-center"><i class="icon-like"></i></div>
<div class="clear"></div>


<!-- COMPANY DETAIL 2 -->                    
<div class="col_one_third">
    <br>
    <img class="alignright img-responsive" src="images/later/logos/logo-frontier.svg" alt="NO image">	
</div>
<div class="col_two_third  col_last">
    <div class="team-title"><h3>2. FRONTIER </h3><span>Less Worry of Contract Strings – More Room To Be Yourself</span></div>
Gone are the days when we used to set antennas to enjoy our favorite primetime TV shows. Frontier FiOS (fiber optic service) brings everything to your doorstep that includes a fuss-free TV connection and the fastest Internet. It is the start of your journey towards a magnificent destination.         <br>
        <br>
        <a href="#" class="button button-rounded button-reveal button-large button-red tright"><i class="icon-angle-right"></i><span>VIEW PLANS</span></a>
</div>                 
<div class="divider divider-short divider-border divider-center"><i class="icon-like"></i></div>
<div class="clear"></div>

<!-- COMPANY DETAIL 3 -->                    
<div class="col_one_third">
    <br>
    <img class="alignright img-responsive" src="images/later/logos/logo-spectrum.svg" alt="NO image">	
</div>
<div class="col_two_third  col_last">
<div class="team-title"><h3>3. SPECTRUM</h3><span>Triple Play Gold - Premium Channels in a Cost-Effective Bundle</span></div>
Charter Spectrum is one of the largest cable TV providers in the US. They have a motto to deliver affordable TV and fulfill the viewing needs of every age group. You get the freedom to switch to premium quality channels with an on-demand option allowing access to 10,000+ movies, shows and what not.         <br>
        <br>
        <a href="#" class="button button-rounded button-reveal button-large button-red tright"><i class="icon-angle-right"></i><span>VIEW PLANS</span></a>
</div>                  
<div class="divider divider-short divider-border divider-center"><i class="icon-like"></i></div>
<div class="clear"></div>

<!-- COMPANY DETAIL 4 -->                    
<div class="col_one_third">
    <br>
    <img class="alignright img-responsive" src="images/later/logos/century.png" alt="NO image">	
</div>
<div class="col_two_third  col_last">
    <div class="team-title"><h3>4. CENTURYLINK </h3><span>Internet unlike any other on planet earth</span></div>
	CenturyLink aims to improve digital lines of communication across the country. In doing so, it is continually growing its Fiber network that is capable of delivering speeds of up to 940 Mbps. It is going after a revolution in the history of the Internet.
       <br>
        <a href="#" class="button button-rounded button-reveal button-large button-red tright"><i class="icon-angle-right"></i><span>VIEW PLANS</span></a>
</div>                 
<div class="divider divider-short divider-border divider-center"><i class="icon-like"></i></div>
<div class="clear"></div>

<!-- COMPANY DETAIL 5 -->                    
<div class="col_one_third">
    <br>
    <img class="alignright img-responsive" src="images/later/logos/wind-PKG.png" alt="NO image">	
</div>
<div class="col_two_third  col_last">
    <div class="team-title"><h3>5. WINDSTREAM </h3><span>Future of telecommunications and visual excellence</span></div>
	From out-of-the-box package selection to seamless customer services, Windstream Communications focuses on HD TV channels, fast Internet speeds and distortion-less connectivity on the phone for its customers. Connect with us to connect with the future.
        <br>
        <a href="#" class="button button-rounded button-reveal button-large button-red tright"><i class="icon-angle-right"></i><span>VIEW PLANS</span></a>
</div>                 
<div class="divider divider-short divider-border divider-center"><i class="icon-like"></i></div>
<div class="clear"></div>

<!-- COMPANY DETAIL 6 -->                    
<div class="col_one_third">
    <br>
    <img class="alignright img-responsive" src="images/later/logos/viasat-logo.png" alt="NO image">	
</div>
<div class="col_two_third  col_last clear-bottommargin-sm">
    <div class="team-title"><h3>6. VIASAT </h3><span>HD channels, two-way speedy data, and voice communication</span></div>
	Viasat, a global communications company with a strong belief that anything, anywhere in the world can be connected. To fulfill this purpose, they have come up with fast Internet connectivity; channels with crisp resolution; and voice that seems to be near your heart.
        <br>
        <br>
        <a href="#" class="button button-rounded button-reveal button-large button-red tright"><i class="icon-angle-right"></i><span>VIEW PLANS</span></a>
</div> 

                
<!--<div class="divider divider-short divider-border divider-center"><i class="icon-like"></i></div>-->
<!--<div class="clear"></div>-->



<!-- ATTRIBUTES SECTION 2 -->
<!--<div class="col_full">

        <div class="heading-block center nobottomborder">
                <h2>What Should Customers Look in the Perfect TV Service Provider? </h2>
                <span>The reliability and availability of services vary, depending upon the service type. Therefore, customers should closely into the pros and cons of each type.</span>
        </div>
</div>-->


<!--<div class="col_one_sixth center align-items-center">
    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
    width="180" height="180"
    viewBox="0 0 224 224"
    style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g><g id="surface1"><path d="M200.66667,112c0,48.96354 -39.70312,88.66667 -88.66667,88.66667c-48.96354,0 -88.66667,-39.70312 -88.66667,-88.66667c0,-48.96354 39.70313,-88.66667 88.66667,-88.66667c48.96354,0 88.66667,39.70313 88.66667,88.66667" fill="#dd8903"></path><path d="M179.13802,54.19531c13.38021,15.53125 21.52864,35.69271 21.52864,57.80469c0,48.96354 -39.70312,88.66667 -88.66667,88.66667c-22.11198,0 -42.27344,-8.14844 -57.80469,-21.52864c16.26042,18.86719 40.26823,30.86198 67.13802,30.86198c48.96354,0 88.66667,-39.70312 88.66667,-88.66667c0,-26.86979 -11.99479,-50.87761 -30.86198,-67.13802z" fill="#ffab00"></path><path d="M191.33333,102.66667c0,48.96354 -39.70312,88.66667 -88.66667,88.66667c-48.96354,0 -88.66667,-39.70312 -88.66667,-88.66667c0,-48.96354 39.70313,-88.66667 88.66667,-88.66667c48.96354,0 88.66667,39.70313 88.66667,88.66667" fill="#ffab00"></path><path d="M102.66667,32.66667c-38.66406,0 -70,31.33594 -70,70c0,38.66406 31.33594,70 70,70c38.66406,0 70,-31.33594 70,-70c0,-38.66406 -31.33594,-70 -70,-70zM162.47656,92.45833l-9.20573,1.54948c-0.47396,-2.77083 -1.16667,-5.52343 -2.09636,-8.20312l8.80469,-3.0625c1.11198,3.15364 1.93229,6.43489 2.49739,9.71614zM155.91406,73.59114l-8.18489,4.48438c-1.34896,-2.47917 -2.91667,-4.86719 -4.66667,-7.07292l7.34636,-5.76042c2.05989,2.60677 3.90104,5.41406 5.50521,8.34896zM107.47917,42.18229l-0.74739,9.3151c-2.80729,-0.23698 -5.66927,-0.20052 -8.47656,0.01823l-0.76563,-9.29688c3.29948,-0.27344 6.70833,-0.29167 9.98958,-0.03646zM87.59114,43.87761l2.31511,9.05989c-2.73437,0.69271 -5.41406,1.62239 -8.00261,2.77083l-3.77343,-8.53125c3.04427,-1.36719 6.23437,-2.46094 9.46093,-3.29948zM69.34375,51.97136l5.14063,7.78386c-2.36979,1.54948 -4.59375,3.31771 -6.67188,5.23177l-6.34375,-6.85417c2.44271,-2.26042 5.10417,-4.33854 7.875,-6.16146zM54.6875,65.51563l7.40104,5.70573c-1.75,2.26042 -3.29948,4.64844 -4.63021,7.12761l-8.22136,-4.44792c1.56771,-2.89844 3.40886,-5.72396 5.45052,-8.38542zM45.22656,83.10677l8.84115,3.00781c-0.91146,2.67969 -1.60417,5.43229 -2.0599,8.20313l-9.20573,-1.49479c0.52864,-3.28125 1.34896,-6.5625 2.42448,-9.71614zM42,102.79427l9.33333,-0.03646c0.01823,2.84375 0.25521,5.6875 0.71094,8.47657l-9.20573,1.53125c-0.54688,-3.26302 -0.82032,-6.63542 -0.83854,-9.97136zM49.34636,131.63281c-1.60417,-2.93489 -2.95313,-6.03385 -4.04688,-9.16927l8.82292,-3.02604c0.92969,2.64323 2.0599,5.25 3.40886,7.72917zM61.65104,147.34636c-2.47917,-2.26042 -4.75782,-4.73958 -6.79948,-7.34636l7.34636,-5.74219c1.73177,2.20573 3.66406,4.30208 5.76042,6.23438zM78.34896,158.26563c-3.04427,-1.33073 -6.03386,-2.95313 -8.80469,-4.75782l5.10417,-7.82031c2.35156,1.54948 4.86719,2.89844 7.45573,4.02864zM97.72656,163.13281c-3.33593,-0.27343 -6.65364,-0.82031 -9.88021,-1.62239l2.27864,-9.04167c2.71614,0.67448 5.52344,1.13021 8.34896,1.36719zM98,135.33333v-48.01562l-14,4.41146v-11.66667l26.70573,-10.0625h1.29427v65.33333zM107.73438,163.13281l-0.78386,-9.29687c2.82552,-0.23698 5.63281,-0.71094 8.36719,-1.40364l2.27864,9.04167c-3.20833,0.82031 -6.54427,1.38542 -9.86198,1.65885zM115.09896,52.84636l2.26042,-9.05989c3.22656,0.82031 6.43489,1.89583 9.49739,3.22656l-3.73698,8.54948c-2.58854,-1.13021 -5.26823,-2.02344 -8.02083,-2.71614zM127.09375,158.21094l-3.77344,-8.53125c2.58854,-1.14844 5.08594,-2.51563 7.4375,-4.04688l5.1224,7.80208c-2.77083,1.82292 -5.72396,3.42708 -8.78646,4.77604zM130.59375,59.59114l5.08594,-7.82031c2.78906,1.80469 5.45052,3.86458 7.91146,6.125l-6.28906,6.89063c-2.07812,-1.91407 -4.33854,-3.66407 -6.70833,-5.19532zM143.77344,147.29167l-6.32552,-6.87239c2.09636,-1.91406 4.01042,-4.01042 5.74219,-6.23438l7.36458,5.74219c-2.04167,2.60677 -4.32031,5.08594 -6.78125,7.36458zM156.04167,131.54167l-8.20312,-4.44792c1.34896,-2.49739 2.49739,-5.10417 3.39062,-7.76562l8.84114,3.02604c-1.07552,3.15364 -2.44271,6.23438 -4.02864,9.1875zM153.30729,111.125c0.45573,-2.78906 0.69271,-5.63281 0.69271,-8.45833l7.94792,-0.20052l1.36719,0.01823c0.01823,3.51823 -0.25521,6.87239 -0.80208,10.15364z" fill="#ffcc80"></path></g></g></g></svg>
</div>
<div class="col_five_sixth col_last">
    <h4>Clear Prices by the Providers</h4>
    <p align="justify">
        Many service providers charge an installation or equipment fee before the actual running of the service. The charges can reach up to $100.00. However, if you use the bundle option, you can get discounted offers.
        It is clearly a burden on the customer. $100 is not a small amount when you have to pay in bulk for a subscription. Whichever provider offers flexible prices in terms of equipment, installation charges and customer support, it vouches in favor of the customers, entirely.
        For those who are not accustomed to such amenities are at a loss because discounts and giveaways have a greater impact than we can imagine! If a company is not willing to share the load of a subscription, it doesn’t sit right with the customer.
    </p>
</div>-->

<!--<div class="col_one_sixth col_last center align-items-center">
    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
    width="180" height="180"
    viewBox="0 0 224 224"
    style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g id="Layer_1"><g id="surface1"><path d="M51.33333,158.66667c-2.8,0 -4.66667,-2.33333 -4.66667,-4.66667v-84c0,-2.8 1.86667,-4.66667 4.66667,-4.66667c2.8,0 4.66667,1.86667 4.66667,4.66667v84c0,2.33333 -1.86667,4.66667 -4.66667,4.66667z" fill="#455a64"></path><path d="M79.33333,158.66667c-2.8,0 -4.66667,-2.33333 -4.66667,-4.66667v-84c0,-2.8 2.33333,-4.66667 4.66667,-4.66667c2.33333,0 4.66667,1.86667 4.66667,4.66667v84c0,2.33333 -2.33333,4.66667 -4.66667,4.66667z" fill="#455a64"></path><path d="M182,200.66667h-140c-7.93333,0 -14,-6.06667 -14,-14v-23.33333c0,-7.93333 6.06667,-14 14,-14h140c7.93333,0 14,6.06667 14,14v23.33333c0,7.93333 -6.06667,14 -14,14z" fill="#78909c"></path><path d="M60.66667,175c0,3.73333 -3.26667,7 -7,7c-3.73333,0 -7,-3.26667 -7,-7c0,-3.73333 3.26667,-7 7,-7c3.73333,0 7,3.26667 7,7z" fill="#64dd17"></path><path d="M84,175c0,3.73333 -3.26667,7 -7,7c-3.73333,0 -7,-3.26667 -7,-7c0,-3.73333 3.26667,-7 7,-7c3.73333,0 7,3.26667 7,7z" fill="#64dd17"></path><path d="M107.33333,175c0,3.73333 -3.26667,7 -7,7c-3.73333,0 -7,-3.26667 -7,-7c0,-3.73333 3.26667,-7 7,-7c3.73333,0 7,3.26667 7,7z" fill="#ecf0f1"></path><path d="M130.66667,175c0,3.73333 -3.26667,7 -7,7c-3.73333,0 -7,-3.26667 -7,-7c0,-3.73333 3.26667,-7 7,-7c3.73333,0 7,3.26667 7,7z" fill="#ecf0f1"></path><path d="M127.86667,109.2c-1.4,0 -2.8,-0.46667 -3.73333,-1.4c-1.86667,-1.86667 -1.4,-4.66667 0.46667,-6.53333c9.8,-8.4 15.4,-20.06667 15.4,-33.13333c0,-12.6 -5.6,-25.2 -15.4,-33.13333c-1.86667,-1.86667 -2.33333,-4.66667 -0.46667,-6.53333c1.86667,-1.86667 4.66667,-2.33333 6.53333,-0.46667c11.66667,9.33333 18.66667,24.26667 18.66667,39.66667c0,15.4 -6.53333,30.33333 -18.2,40.6c-0.93333,0.46667 -2.33333,0.93333 -3.26667,0.93333z" fill="#3498db"></path><path d="M114.8,96.13333c-1.4,0 -2.8,-0.46667 -3.73333,-1.86667c-1.4,-1.86667 -1.4,-5.13333 0.93333,-6.53333c6.06667,-4.66667 9.33333,-12.13333 9.33333,-20.06667c0,-7.93333 -3.26667,-15.4 -9.33333,-20.06667c-1.86667,-1.4 -2.33333,-4.66667 -0.93333,-6.53333c1.4,-1.86667 4.66667,-2.33333 6.53333,-0.93333c8.4,6.53333 13.06667,16.33333 13.06667,27.53333c0,10.73333 -4.66667,21 -13.06667,27.53333c-0.93333,0.46667 -1.86667,0.93333 -2.8,0.93333z" fill="#3498db"></path><path d="M101.26667,82.6c-1.86667,0 -3.26667,-0.93333 -4.2,-2.8c-0.93333,-2.33333 0,-5.13333 2.33333,-6.06667c1.86667,-0.93333 3.26667,-3.73333 3.26667,-6.06667c0,-2.8 -1.4,-5.13333 -3.26667,-6.06667c-2.33333,-0.93333 -3.26667,-3.73333 -2.33333,-6.06667c0.93333,-2.33333 3.73333,-3.26667 6.06667,-2.33333c5.6,2.8 8.86667,7.93333 8.86667,14.46667c0,6.06667 -3.26667,11.66667 -8.86667,14.46667c-0.46667,0.46667 -1.4,0.46667 -1.86667,0.46667z" fill="#3498db"></path></g></g></g></svg>
</div>
<div class="col_five_sixth ">
    <h4>What Kind Of Equipment Is There?</h4>
    <p align="justify">    
        Cable TV and Internet packages with FREE equipment and other niceties are acceptable to the modern customer. Because you’re paying a reasonable amount for the service, getting some extra value to your package isn’t bad. 
        You can go for AT&T U-verse or DIRECTV with the FREE equipment factor attached to them. The kind of equipment that comes with a TV, Internet and a phone bundle is in the form of a modem, a DVR device and other related hardware.
        We are not here to those providers with FREE equipment, but rather to those services which do not compromise on quality. Their criterion to sell is based on quality and not just the giveaways.
    </p>
</div>-->


<!--<div class="col_one_sixth center align-items-center">
    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
    width="180" height="180"
    viewBox="0 0 224 224"
    style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g><g id="surface1"><path d="M42,18.66667h140v186.66667h-140z" fill="#fbe9e7"></path><path d="M140,172.66667h13.07031l-23.33333,-23.33333l-13.07031,13.07031l23.33333,23.33333z" fill="#f4511e"></path><path d="M84,172.66667h-13.07031l23.33333,-23.33333l13.07031,13.07031l-23.33333,23.33333z" fill="#f4511e"></path><path d="M70,60.66667h84v18.66667h-84z" fill="#ff8a65"></path><path d="M70,93.33333h84v9.33333h-84z" fill="#ff8a65"></path><path d="M112,121.33333c-13.07031,0 -23.33333,10.26302 -23.33333,23.33333c0,13.07031 10.26302,23.33333 23.33333,23.33333c13.07031,0 23.33333,-10.26302 23.33333,-23.33333c0,-13.07031 -10.26302,-23.33333 -23.33333,-23.33333zM112,158.66667c-7.92969,0 -14,-6.07031 -14,-14c0,-7.92969 6.07031,-14 14,-14c7.92969,0 14,6.07031 14,14c0,7.92969 -6.07031,14 -14,14z" fill="#ff8a65"></path><path d="M37.33333,14v196h149.33333v-196zM177.33333,186.66667c-7.92969,0 -14,6.07031 -14,14h-102.66667c0,-7.92969 -6.07031,-14 -14,-14v-149.33333c7.92969,0 14,-6.07031 14,-14h102.66667c0,7.92969 6.07031,14 14,14z" fill="#ff8a65"></path></g></g></g></svg>
</div>
<div class="col_five_sixth col_last">
    <h4>Terms & Conditions by Service Providers</h4>
    <p align="justify">
    Carefully going through the contract’s terms and conditions is must before purchasing any service plan. Generally, TV-service providers contract on a yearly basis. However, satellite TV contracts are for two-years.
    Customers should not have any problem with the length of the contract’s and the promotional prices’ time. Shorter promotional pricing period than the contract can make them pay more in between your contract; hence, contracts should be clear.
    For people who don’t want to stick with a contract and are always on the go, can consult TV providers like Spectrum or Frontier FiOS. It is indeed a comfortable choice for customers with any specific place to live in.
    </p>
</div>-->


<!--<div class="col_one_sixth center align-items-center col_last">
    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
    width="180"
    viewBox="0 0 224 224"
    style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g><g id="surface1"><path d="M18.66667,130.66667v-93.33333c0,-10.26302 8.40364,-18.66667 18.66667,-18.66667h130.66667c10.26302,0 18.66667,8.40364 18.66667,18.66667v93.33333c0,10.26302 -8.40364,18.66667 -18.66667,18.66667h-130.66667c-10.26302,0 -18.66667,-8.40364 -18.66667,-18.66667z" fill="#34495e"></path><path d="M168,32.66667h-130.66667c-2.80729,0 -4.66667,1.85938 -4.66667,4.66667v93.33333c0,2.80729 1.85938,4.66667 4.66667,4.66667h130.66667c2.80729,0 4.66667,-1.85937 4.66667,-4.66667v-93.33333c0,-2.80729 -1.85937,-4.66667 -4.66667,-4.66667z" fill="#bbdefb"></path><path d="M177.33333,154h-149.33333c-10.26302,0 -18.66667,-8.40364 -18.66667,-18.66667h186.66667c0,10.26302 -8.40364,18.66667 -18.66667,18.66667z" fill="#37474f"></path><path d="M112,186.66667v-112c0,-10.26302 8.40364,-18.66667 18.66667,-18.66667h56c10.26302,0 18.66667,8.40364 18.66667,18.66667v112c0,10.26302 -8.40364,18.66667 -18.66667,18.66667h-56c-10.26302,0 -18.66667,-8.40364 -18.66667,-18.66667z" fill="#333333"></path><path d="M186.66667,70h-56c-2.80729,0 -4.66667,1.85938 -4.66667,4.66667v102.66667c0,2.80729 1.85938,4.66667 4.66667,4.66667h56c2.80729,0 4.66667,-1.85937 4.66667,-4.66667v-102.66667c0,-2.80729 -1.85937,-4.66667 -4.66667,-4.66667z" fill="#fff3e0"></path><path d="M165.66667,193.66667c0,3.86458 -3.13542,7 -7,7c-3.86458,0 -7,-3.13542 -7,-7c0,-3.86458 3.13542,-7 7,-7c3.86458,0 7,3.13542 7,7z" fill="#1abc9c"></path></g></g></g></svg>
</div>
<div class="col_five_sixth">
    <h4>Service that Entertains You All the Time</h4> 
    <p align="justify">
        People are not always in front of TV screens, holding remote to entertain themselves. With the technology advancements, the mobile phone is become the vantage point of all entertainment and information.   
        Thus, many TV-service providers now offer TV- apps along with the service so that, you never miss any show, news, or sports event.
        These apps allow you to watch more channels on the Home-WIFI system comparatively. Before making the deal done, make sure that you carefully read all the reviews and available channels on your app.
    </p>
</div>-->


</div>
</div>
            
            
            
<!-- PROMOBOX 2 START -->                
<div class="section clear-bottommargin-lg" style="background-color: #EBA74B">
    <div class="container">
        <div class="row center justify-content-center">
                <div class="col-md-auto">
                        <h2 class="t700 text-white mb-3">What you see is what you get – the ultimate answer to your TV, Internet and phone rhetoric!</h2>
                        <!--<h4 class="text-white">We provide you with plenty of information about the epic TV-services, So that, you make an educated choice.</h4>-->
                </div>
        </div>
    </div>
</div>
<!-- PROMOBOX 2 END   -->  


</div>
</section> 


</div>
</section><!-- #content end -->
<?php require_once 'views/footer.php'; ?>