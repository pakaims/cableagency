<?php require_once 'include/functions.php'; ?>
<?php
    if (!isset($_SESSION["login_name"])) {
        redirect_to("login.php");
}
?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,400i,700|Istok+Web:400,700" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />

	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="css/components/ion.rangeslider.css" type="text/css" />

	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<meta name='viewport' content='initial-scale=1, viewport-fit=cover'>

	<!-- Hosting Demo Specific Stylesheet -->
	<link rel="stylesheet" href="css/colors.php?color=44aaac" type="text/css" /> <!-- Theme Color -->
	<link rel="stylesheet" href="demos/hosting/css/fonts.css" type="text/css" />
	<link rel="stylesheet" href="demos/hosting/hosting.css" type="text/css" />
        
	<!-- Bootstrap Data Table Plugin -->
	<link rel="stylesheet" href="css/components/bs-datatable.css" type="text/css" />        
	<!-- / -->

	<!-- Document Title
	============================================= -->
	<title>Cable | Table Data</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar
		============================================= -->
<!--		<div id="top-bar" class="center dark" style="background-color: #15888a">
			<p class="mb-0 text-white" style="font-size: 14px;">Holisticly cultivate multifunctional quality vectors after Mobile SDK.<a href="#" class="ml-2 font-primary t700 text-white"><u>Learn More</u> &#8250;</a></p>
		</div>-->

		<!-- Header
		============================================= -->
		<header id="header">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="index.php" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="images/logo.png" alt="Canvas Logo"></a>
						<!-- <a href="index.php" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="images/logo@2x.png" alt="Canvas Logo"></a> -->
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu" class="sub-title">

						<ul>
							<li><a href="index.php"><div>Home</div><span>Lets Start</span></a></li>
							<li><a href="best-providers.php"><div>Providers</div><span>Awesome deals</span></a></li>
                                                        <li><a href="about-us.php"><div>About us</div><span>what we do</span></a></li>
                                                        <li><a href="data-selector.php"><div>Main Admin</div><span>admin panel</span></a></li>
                                                        <li><a href="include/logout.php"><div>Logout</div><span>bye bye!</span></a></li>
						</ul>

					</nav><!-- #primary-menu end -->

				</div>

			</div>

		</header>
                

		<!-- Page Title
		============================================= -->
<!--		<section id="page-title" class="page-title-parallax page-title-dark" style="padding: 250px 0; background-image: url('images/later/privacy.jpg'); background-size: cover; background-position: center center;" data-bottom-top="background-position:0px 400px;" data-top-bottom="background-position:0px -500px;">

			<div class="container clearfix">
                            <h1><strong>Data Table</strong></h1>
                            <h3 class="text-white">Nothing to hide and everything to offer</h3>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Pages</a></li>
					<li class="breadcrumb-item active" aria-current="page">About Us</li>
				</ol>
			</div>
                    
		</section> #page-title end -->


                
		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<div class="table-responsive">
						<table id="datatable1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                    <thead>
								<tr>
									<th>S.No.</th>
									<th>Zip Code</th>
									<th>Modification</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>S.No.</th>
									<th>Zip Code</th>
									<th>Modification</th>
								</tr>
							</tfoot>
                                                        
							<tbody>
<?php                                                             
if(isset($_POST['frontier-form'])){
$selected_val = $_POST['selected_option1'];  // Storing Selected Value In Variable
 
if ($selected_val == 1) {
        $_SESSION['thistable'] = "charter";
//        $tablename = "frontier_tv"; 
}elseif ($selected_val == 2) {
                $_SESSION['thistable'] = "frontier";
//        $tablename = "frontier_phone";      
         }else { echo "Something is wrong!"; }      
}





$session_table = $_SESSION['thistable'];
$zips_set = find_zip_for_datatable($session_table);  
$counter = 0;
                                                             
while($row = mysqli_fetch_array($zips_set))                                                            
{
    $counter++;
    $output  = "<tr>";
    $output .= "<td>".$counter."</td>";
    $output .= "<td>".$row['zip']."</td>";
    $output .= "<td class='center'><a href='include/delete-record.php?record=".$row['zip']."' onclick='javascript:confirmationDelete($(this));return false;' class='button button-rounded button-reveal button-mini button-red'><i class='icon-trash'></i><span>DELETE</span></a></td>";
    $output .= "</tr>";
    echo $output;
}
unset($row);
unset($zips_set);
?>
                                                            
<!--								<tr>
                                                                    <td>1</td>
                                                                    <td>133301</td>
                                                                    <td class="center"><a href="delete.php?id=" class="button button-rounded button-reveal button-mini button-red"><i class="icon-trash"></i><span>DELETE</span></a></td>
                                                                </tr>                                                     -->
							</tbody>
						</table>
					</div>

				</div>

			</div>

		</section><!-- #content end -->


<!-- Footer
============================================= -->
<?php require_once 'views/footer.php'; ?>

        </div>
<!--<script>
        $(function() {
                $( "#side-navigation" ).tabs({ show: { effect: "fade", duration: 400 } });
        });
</script>-->

	<!-- External JavaScripts
	============================================= -->
	<!--<script src="js/jquery.js"></script>-->
	<!--<script src="js/plugins.js"></script>-->

	<!-- Bootstrap Data Table Plugin -->
	<script src="js/components/bs-datatable.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<!--<script src="js/functions.js"></script>-->

	<script>

		$(document).ready(function() {
			$('#datatable1').dataTable();
		});

	</script>
        <script>
            function confirmationDelete(anchor)
            {
               var conf = confirm('Are you sure want to delete this record?');
               if(conf)
                  window.location=anchor.attr("href");
            }
        </script>