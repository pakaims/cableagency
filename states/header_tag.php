</head>
<body class="stretched">
<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">
<!-- Header
============================================= -->
<header>
        <div class="menu-toggle" id="hamburger">
            <i class="fas fa-bars"></i>
        </div>
        <div class="overlay"></div>
        <div class="container">
            <nav>
			<div id="logo">
            <a href="index.php" class="standard-logo" data-dark-logo="images/logo-02.png"><img src="../images/logo-02.png" alt="Canvas Logo"></a>
						<a href="index.php" class="retina-logo" data-dark-logo="images/white logo small size-02.png"><img src="../images/white logo small size-02.png" alt="Canvas Logo"></a>
	</div>
                <ul>
                    <li class="active current"><a href="../index.php">Home</a></li>
                    <li><a href="../best-providers.php">Providers</a></li>
                    <li><a href="../about-us.php">About Us</a></li>
                    <div style="padding-left:25px;" ><a href="tel:18889309001"> (888) 930-9001</a></div>
                </ul>
            </nav>
        </div>
</header>
	<script>var open = document.getElementById('hamburger');
var changeIcon = true;

open.addEventListener("click", function(){

    var overlay = document.querySelector('.overlay');
    var nav = document.querySelector('nav');
    var icon = document.querySelector('.menu-toggle i');

    overlay.classList.toggle("menu-open");
    nav.classList.toggle("menu-open");

    if (changeIcon) {
        icon.classList.remove("fa-bars");
        icon.classList.add("fa-times");

        changeIcon = false;
    }
    else {
        icon.classList.remove("fa-times");
        icon.classList.add("fa-bars");
        changeIcon = true;
    }
});</script>
<!-- <header id="header">
    <div id="header-wrap">
        <div class="container clearfix">
            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div> -->
            <!-- Logo
            ============================================= -->
            <!-- <div id="logo">
                    <a href="index.php" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="../images/logo.png" alt="Canvas Logo"></a>
                    <a href="index.php" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="../images/logo@2x.png" alt="Canvas Logo"></a>
            </div> -->
            <!-- #logo end -->
            <!-- Primary Navigation
            ============================================= -->
            <!-- <nav id="primary-menu" class="sub-title">
                <ul>
                    <li class="current"><a href="../index.php"><div>Home</div><span>Lets Start</span></a></li>
                    <li><a href="../best-providers.php"><div>Providers</div><span>Awesome deals</span></a></li>
                    <li><a href="../about-us.php"><div>About us</div><span>what we do</span></a></li>
                </ul>
            </nav> -->
            <!-- #primary-menu end -->
        <!-- </div>
    </div>
</header> -->

<!-- Page Title
============================================= -->
<!-- <section id="page-title" class="page-title-center" style="background: #44aaac">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-4">
                <form action="../include/zip-processor.php" method="get" class="center align-items-center">
                    <div class="heading-block nobottommargin nobottomborder center align-items-center">
                        <h2 class="text-white">Cable TV and Internet Providers in a Click</h2>
                        <span class="text-white">Enter your zip code to view providers</span>
                    </div>
                    <div class="line" style="margin: 20px 0 30px;"></div>
                    <div class="col_full">
                        <input type="number" name="zipcode" maxlength="5" pattern="[0-9]{5}" required="true" class="form-control required form-control-lg not-dark input-number–noSpinners" value="" placeholder="Zip Code*">
                    </div>
                    <div class="col_full nobottommargin">
                        <button class="btn btn-lg btn-danger btn-block nomargin" value="submit" type="submit" style="">FIND</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section> -->
<div class="header">
	<div class="vertical-middle ignore-header dark">
				<div class="container">
	<div class="row">
	<div class="col-md-4"></div>
	<div class="col-md-4">
	<form action="include/zip-processor.php" method="get" class="topmargin-lg landing-wide-form landing-form-overlay dark nobottommargin clearfix">
						<div class="heading-block nobottommargin nobottomborder center align-items-center">
							<h2>Search for Cable TV and Internet Providers in Your Area</h2>
							<!--<span>Enter your zip code to view providers available in your area.</span>-->
						</div>
						<div class="line" style="margin: 20px 0 30px;"></div>
						<div class="col_full">
                                                    <input type="number" name="zipcode" maxlength="5" pattern="[0-9]{5}" required="true" class="form-control required form-control-lg not-dark input-number–noSpinners" value="" placeholder="Zip Code*">
                                                        <!--<input type="text" name="zipcode">-->
						</div>
						<div class="col_full nobottommargin">
							<button class="btn btn-lg btn-danger btn-block nomargin" value="submit" type="submit" style="">FIND</button>
                                                        <!--<button type="submit">submit</button>-->
						</div>
					</form>
	</div>
	</div>
												
						</div>
					</div>
				</div>
						
<!-- #page-title end -->
                            
<div class="container topmargin-sm clearfix">
<div class="heading-block center divcenter nobottomborder clearfix" style="max-width: 700px">
        <h2>Fill the Void of Classic TV Viewing and Speedy Internet Streaming in 3 Easy Steps.</h2>
        <!--<p>Competently recaptiualize multifunctional schemas without an expanded array of niches. Continually engage cooperative sources vis-a-vis web-enabled benefits.</p>-->
</div>
<div class="col_one_third" data-animate="flipInY">
    <div class="feature-box fbox-center fbox-bg fbox-light fbox-effect">
        <div class="fbox-icon">
            <a href="#"><i class="i-alt">1</i></a>
        </div>
        <h3>Enter the zip code of the place you want to search around<span class="subtitle">Step 1</span></h3>
    </div>
</div>

<div class="col_one_third" data-animate="flipInY" data-delay="500">
    <div class="feature-box fbox-center fbox-bg fbox-border fbox-effect">
        <div class="fbox-icon">
            <a href="#"><i>2</i></a>
        </div>
        <h3>System checks the zip code against the database of providers<span class="subtitle">Step 2</span></h3>
    </div>
</div>

<div class="col_one_third col_last" data-animate="flipInY" data-delay="1000">
    <div class="feature-box fbox-center fbox-bg fbox-outline fbox-dark fbox-effect">
        <div class="fbox-icon">
            <a href="#"><i class="i-alt">3</i></a>
        </div>
        <h3>Make the call to subscribe for a suitable TV & Internet package<span class="subtitle">Step 3</span></h3>
    </div>
</div>
<div class="divider" data-animate="bounceInLeft"><i class="icon-circle"></i></div>

</div>
<div class="clear"></div>		<!-- Content
		============================================= -->
<section id="content">
    <div class="content-wrap nopadding">
        <div class="container clearfix">
        <br><br>    
        <div class="nobottommargin">

