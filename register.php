<?php session_start(); ?>
<?php // $already_email = $_SESSION[already_email]; ?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,400i,700|Istok+Web:400,700" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />

	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="css/components/ion.rangeslider.css" type="text/css" />

	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<meta name='viewport' content='initial-scale=1, viewport-fit=cover'>

	<!-- Hosting Demo Specific Stylesheet -->
	<link rel="stylesheet" href="css/colors.php?color=44aaac" type="text/css" /> <!-- Theme Color -->
	<link rel="stylesheet" href="demos/hosting/css/fonts.css" type="text/css" />
	<link rel="stylesheet" href="demos/hosting/hosting.css" type="text/css" />
        
        
	<!-- / -->

	<!-- Document Title
	============================================= -->
	<title>Cable | Privacy Policy</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar
		============================================= -->
<!--		<div id="top-bar" class="center dark" style="background-color: #15888a">
			<p class="mb-0 text-white" style="font-size: 14px;">Holisticly cultivate multifunctional quality vectors after Mobile SDK.<a href="#" class="ml-2 font-primary t700 text-white"><u>Learn More</u> &#8250;</a></p>
		</div>-->

		<!-- Header
		============================================= -->
		<header id="header">
			<div id="header-wrap">
				<div class="container clearfix">
					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="index.php" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="images/logo.png" alt="Canvas Logo"></a>
						<a href="index.php" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="images/logo@2x.png" alt="Canvas Logo"></a>
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu" class="sub-title">
						<ul>
							<li><a href="index.php"><div>Home</div><span>Lets Start</span></a></li>
							<li><a href="best-providers.php"><div>Providers</div><span>Awesome deals</span></a></li>
                                                        <li><a href="about-us.php"><div>About us</div><span>what we do</span></a></li>
						</ul>
					</nav><!-- #primary-menu end -->
				</div>
			</div>
		</header>
                



                
		<!-- Content
		============================================= -->
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            
            <div class="acctitle"><i class="acc-closed icon-user4"></i><i class="acc-open icon-ok-sign"></i>New Signup? Register for an Account</div>
            <div class="acc_content clearfix">
                <form id="register-form" name="register-form" class="nobottommargin" action="include/reg-processor.php" method="POST">
                            <div class="col_full">
                                    <label for="register-form-name">Name:</label>
                                    <input type="text" required id="register-form-name" name="register-form-name" value="" class="form-control" />
                            </div>

                            <div class="col_full">
                                    <label for="register-form-email">Email Address:</label>
                                    <input type="email" required id="register-form-email" name="register-form-email" value="" class="form-control" />
                                    <?php if (isset($_SESSION['already_email'])) {
                                        echo '<p style="color:red;">Email already exsit!</p>';
                                        unset($_SESSION['already_email']);
                                    }?>
                                    
                            </div>

                            <div class="col_full">
                                    <label for="register-form-password">Choose Password:</label>
                                    <input type="password" required id="register-form-password" name="register-form-password" value="" class="form-control" />
                            </div>

                            <div class="col_full">
                                    <label for="register-form-repassword">Re-enter Password:</label>
                                    <input type="password" required id="register-form-repassword" name="register-form-repassword" value="" class="form-control" />
                            </div>

                            <div class="col_full nobottommargin">
                                    <button class="button button-3d button-black nomargin" id="register-form-submit" name="register-form-submit" value="register">Register Now</button>
                            </div>
                    </form>
            </div>

        </div>

    </div>

</section><!-- #content end -->                





<!-- Footer
============================================= -->
<?php require_once 'views/footer.php'; ?>