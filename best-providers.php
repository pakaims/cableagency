<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,400i,700|Istok+Web:400,700" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="css/components/ion.rangeslider.css" type="text/css" />

	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<meta name='viewport' content='initial-scale=1, viewport-fit=cover'>

	<!-- Hosting Demo Specific Stylesheet -->
	<link rel="stylesheet" href="css/colors.php?color=44aaac" type="text/css" /> <!-- Theme Color -->
	<link rel="stylesheet" href="demos/hosting/css/fonts.css" type="text/css" />
	<link rel="stylesheet" href="demos/hosting/hosting.css" type="text/css" />
        
        
	<!-- / -->

	<!-- Document Title
	============================================= -->
	<title>Cable | Best Providers</title>

</head>

<body class="stretched">
    
<?php require_once 'views/modalbox.php'; ?>
	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">
    <header>
        <div class="menu-toggle" id="hamburger">
            <i class="fas fa-bars"></i>
        </div>
        <div class="overlay"></div>
        <div class="container">
            <nav>
			<div id="logo">
						<a href="index.php" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="images/logo.png" alt="Canvas Logo"></a>
						<a href="index.php" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="images/logo@2x.png" alt="Canvas Logo"></a>
	</div>
                <ul>
                    <li class="active current"><a href="index.php">Home</a></li>
                    <li><a href="best-providers.php">Providers</a></li>
                    <li><a href="about-us.php">About Us</a></li>
                    <div style="padding-left:25px;" ><a href="tel:18889309001"> (888) 930-9001</a></div>
                </ul>
            </nav>
        </div>
</header>
	<script>var open = document.getElementById('hamburger');
var changeIcon = true;

open.addEventListener("click", function(){

    var overlay = document.querySelector('.overlay');
    var nav = document.querySelector('nav');
    var icon = document.querySelector('.menu-toggle i');

    overlay.classList.toggle("menu-open");
    nav.classList.toggle("menu-open");

    if (changeIcon) {
        icon.classList.remove("fa-bars");
        icon.classList.add("fa-times");

        changeIcon = false;
    }
    else {
        icon.classList.remove("fa-times");
        icon.classList.add("fa-bars");
        changeIcon = true;
    }
});</script>
		
                
                <!-- #header end -->


		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

<div class="clear clearfix clear-bottommargin-lg"></div>
<div class="clear clearfix clear-bottommargin-sm"></div>
<div class="clear clearfix clear-bottommargin-sm"></div>
<div class="section clear-bottommargin-lg">
    <div class="container clearfix">
        
        <div class="heading-block center">
                <h2 data-animate="fadeIn">The Top-Tier TV Service Providers in the US.</h2>
                <span data-animate="fadeIn" class="delayyy-s25">Everybody deserves a full entertainment package via HD TV service at home, waiting areas, or lounges. Nevertheless, what’s worse when you have a so-called best television connection and still can’t enjoy unlimited channel streaming with advanced features, and a comfortable agreement? We got you covered and bring the best of the best TV service providers with specialties and ratings to limit your search with CableAgency.com </span>
        </div>

<?php 
require_once"views/companies/directv_box.php";
require_once"views/companies/frontier_box.php";
require_once"views/companies/spectrum_box.php";
require_once"views/companies/centurylink_box.php";
require_once"views/companies/windstream_box.php";
require_once"views/companies/viasat_box.php";
?>                                        

 
                                
 

        <div class=" center clear-bottommargin" data-animate="fadeIn">
                <!--<h2>The Top-Tier TV Service Providers in the US.</h2>-->
                <span>Our Recommended TV service providers weigh heavy on customer satisfaction with attractive deals to satisfy your daily entertainment dose. We rate these providers for popularity and innovation in technology with ample knowledge. Trust our years of thorough research and choose only the best for your homes. </span>
        </div>
        <br>
        <br>
        
    </div> 
    
    
    

    
</div>

<!-- PROMOBOX 1 START -->
<div class="section clear-bottommargin-lg" style="background-color: #AEBC4A">
<div class="container">
        <div class="row center justify-content-center">
                <div class="col-md-auto"  data-animate="bounceInLeft">
                        <h2 class="t700 text-white mb-3">Look for the Best Available Service in Your Area by entering your ZIP code</h2>
                        <!--<h4 class="text-white">We provide you with plenty of information about the epic TV-services, So that, you make an educated choice.</h4>-->
                </div>
            <!--<div class="subscribe-widget clear-bottommargin">-->
                <!--<div class="widget-subscribe-form-result"></div>-->
                <form id="zipform1" action="include/zip-processor.php" method="get"  data-animate="bounceInRight">
                    <input name="zipcode" maxlength="5" required class="input-number–noSpinners form-control-lg float-left" type="number" placeholder="Enter Zip Code">
                    <a style="margin-top:1px;" href="javascript:{}" onclick="document.getElementById('zipform1').submit();" class="button button-rounded button-reveal button-large button-dirtygreen"><i class="icon-map-marker2"></i><span>Find</span></a>
                    <!--<input type="submit" hidden="true">-->
                </form>
            <!--</div>-->

        </div>
</div>
</div>
<!-- PROMOBOX 1 END   -->

<br>
<br>
<br>
<section id="content">
        <div class="content-wrap nopadding">
            <div class="container clearfix"> 
            <div class="nobottommargin topmargin-lg">
                <div class=" center bottommargin-sm" data-animate="fadeIn">
                    <h2>Experience Value for Money Entertainment by the Best Brands in Town</h2>
                </div>                    

<!-- COMPANY DETAIL 1 -->                    
<div class="col_one_third" data-animate="jackInTheBox">
    <br>
    <br>
    <img class="alignright img-responsive" src="images/later/logos/logo-directv.svg" alt="NO image">	
</div>
<div class="col_two_third  col_last delayyy-s25" data-animate="fadeIn">
    <div class="team-title"><h3>1. DIRECTV</h3><span>Sports channels that never let you miss any event</span></div>
        With over thirty-five sports channels and NFL Sunday Ticket, sports lovers can spend hours of absolute leisure and it multiplies as the games proceed. In addition, DIRECTV lets you explore matches and world cups via smartphones and tablets.  Also, get an International Sports package at a pocket-friendly rate with HD picture clarity.<br>
    <br>
    <a href="#" class="button button-rounded button-reveal button-large button-red tright"><i class="icon-angle-right"></i><span>VIEW PLANS</span></a>
</div>                 
<div class="divider divider-short divider-border divider-center"><i class="icon-like"></i></div>
<div class="clear"></div>

<!-- COMPANY DETAIL 2 -->                    
<div class="col_one_third" data-animate="jackInTheBox">
    <br>
    <br>
    <img class="alignright img-responsive" src="images/later/logos/logo-frontier.svg" alt="NO image">	
</div>
<div class="col_two_third  col_last delayyy-s25" data-animate="fadeIn">
    <div class="team-title"><h3>2. FRONTIER </h3><span>Less Worry of Contract Strings – More Room To Be Yourself</span></div>
        When your lifestyle is always on the go and can’t afford to bind yourself with a contract, Frontier FiOS is your best shot. It is everything that you need at an affordable price and at your comfortability. Subscribe to it for stress-free channel surfing and unlimited options to explore.    <br>
        <br>
        <a href="#" class="button button-rounded button-reveal button-large button-red tright"><i class="icon-angle-right"></i><span>VIEW PLANS</span></a>
</div>                 
<div class="divider divider-short divider-border divider-center"><i class="icon-like"></i></div>
<div class="clear"></div>
<!-- COMPANY DETAIL 3 -->                    
<div class="col_one_third" data-animate="jackInTheBox">
    <br>
    <br>
    <img class="alignright img-responsive" src="images/later/logos/logo-spectrum.svg" alt="NO image">	
</div>
<div class="col_two_third  col_last delayyy-s25" data-animate="fadeIn">
    <div class="team-title"><h3>3. SPECTRUM</h3><span>Triple Play Gold - Premium Channels in a Cost-Effective Bundle</span></div>
        Who doesn’t love premium entertainment channels in HD-picture quality without having to pay extra? Everybody does. Spectrum tends to this demand and offers an amazing deal of channels like SHOWTIME, HBO, STARZ, TMC, and Cinemax within your budget. Experience the new level of pocket-friendly entertainment.    <br>
        <br>
        <a href="#" class="button button-rounded button-reveal button-large button-red tright"><i class="icon-angle-right"></i><span>VIEW PLANS</span></a>
</div>                 
<div class="divider divider-short divider-border divider-center"><i class="icon-like"></i></div>
<div class="clear"></div>
<!-- COMPANY DETAIL 4 -->                    
<div class="col_one_third" data-animate="jackInTheBox">
    <br>
    <br>
    <img class="alignright img-responsive" src="images/later/logos/century.png" alt="NO image">	
</div>
<div class="col_two_third  col_last delayyy-s25" data-animate="fadeIn">
<div class="team-title"><h3>4. CENTURYLINK </h3><span>Internet unlike any other on planet earth</span></div>
	CenturyLink aims to improve digital lines of communication across the country. In doing so, it is continually growing its Fiber network that is capable of delivering speeds of up to 940 Mbps. It is going after a revolution in the history of the Internet.
       <br>
       <br>
        <a href="#" class="button button-rounded button-reveal button-large button-red tright"><i class="icon-angle-right"></i><span>VIEW PLANS</span></a>
</div>                 
<div class="divider divider-short divider-border divider-center"><i class="icon-like"></i></div>
<div class="clear"></div>
<!-- COMPANY DETAIL 5 -->                    
<div class="col_one_third" data-animate="jackInTheBox">
    <br>
    <br>
    <img class="alignright img-responsive" src="images/later/logos/wind-PKG.png" alt="NO image">	
</div>
<div class="col_two_third  col_last delayyy-s25" data-animate="fadeIn">
<div class="team-title"><h3>5. WINDSTREAM </h3><span>Future of telecommunications and visual excellence</span></div>
	From out-of-the-box package selection to seamless customer services, Windstream Communications focuses on HD TV channels, fast Internet speeds and distortion-less connectivity on the phone for its customers. Connect with us to connect with the future.
        <br>
        <br>
        <a href="#" class="button button-rounded button-reveal button-large button-red tright"><i class="icon-angle-right"></i><span>VIEW PLANS</span></a>
</div>                 
<div class="divider divider-short divider-border divider-center"><i class="icon-like"></i></div>
<div class="clear"></div>
<!-- COMPANY DETAIL 6 -->                    
<div class="col_one_third" data-animate="jackInTheBox">
    <br>
    <br>
    <img class="alignright img-responsive" src="images/later/logos/viasat-logo.png" alt="NO image">	
</div>
<div class="col_two_third  col_last delayyy-s25" data-animate="fadeIn">
<div class="team-title"><h3>6. VIASAT </h3><span>HD channels, two-way speedy data, and voice communication</span></div>
	Viasat, a global communications company with a strong belief that anything, anywhere in the world can be connected. To fulfill this purpose, they have come up with fast Internet connectivity; channels with crisp resolution; and voice that seems to be near your heart.
        <br>
        <br>
        <a href="#" class="button button-rounded button-reveal button-large button-red tright"><i class="icon-angle-right"></i><span>VIEW PLANS</span></a>
</div>                 
<div class="divider divider-short divider-border divider-center"><i class="icon-like"></i></div>
<div class="clear"></div>



<!-- ATTRIBUTES SECTION 1 -->
<div class="col_full">
        <div class="heading-block center nobottomborder" data-animate="fadeIn">
                <h2>Choose a TV Service Provider that makes your Life Easy </h2>
                <!--<span>We value Work Ethics &amp; Environment as it helps in creating a Creative Thinktank</span>-->
        </div>
        <div class="fslider" data-pagi="false" data-animation="fade">
                <div class="flexslider">
                        <div class="slider-wrap">
                                <div class="slide"><a href="#"><img src="images/about/4.jpg" alt="About Image"></a></div>
                                <div class="slide"><a href="#"><img src="images/about/5.jpg" alt="About Image"></a></div>
                                <div class="slide"><a href="#"><img src="images/about/6.jpg" alt="About Image"></a></div>
                                <div class="slide"><a href="#"><img src="images/about/7.jpg" alt="About Image"></a></div>
                        </div>
                </div>
        </div>

</div>


<div class="col_one_third" >
        <div class="feature-box fbox-large fbox-border fbox-light fbox-effect" data-animate="bounceIn">
                <div class="fbox-icon">
                        <a href="#"><i class="icon-screen"></i></a>
                </div>
                <h3>Freedom to Select</h3>
                <p align="justify">
Channels of your choice with a variety of cable TV, satellite, and Internet service providers and that is how the cookie crumbles.
                </p>
        </div>
</div>

<div class="col_one_third" data-animate="bounceIn" data-delay="250">
        <div class="feature-box fbox-large fbox-border fbox-light fbox-effect">
                <div class="fbox-icon">
                        <a href="#"><i class="icon-stack"></i></a>
                </div>
                <h3>Lucrative Bundles</h3>
                <p align="justify">To give your TV entertainment an edge over the others! Forget individual services and bundle up to save and extrapolate.</p>
        </div>
</div>

<div class="col_one_third col_last" data-animate="bounceIn" data-delay="500">
        <div class="feature-box fbox-large fbox-border fbox-light fbox-effect">
                <div class="fbox-icon">
                        <a href="#"><i class="icon-location"></i></a>
                </div>
                <h3>24/7 Customer Support</h3>
                <p align="justify">With any one of the brands listed on CableAgency.com. As long as they are available in your area, connect with their 24/7 customer support.</p>
        </div>
</div>


<div class="col_one_third" data-animate="bounceIn" data-delay="750">
        <div class="feature-box fbox-large fbox-border fbox-light fbox-effect">
                <div class="fbox-icon">
                        <a href="#"><i class="icon-info"></i></a>
                </div>
                <h3>Service Reliability</h3>
                <p align="justify">Whether you choose with your eyes closed, you can depend on it for your entertainment needs, Internet speeds, and phone calls.</p>
        </div>
</div>

<div class="col_one_third" data-animate="bounceIn" data-delay="1000">
        <div class="feature-box fbox-large fbox-border fbox-light fbox-effect">
                <div class="fbox-icon">
                        <a href="#"><i class="icon-thumbs-up"></i></a>
                </div>
                <h3>Value-Added Services</h3>
                <p align="justify">Come with brands as part of their marketing policy. Some are free from contracts, while others giveaway equipment as part of the deal.</p>
        </div>
</div>

<div class="col_one_third col_last" data-animate="bounceIn" data-delay="1250">
        <div class="feature-box fbox-large fbox-border fbox-light fbox-effect">
                <div class="fbox-icon">
                        <a href="#"><i class="icon-dollar"></i></a>
                </div>
                <h3>Land a Perfect Deal</h3>
                <p align="justify">Is what we want you to have in the end? We want you to get the best deal in your area with the brands registered in your location.</p>
        </div>
</div>




<!-- ATTRIBUTES SECTION 2 -->
<div class="col_full" data-animate="fadeIn">

        <div class="heading-block center nobottomborder">
                <h2>What Should Customers Look in the Perfect TV Service Provider? </h2>
                <span>The reliability and availability of services vary, depending upon the service type. Therefore, customers should closely into the pros and cons of each type.</span>
        </div>
</div>

<div class="row grid-container" data-layout="masonry" style="overflow: visible">
        <div class="col-lg-4 mb-4" data-animate="bounceIn">
                <div class="flip-card text-center">
                        <div class="flip-card-front dark" style="background-image: url('images/later/flip_boxes/1.jpg')">
                                <div class="flip-card-inner">
                                        <div class="card nobg noborder text-center">
                                                <div class="card-body">
                                                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                                    width="150" height="150"
                                                    viewBox="0 0 224 224"
                                                    style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g id="Layer_1"><g id="surface1"><path d="M97.06667,120.4c1.86667,-1.86667 4.66667,-1.86667 6.53333,0c1.86667,1.86667 1.86667,4.66667 0,6.53333l-6.53333,6.53333c-1.86667,1.86667 -4.66667,1.86667 -6.53333,0c-1.86667,-1.86667 -1.86667,-4.66667 0,-6.53333z" fill="#546e7a"></path><path d="M156.33333,173.13333l-33.13333,-33.13333l39.66667,-39.66667l33.13333,33.13333z" fill="#673ab7"></path><path d="M84,100.8l-33.13333,-33.13333l39.66667,-39.66667l33.13333,33.13333z" fill="#673ab7"></path><path d="M156.33333,160.06667l-6.53333,-6.53333l9.8,-9.8l6.53333,6.53333z" fill="#b39ddb"></path><path d="M143.26667,147l-6.53333,-7l9.8,-9.8l6.53333,6.53333z" fill="#b39ddb"></path><path d="M173.13333,143.26667l-6.53333,-6.53333l9.8,-9.8l6.53333,6.53333z" fill="#b39ddb"></path><path d="M159.6,130.2l-6.53333,-6.53333l9.8,-9.8l6.53333,6.53333z" fill="#b39ddb"></path><path d="M84,87.26667l-6.53333,-6.53333l9.8,-9.8l6.53333,6.53333z" fill="#b39ddb"></path><path d="M70.46667,74.2l-6.53333,-6.53333l9.8,-9.8l6.53333,6.53333z" fill="#b39ddb"></path><path d="M100.33333,70.93333l-6.53333,-6.53333l9.8,-9.8l6.53333,6.53333z" fill="#b39ddb"></path><path d="M87.26667,57.86667l-6.53333,-6.53333l9.8,-9.8l6.53333,6.53333z" fill="#b39ddb"></path><path d="M140,123.66667l-39.66667,-39.66667l6.53333,-6.53333l39.66667,39.66667z" fill="#78909c"></path><path d="M116.2,146.06667c1.86667,-10.26667 -0.93333,-21 -8.86667,-28.93333c-7.93333,-7.93333 -18.66667,-10.73333 -28.93333,-8.86667z" fill="#78909c"></path><path d="M93.33333,168.93333c-15.86667,0 -38.26667,-14.93333 -38.26667,-38.26667h9.33333c0,16.8 17.26667,28.93333 28.93333,28.93333z" fill="#90caf9"></path><path d="M93.33333,196c-35.93333,0 -65.33333,-29.4 -65.33333,-65.33333h9.33333c0,30.8 25.2,56 56,56z" fill="#90caf9"></path><path d="M133.46667,64.4c1.86667,-1.86667 4.66667,-1.86667 6.53333,0l19.6,19.6c1.86667,1.86667 1.86667,4.66667 0,6.53333l-36.4,36.4c-1.86667,1.86667 -4.66667,1.86667 -6.53333,0l-19.6,-19.6c-1.86667,-1.86667 -1.86667,-4.66667 0,-6.53333z" fill="#b0bec5"></path></g></g></g></svg>
                                                    <!--<i class="icon-line2-shuffle h1" ></i>-->
                                                    <h3 class="card-title">Satellite</h3>
                                                    <!--<p class="card-text t400">With supporting text below as a natural lead-in to additional content.</p>-->
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <div class="flip-card-back bg-danger no-after">
                                <div class="flip-card-inner">
                                        <p class="mb-2 text-white">This service is easy to set-up on any south-facing roof. It receives a TV signal via a satellite. The extreme service interruptions occur not very often and temporary outreaches are for short time only.</p>
                                        <!--<button type="button" class="btn btn-outline-light mt-2">View Details</button>-->
                                </div>
                        </div>
                </div>
        </div>





            <div class="col-lg-4 mb-4 delayyy-s25" data-animate="bounceIn">
                <div class="flip-card text-center">
                        <div class="flip-card-front dark" style="background-image: url('images/later/flip_boxes/2.jpg')">
                                <div class="flip-card-inner">
                                        <div class="card nobg noborder text-center">
                                                <div class="card-body">

                                                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                                    width="150" height="150 "
                                                    viewBox="0 0 224 224"
                                                    style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g id="Layer_1"><g id="surface1"><path d="M37.33333,130.66667h-9.33333c0,-18.66667 6.53333,-34.53333 19.6,-46.66667c35.93333,-33.6 108.26667,-28.46667 111.53333,-28l-0.46667,9.33333c-0.93333,0 -71.4,-4.66667 -104.53333,25.66667c-11.2,10.26667 -16.8,23.8 -16.8,39.66667z" fill="#1565c0"></path><path d="M77,177.33333c-27.06667,0 -49,-21.93333 -49,-49c0,-27.06667 21.93333,-49 49,-49c27.06667,0 49,21.93333 49,49c0,27.06667 -21.93333,49 -49,49zM77,88.66667c-21.93333,0 -39.66667,17.73333 -39.66667,39.66667c0,21.93333 17.73333,39.66667 39.66667,39.66667c21.93333,0 39.66667,-17.73333 39.66667,-39.66667c0,-21.93333 -17.73333,-39.66667 -39.66667,-39.66667z" fill="#1565c0"></path><path d="M95.66667,177.33333c-27.06667,0 -49,-21.93333 -49,-49c0,-27.06667 21.93333,-49 49,-49c27.06667,0 49,21.93333 49,49c0,27.06667 -21.93333,49 -49,49zM95.66667,88.66667c-21.93333,0 -39.66667,17.73333 -39.66667,39.66667c0,21.93333 17.73333,39.66667 39.66667,39.66667c21.93333,0 39.66667,-17.73333 39.66667,-39.66667c0,-21.93333 -17.73333,-39.66667 -39.66667,-39.66667z" fill="#1565c0"></path><path d="M186.66667,167.53333c-13.53333,7.93333 -40.13333,19.13333 -84,19.13333c-64.4,0 -65.33333,-53.66667 -65.33333,-56h-4.66667h-4.66667c0,0.46667 0.93333,65.33333 74.66667,65.33333c41.53333,0 68.13333,-9.33333 84,-17.73333c3.26667,-1.4 6.53333,-3.73333 9.33333,-6.06667v-12.6c-2.8,2.8 -5.6,5.6 -9.33333,7.93333z" fill="#1565c0"></path><path d="M196,46.66667l-32.66667,-18.66667l-4.66667,7.93333l18.66667,10.73333z" fill="#1565c0"></path><path d="M154,46.66667h42v28h-42z" fill="#2196f3"></path><path d="M46.66667,132.53333c0,-1.4 0,-2.8 0,-4.2c0,-1.4 0,-2.8 0,-4.2c1.86667,-23.8 20.53333,-42.46667 44.33333,-44.8c-1.4,0 -3.26667,-0.46667 -4.66667,-0.46667c-27.06667,0.46667 -49,22.4 -49,49.46667c0,27.06667 21.93333,49 49,49c1.4,0 3.26667,0 4.66667,-0.46667c-23.33333,-1.86667 -42,-21 -44.33333,-44.33333z" fill="#2196f3"></path><path d="M95.66667,88.66667c-1.4,0 -3.26667,0 -4.66667,0.46667c19.6,2.33333 35,19.13333 35,39.2c0,20.06667 -15.4,36.86667 -35,39.2c1.4,0 3.26667,0.46667 4.66667,0.46667c20.53333,0 37.33333,-15.86667 39.66667,-35.46667c0,-1.4 0,-2.8 0,-4.2c0,-1.4 0,-2.8 0,-4.2c-2.33333,-19.6 -19.13333,-35.46667 -39.66667,-35.46667z" fill="#2196f3"></path></g></g></g></svg>
                                                    <h3 class="card-title">Cable</h3>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <div class="flip-card-back bg-danger no-after">
                                <div class="flip-card-inner">
                                        <p class="mb-2 text-white">This type of service carries signals through copper wires and most of the homes are equipped with this service.  However, a big disadvantage is the fixation period after a wire is accidently cut.</p>
                                        <!--<button type="button" class="btn btn-outline-light mt-2">View Details</button>-->
                                </div>
                        </div>
                </div>
        </div>




        <div class="col-lg-4 mb-4 delayyy-s50" data-animate="bounceIn">
                <div class="flip-card text-center">
                        <div class="flip-card-front dark" style="background-image: url('images/later/flip_boxes/3.jpg')">
                                <div class="flip-card-inner">
                                        <div class="card nobg noborder text-center">
                                                <div class="card-body">
                                                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                                width="150" height="150"
                                                viewBox="0 0 224 224"
                                                style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g><g id="surface1"><path d="M162.85938,135.80729c-12.59636,-12.61458 -33.1224,-12.61458 -45.71875,0l-18.66667,18.66667c-7.94792,7.92968 -21,7.92968 -28.94792,0c-7.92969,-7.94792 -7.92969,-21 0,-28.94792l21,-21l-13.05208,-13.05208l-21,21c-15.40365,15.38542 -15.40365,40.12239 0,55.05208c15.38542,15.40364 40.12239,15.40364 55.05208,0l18.66667,-18.66667c5.61458,-5.1224 14,-5.1224 19.61458,0c5.12239,5.61458 5.12239,14 0,19.61458l-23.80729,23.78906l13.07031,13.07031l23.78907,-23.80729c12.61458,-12.59636 12.61458,-33.1224 0,-45.71875z" fill="#37474f"></path><path d="M75.14063,106.85938c-13.07032,-13.05208 -13.07032,-34.05208 0,-46.66667l31.71875,-31.71875c13.07031,-13.07032 34.07031,-13.07032 46.66667,0c13.07031,13.05208 13.07031,34.05208 0,46.66667l-31.71875,31.71875c-13.07032,13.07031 -33.61458,13.07031 -46.66667,0z" fill="#0277bd"></path><path d="M149.33333,51.33333c0,10.31771 -8.34896,18.66667 -18.66667,18.66667c-10.31771,0 -18.66667,-8.34896 -18.66667,-18.66667c0,-10.31771 8.34896,-18.66667 18.66667,-18.66667c10.31771,0 18.66667,8.34896 18.66667,18.66667z" fill="#b3e5fc"></path></g></g></g></svg>
                                                <h3 class="card-title">Fiber-optic</h3>
                                                        <!--<p class="card-text t400">With supporting text below as a natural lead-in to additional content.</p>-->
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <div class="flip-card-back bg-danger no-after">
                                <div class="flip-card-inner">
                                        <p class="mb-2 text-white">Fiber-optic provides signals as light via glass or plastic wires. It is mostly available in big cities and a bit expensive. Hi-tech digital service providers offer broadband connection via a hybrid system to reduce the cost. Also, signal distortion issues are very rare.</p>
                                </div>
                        </div>
                </div>
        </div>
</div>

<br>
<br>

<div class="col_one_sixth center align-items-center" data-animate="fadeInLeft">
    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
    width="180" height="180"
    viewBox="0 0 224 224"
    style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g><g id="surface1"><path d="M200.66667,112c0,48.96354 -39.70312,88.66667 -88.66667,88.66667c-48.96354,0 -88.66667,-39.70312 -88.66667,-88.66667c0,-48.96354 39.70313,-88.66667 88.66667,-88.66667c48.96354,0 88.66667,39.70313 88.66667,88.66667" fill="#dd8903"></path><path d="M179.13802,54.19531c13.38021,15.53125 21.52864,35.69271 21.52864,57.80469c0,48.96354 -39.70312,88.66667 -88.66667,88.66667c-22.11198,0 -42.27344,-8.14844 -57.80469,-21.52864c16.26042,18.86719 40.26823,30.86198 67.13802,30.86198c48.96354,0 88.66667,-39.70312 88.66667,-88.66667c0,-26.86979 -11.99479,-50.87761 -30.86198,-67.13802z" fill="#ffab00"></path><path d="M191.33333,102.66667c0,48.96354 -39.70312,88.66667 -88.66667,88.66667c-48.96354,0 -88.66667,-39.70312 -88.66667,-88.66667c0,-48.96354 39.70313,-88.66667 88.66667,-88.66667c48.96354,0 88.66667,39.70313 88.66667,88.66667" fill="#ffab00"></path><path d="M102.66667,32.66667c-38.66406,0 -70,31.33594 -70,70c0,38.66406 31.33594,70 70,70c38.66406,0 70,-31.33594 70,-70c0,-38.66406 -31.33594,-70 -70,-70zM162.47656,92.45833l-9.20573,1.54948c-0.47396,-2.77083 -1.16667,-5.52343 -2.09636,-8.20312l8.80469,-3.0625c1.11198,3.15364 1.93229,6.43489 2.49739,9.71614zM155.91406,73.59114l-8.18489,4.48438c-1.34896,-2.47917 -2.91667,-4.86719 -4.66667,-7.07292l7.34636,-5.76042c2.05989,2.60677 3.90104,5.41406 5.50521,8.34896zM107.47917,42.18229l-0.74739,9.3151c-2.80729,-0.23698 -5.66927,-0.20052 -8.47656,0.01823l-0.76563,-9.29688c3.29948,-0.27344 6.70833,-0.29167 9.98958,-0.03646zM87.59114,43.87761l2.31511,9.05989c-2.73437,0.69271 -5.41406,1.62239 -8.00261,2.77083l-3.77343,-8.53125c3.04427,-1.36719 6.23437,-2.46094 9.46093,-3.29948zM69.34375,51.97136l5.14063,7.78386c-2.36979,1.54948 -4.59375,3.31771 -6.67188,5.23177l-6.34375,-6.85417c2.44271,-2.26042 5.10417,-4.33854 7.875,-6.16146zM54.6875,65.51563l7.40104,5.70573c-1.75,2.26042 -3.29948,4.64844 -4.63021,7.12761l-8.22136,-4.44792c1.56771,-2.89844 3.40886,-5.72396 5.45052,-8.38542zM45.22656,83.10677l8.84115,3.00781c-0.91146,2.67969 -1.60417,5.43229 -2.0599,8.20313l-9.20573,-1.49479c0.52864,-3.28125 1.34896,-6.5625 2.42448,-9.71614zM42,102.79427l9.33333,-0.03646c0.01823,2.84375 0.25521,5.6875 0.71094,8.47657l-9.20573,1.53125c-0.54688,-3.26302 -0.82032,-6.63542 -0.83854,-9.97136zM49.34636,131.63281c-1.60417,-2.93489 -2.95313,-6.03385 -4.04688,-9.16927l8.82292,-3.02604c0.92969,2.64323 2.0599,5.25 3.40886,7.72917zM61.65104,147.34636c-2.47917,-2.26042 -4.75782,-4.73958 -6.79948,-7.34636l7.34636,-5.74219c1.73177,2.20573 3.66406,4.30208 5.76042,6.23438zM78.34896,158.26563c-3.04427,-1.33073 -6.03386,-2.95313 -8.80469,-4.75782l5.10417,-7.82031c2.35156,1.54948 4.86719,2.89844 7.45573,4.02864zM97.72656,163.13281c-3.33593,-0.27343 -6.65364,-0.82031 -9.88021,-1.62239l2.27864,-9.04167c2.71614,0.67448 5.52344,1.13021 8.34896,1.36719zM98,135.33333v-48.01562l-14,4.41146v-11.66667l26.70573,-10.0625h1.29427v65.33333zM107.73438,163.13281l-0.78386,-9.29687c2.82552,-0.23698 5.63281,-0.71094 8.36719,-1.40364l2.27864,9.04167c-3.20833,0.82031 -6.54427,1.38542 -9.86198,1.65885zM115.09896,52.84636l2.26042,-9.05989c3.22656,0.82031 6.43489,1.89583 9.49739,3.22656l-3.73698,8.54948c-2.58854,-1.13021 -5.26823,-2.02344 -8.02083,-2.71614zM127.09375,158.21094l-3.77344,-8.53125c2.58854,-1.14844 5.08594,-2.51563 7.4375,-4.04688l5.1224,7.80208c-2.77083,1.82292 -5.72396,3.42708 -8.78646,4.77604zM130.59375,59.59114l5.08594,-7.82031c2.78906,1.80469 5.45052,3.86458 7.91146,6.125l-6.28906,6.89063c-2.07812,-1.91407 -4.33854,-3.66407 -6.70833,-5.19532zM143.77344,147.29167l-6.32552,-6.87239c2.09636,-1.91406 4.01042,-4.01042 5.74219,-6.23438l7.36458,5.74219c-2.04167,2.60677 -4.32031,5.08594 -6.78125,7.36458zM156.04167,131.54167l-8.20312,-4.44792c1.34896,-2.49739 2.49739,-5.10417 3.39062,-7.76562l8.84114,3.02604c-1.07552,3.15364 -2.44271,6.23438 -4.02864,9.1875zM153.30729,111.125c0.45573,-2.78906 0.69271,-5.63281 0.69271,-8.45833l7.94792,-0.20052l1.36719,0.01823c0.01823,3.51823 -0.25521,6.87239 -0.80208,10.15364z" fill="#ffcc80"></path></g></g></g></svg>
</div>
<div class="col_five_sixth col_last" data-animate="">
    <h4>Clear Prices by the Providers</h4>
    <p align="justify">
Many service providers charge an installation or equipment fee before the actual running of the service. The charges can reach up to $100.00. However, if you use the bundle option, you can get discounted offers. It is clearly a burden on the customer. $100 is not a small amount when you have to pay in bulk for a subscription. Whichever provider offers flexible prices in terms of equipment, installation charges, and customer support, it vouches in favor of the customers, entirely. For those who are not accustomed to such amenities are at a loss because discounts and giveaways have a greater impact than we can imagine! If a company is not willing to share the load of a subscription, it doesn’t sit right with the customer.        
    </p>
</div>

<div class="col_one_sixth col_last center align-items-center" data-animate="fadeInRight">
    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
    width="180" height="180"
    viewBox="0 0 224 224"
    style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g id="Layer_1"><g id="surface1"><path d="M51.33333,158.66667c-2.8,0 -4.66667,-2.33333 -4.66667,-4.66667v-84c0,-2.8 1.86667,-4.66667 4.66667,-4.66667c2.8,0 4.66667,1.86667 4.66667,4.66667v84c0,2.33333 -1.86667,4.66667 -4.66667,4.66667z" fill="#455a64"></path><path d="M79.33333,158.66667c-2.8,0 -4.66667,-2.33333 -4.66667,-4.66667v-84c0,-2.8 2.33333,-4.66667 4.66667,-4.66667c2.33333,0 4.66667,1.86667 4.66667,4.66667v84c0,2.33333 -2.33333,4.66667 -4.66667,4.66667z" fill="#455a64"></path><path d="M182,200.66667h-140c-7.93333,0 -14,-6.06667 -14,-14v-23.33333c0,-7.93333 6.06667,-14 14,-14h140c7.93333,0 14,6.06667 14,14v23.33333c0,7.93333 -6.06667,14 -14,14z" fill="#78909c"></path><path d="M60.66667,175c0,3.73333 -3.26667,7 -7,7c-3.73333,0 -7,-3.26667 -7,-7c0,-3.73333 3.26667,-7 7,-7c3.73333,0 7,3.26667 7,7z" fill="#64dd17"></path><path d="M84,175c0,3.73333 -3.26667,7 -7,7c-3.73333,0 -7,-3.26667 -7,-7c0,-3.73333 3.26667,-7 7,-7c3.73333,0 7,3.26667 7,7z" fill="#64dd17"></path><path d="M107.33333,175c0,3.73333 -3.26667,7 -7,7c-3.73333,0 -7,-3.26667 -7,-7c0,-3.73333 3.26667,-7 7,-7c3.73333,0 7,3.26667 7,7z" fill="#ecf0f1"></path><path d="M130.66667,175c0,3.73333 -3.26667,7 -7,7c-3.73333,0 -7,-3.26667 -7,-7c0,-3.73333 3.26667,-7 7,-7c3.73333,0 7,3.26667 7,7z" fill="#ecf0f1"></path><path d="M127.86667,109.2c-1.4,0 -2.8,-0.46667 -3.73333,-1.4c-1.86667,-1.86667 -1.4,-4.66667 0.46667,-6.53333c9.8,-8.4 15.4,-20.06667 15.4,-33.13333c0,-12.6 -5.6,-25.2 -15.4,-33.13333c-1.86667,-1.86667 -2.33333,-4.66667 -0.46667,-6.53333c1.86667,-1.86667 4.66667,-2.33333 6.53333,-0.46667c11.66667,9.33333 18.66667,24.26667 18.66667,39.66667c0,15.4 -6.53333,30.33333 -18.2,40.6c-0.93333,0.46667 -2.33333,0.93333 -3.26667,0.93333z" fill="#3498db"></path><path d="M114.8,96.13333c-1.4,0 -2.8,-0.46667 -3.73333,-1.86667c-1.4,-1.86667 -1.4,-5.13333 0.93333,-6.53333c6.06667,-4.66667 9.33333,-12.13333 9.33333,-20.06667c0,-7.93333 -3.26667,-15.4 -9.33333,-20.06667c-1.86667,-1.4 -2.33333,-4.66667 -0.93333,-6.53333c1.4,-1.86667 4.66667,-2.33333 6.53333,-0.93333c8.4,6.53333 13.06667,16.33333 13.06667,27.53333c0,10.73333 -4.66667,21 -13.06667,27.53333c-0.93333,0.46667 -1.86667,0.93333 -2.8,0.93333z" fill="#3498db"></path><path d="M101.26667,82.6c-1.86667,0 -3.26667,-0.93333 -4.2,-2.8c-0.93333,-2.33333 0,-5.13333 2.33333,-6.06667c1.86667,-0.93333 3.26667,-3.73333 3.26667,-6.06667c0,-2.8 -1.4,-5.13333 -3.26667,-6.06667c-2.33333,-0.93333 -3.26667,-3.73333 -2.33333,-6.06667c0.93333,-2.33333 3.73333,-3.26667 6.06667,-2.33333c5.6,2.8 8.86667,7.93333 8.86667,14.46667c0,6.06667 -3.26667,11.66667 -8.86667,14.46667c-0.46667,0.46667 -1.4,0.46667 -1.86667,0.46667z" fill="#3498db"></path></g></g></g></svg>
</div>
<div class="col_five_sixth " data-animate="">
    <h4>What Kind Of Equipment Is There?</h4>
    <p align="justify">    
Cable TV and Internet packages with FREE equipment and other niceties are acceptable to the modern customer. Because you’re paying a reasonable amount for the service, getting some extra value to your package isn’t bad. You can go for hi-end brands as the FREE equipment factor is attached to them. The kind of equipment that comes with a TV, Internet, and a phone bundle is in the form of a modem, a DVR device, and other related hardware. We are not only here for providers with FREE equipment, but also for those services, which do not compromise on quality. Their criterion to sell is based on quality and not just the giveaways.        
    </p>
</div>


<div class="col_one_sixth center align-items-center" data-animate="fadeInLeft">
    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
    width="180" height="180"
    viewBox="0 0 224 224"
    style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g><g id="surface1"><path d="M42,18.66667h140v186.66667h-140z" fill="#fbe9e7"></path><path d="M140,172.66667h13.07031l-23.33333,-23.33333l-13.07031,13.07031l23.33333,23.33333z" fill="#f4511e"></path><path d="M84,172.66667h-13.07031l23.33333,-23.33333l13.07031,13.07031l-23.33333,23.33333z" fill="#f4511e"></path><path d="M70,60.66667h84v18.66667h-84z" fill="#ff8a65"></path><path d="M70,93.33333h84v9.33333h-84z" fill="#ff8a65"></path><path d="M112,121.33333c-13.07031,0 -23.33333,10.26302 -23.33333,23.33333c0,13.07031 10.26302,23.33333 23.33333,23.33333c13.07031,0 23.33333,-10.26302 23.33333,-23.33333c0,-13.07031 -10.26302,-23.33333 -23.33333,-23.33333zM112,158.66667c-7.92969,0 -14,-6.07031 -14,-14c0,-7.92969 6.07031,-14 14,-14c7.92969,0 14,6.07031 14,14c0,7.92969 -6.07031,14 -14,14z" fill="#ff8a65"></path><path d="M37.33333,14v196h149.33333v-196zM177.33333,186.66667c-7.92969,0 -14,6.07031 -14,14h-102.66667c0,-7.92969 -6.07031,-14 -14,-14v-149.33333c7.92969,0 14,-6.07031 14,-14h102.66667c0,7.92969 6.07031,14 14,14z" fill="#ff8a65"></path></g></g></g></svg>
</div>
<div class="col_five_sixth col_last" data-animate="">
    <h4>Terms & Conditions by Service Providers</h4>
    <p align="justify">
Carefully going through the contract’s terms and conditions is must before purchasing any service plan. Generally, TV-service providers contract on a yearly basis. However, satellite TV contracts are for two-years. Customers should not have any problem with the length of the contract’s and the promotional prices’ time. Shorter promotional pricing period than the contract can make them pay more in between your contract; hence, contracts should be clear. For people who don’t want to stick with a contract and are always on the go, we have amazing deals for them. It is indeed a comfortable choice for customers with any specific place to live in.    
    </p>
</div>


<div class="col_one_sixth center align-items-center col_last" data-animate="fadeInRight">
    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
    width="180"
    viewBox="0 0 224 224"
    style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g><g id="surface1"><path d="M18.66667,130.66667v-93.33333c0,-10.26302 8.40364,-18.66667 18.66667,-18.66667h130.66667c10.26302,0 18.66667,8.40364 18.66667,18.66667v93.33333c0,10.26302 -8.40364,18.66667 -18.66667,18.66667h-130.66667c-10.26302,0 -18.66667,-8.40364 -18.66667,-18.66667z" fill="#34495e"></path><path d="M168,32.66667h-130.66667c-2.80729,0 -4.66667,1.85938 -4.66667,4.66667v93.33333c0,2.80729 1.85938,4.66667 4.66667,4.66667h130.66667c2.80729,0 4.66667,-1.85937 4.66667,-4.66667v-93.33333c0,-2.80729 -1.85937,-4.66667 -4.66667,-4.66667z" fill="#bbdefb"></path><path d="M177.33333,154h-149.33333c-10.26302,0 -18.66667,-8.40364 -18.66667,-18.66667h186.66667c0,10.26302 -8.40364,18.66667 -18.66667,18.66667z" fill="#37474f"></path><path d="M112,186.66667v-112c0,-10.26302 8.40364,-18.66667 18.66667,-18.66667h56c10.26302,0 18.66667,8.40364 18.66667,18.66667v112c0,10.26302 -8.40364,18.66667 -18.66667,18.66667h-56c-10.26302,0 -18.66667,-8.40364 -18.66667,-18.66667z" fill="#333333"></path><path d="M186.66667,70h-56c-2.80729,0 -4.66667,1.85938 -4.66667,4.66667v102.66667c0,2.80729 1.85938,4.66667 4.66667,4.66667h56c2.80729,0 4.66667,-1.85937 4.66667,-4.66667v-102.66667c0,-2.80729 -1.85937,-4.66667 -4.66667,-4.66667z" fill="#fff3e0"></path><path d="M165.66667,193.66667c0,3.86458 -3.13542,7 -7,7c-3.86458,0 -7,-3.13542 -7,-7c0,-3.86458 3.13542,-7 7,-7c3.86458,0 7,3.13542 7,7z" fill="#1abc9c"></path></g></g></g></svg>
</div>
<div class="col_five_sixth" data-animate="">
    <h4>Service that Entertains You All the Time</h4> 
    <p align="justify">
People are not always in front of TV screens, holding remote to entertain themselves. With the technology advancements, the mobile phone has become the vantage point of all entertainment and information. Thus, many TV-service providers now offer TV- apps along with the service so that, you never miss any show, news, or sports event. These apps allow you to watch more channels on the Home-WIFI system comparatively. Before making the deal done, make sure that you carefully read all the reviews and available channels on your app.
    </p>
</div>


</div>
</div>
            
            
            
<!-- PROMOBOX 2 START -->                
<div class="section clear-bottommargin-lg" style="background-color: #EBA74B">
    <div class="container">
        <div class="row center justify-content-center">
                <div class="col-md-auto">
                        <h2 class="t700 text-white mb-3"   data-animate="bounceInRight">Know About the Top TV-Service Providers that Lead to Fuss-Free TV Entertainment!</h2>
                        <h4 class="text-white"   data-animate="bounceInLeft">We provide you with plenty of information about the epic TV-services, So that, you make an educated choice.</h4>
                </div>
            <div class="subscribe-widget clear-bottommargin align-items-center center"   data-animate="flipInX">
                <div class="widget-subscribe-form-result"></div>
                <form id="zipform2" action="include/zip-processor.php" method="get">
                    <input class="input-number–noSpinners form-control-lg postision____absoult_____top" type="text" name="zipcode" placeholder="Enter Zip Code">
                    <a style="margin-top:1px;" href="javascript:{}" onclick="document.getElementById('zipform2').submit();" class="button button-rounded button-reveal button-large button-dirtygreen"><i class="icon-map-marker2"></i><span>Find Area</span></a>
                </form>
            </div>

        </div>
    </div>
</div>
<!-- PROMOBOX 2 END   -->  


</div>
</section> 

			</div>

                </section>
                <!-- #content end -->

                
<?php require_once 'views/footer.php'; ?>