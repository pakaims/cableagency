<?php session_start(); ?>
<?php include "views/header.php";?>
<?php 
    
    if (isset($_SESSION["errors"])) {
    $errors = $_SESSION["errors"];
    $message = $errors;
    session_destroy();
    echo "<script type='text/javascript'>alert('Sorry you have enter an invalid zip code!');</script>";

}elseif (isset($_SESSION["mailmessage"])) {
    session_destroy();
    echo "<script type='text/javascript'>alert('Thanks, Your information is submitted. Relevant company representative will get back to you soon!');</script>";
}

?>


                <!-- #header end --><!-- #header end -->
	<!-- <div class="bg___imge_banner">

	</div> -->

	<div class="header">
	<div class="vertical-middle ignore-header dark">
				<div class="container">
	<div class="row">
	<div class="col-md-4"></div>
	<div class="col-md-4">
	<form action="include/zip-processor.php" method="get" class="topmargin-lg landing-wide-form landing-form-overlay dark nobottommargin clearfix">
						<div class="heading-block nobottommargin nobottomborder center align-items-center">
							<h2>Search for Cable TV and Internet Providers in Your Area</h2>
							<!--<span>Enter your zip code to view providers available in your area.</span>-->
						</div>
						<div class="line" style="margin: 20px 0 30px;"></div>
						<div class="col_full">
                                                    <input type="number" name="zipcode" maxlength="5" pattern="[0-9]{5}" required="true" class="form-control required form-control-lg not-dark input-number–noSpinners" value="" placeholder="Zip Code*">
                                                        <!--<input type="text" name="zipcode">-->
						</div>
						<div class="col_full nobottommargin">
							<button class="btn btn-lg btn-danger btn-block nomargin" value="submit" type="submit" style="">FIND</button>
                                                        <!--<button type="submit">submit</button>-->
						</div>
					</form>
	</div>
	</div>
												
						</div>
					</div>
				</div>
						
								<!-- Content
		============================================= -->
<section id="content">
    <div class="content-wrap nopadding">
        <div class="container clearfix">
        <br>    
        <br>    
        <div class="nobottommargin topmargin-lg">
                                    
                                    
                                    
            <!--SECTION 1 DESC/PICTURE-->
                <div class="col_two_third" data-animate="">
                        <h3>Find Suitable TV and Internet Providers and Compare them by Zip Code</h3>
                        <p align="justify">
CableAgency.com comes out as a name that supports the best brands in terms of TV, Internet, and phone service providers. Not only does it search for Internet Service Providers (ISPs) and Cable TV services, but it also compares them for efficiency and affordability. For anyone interested in a reliable landline, they can compare services from a wide range of telecommunication companies. The whole idea is to stay authentic and customer-centric since there are a ton of companies flaunting their services. And, it becomes difficult to differentiate between the good ones and the bad ones. Read more about us.
                        </p>
                </div>
                <div class="col_one_third col_last center align-items-center topmargin-sm" data-animate="bounceIn">
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                    width="250" height="250"
                    viewBox="0 0 224 224"
                    style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g><g id="surface1"><path d="M172.66667,84h28v112h-28z" fill="#00bcd4"></path><path d="M135.33333,121.33333h28v74.66667h-28z" fill="#00bcd4"></path><path d="M98,102.66667h28v93.33333h-28z" fill="#00bcd4"></path><path d="M60.66667,149.33333h28v46.66667h-28z" fill="#00bcd4"></path><path d="M23.33333,130.66667h28v65.33333h-28z" fill="#00bcd4"></path><path d="M51.33333,74.66667c0,7.72917 -6.27083,14 -14,14c-7.72917,0 -14,-6.27083 -14,-14c0,-7.72917 6.27083,-14 14,-14c7.72917,0 14,6.27083 14,14z" fill="#3f51b5"></path><path d="M88.66667,84c0,7.72917 -6.27083,14 -14,14c-7.72917,0 -14,-6.27083 -14,-14c0,-7.72917 6.27083,-14 14,-14c7.72917,0 14,6.27083 14,14z" fill="#3f51b5"></path><path d="M126,51.33333c0,7.72917 -6.27083,14 -14,14c-7.72917,0 -14,-6.27083 -14,-14c0,-7.72917 6.27083,-14 14,-14c7.72917,0 14,6.27083 14,14z" fill="#3f51b5"></path><path d="M163.33333,60.66667c0,7.72917 -6.27083,14 -14,14c-7.72917,0 -14,-6.27083 -14,-14c0,-7.72917 6.27083,-14 14,-14c7.72917,0 14,6.27083 14,14z" fill="#3f51b5"></path><path d="M200.66667,42c0,7.72917 -6.27083,14 -14,14c-7.72917,0 -14,-6.27083 -14,-14c0,-7.72917 6.27083,-14 14,-14c7.72917,0 14,6.27083 14,14z" fill="#3f51b5"></path><path d="M182.47396,33.59636l-34.07032,17.26302l-38.73698,-9.78907l-37.33333,32.66667l-32.66667,-7.92968l-4.66667,17.71875l42,10.73698l37.33333,-32.66667l35.92969,8.87761l40.59636,-20.07032z" fill="#3f51b5"></path></g></g></g></svg>
                    <!--<img class="alignright img-responsive" src="images/landing/responsive.png" alt="">-->	
                </div>
                <div class="divider" data-animate="bounceInLeft"><i class="icon-circle"></i></div>
                <div class="clear"></div>




            <!-- SECTION MAP -->
                        <div class="heading-block center topmargin-lg divcenter nobottomborder clearfix">
                                <h2>Click on the desired state to search for the best TV & Internet deals in town</h2>
                        </div>           
                <div id="map" class="center align-items-center" data-animate="fadeInUp"></div>
                <div class="divider" data-animate="bounceInRight"><i class="icon-circle"></i></div>











                
                
                         
            <!--SECTION 2 PICTURE/DESC-->                                        
                <div class="col_one_third center align-items-center" data-animate="bounceIn">
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                    width="230" height="230"
                    viewBox="0 0 224 224"
                    style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g id="Layer_1"><circle cx="24" cy="24" transform="scale(4.66667,4.66667)" r="18" fill="#eceff1"></circle><path d="M112,205.33333c-51.464,0 -93.33333,-41.86933 -93.33333,-93.33333c0,-51.464 41.86933,-93.33333 93.33333,-93.33333c51.464,0 93.33333,41.86933 93.33333,93.33333c0,51.464 -41.86933,93.33333 -93.33333,93.33333zM112,37.33333c-41.16933,0 -74.66667,33.49733 -74.66667,74.66667c0,41.16933 33.49733,74.66667 74.66667,74.66667c41.16933,0 74.66667,-33.49733 74.66667,-74.66667c0,-41.16933 -33.49733,-74.66667 -74.66667,-74.66667z" fill="#607d8b"></path><circle cx="24" cy="24" transform="scale(4.66667,4.66667)" r="12" fill="#4caf50"></circle><path d="M107.91667,143.86867l-29.932,-29.93667l13.19733,-13.19733l16.73467,16.73l49.10733,-49.10733l13.202,13.202z" fill="#eceff1"></path></g></g></svg>
                    <!--<img class="alignright img-responsive" src="images/landing/responsive.png" alt="">-->	
                </div>

                <div class="col_two_third  col_last" data-animate="">
                        <h3 >The Right TV and Internet Service Balances Your Life</h3>
                        <p align="justify">
That is what we are here for! To suggest TV, Internet and phone services with the right focus, enough freedom to deliver independence and pleasure, above all. We will amplify your happy hormones and dopamine levels as if you just had a fulfilling cardio session. Once you place the zip code in, CableAgency.com gets to work and searches for ISPs and cable TV companies in the area. You have the relevant information at your disposal from channel lineups, customer reviews to the pros and budget requirements. </p>
                </div>                                        
                <div class="divider" data-animate="bounceInRight"><i class="icon-circle"></i></div>
                                     
            <!--SECTION 3 DESC/PICTURE-->
                <div class="col_two_third" data-animate="">
                        <h3>How Many Cable Companies Serve In My Area?</h3>
                        <p align="justify">
One purpose of CableAgency.com is to let you explore all the cable TV options in your vicinity. If you move a lot, this particular feature comes in handy when you have no clue about the TV companies and Internet service providers in that area. When we say cable TV, we mean to explain the type of subscribable TV service(s) in a specific area. That’s the reason behind the diversity you see at CableAgency.com. The TV and Internet providers listed here include satellite companies, conventional cable corporations, digital service providers, who function in the optic fiber category. If you don’t get anything with the search, call us, and we’ll have a go at it and do our best to turn things around you.</p>
                </div>
                <div class="col_one_third col_last center align-items-center" data-animate="bounceIn">
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
width="250" height="250"
viewBox="0 0 224 224"
style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g><g id="surface1"><path d="M196,165.66667c0,-16.75261 -37.60677,-30.33333 -84,-30.33333c-46.39323,0 -84,13.58073 -84,30.33333c0,16.75261 37.60677,30.33333 84,30.33333c46.39323,0 84,-13.58073 84,-30.33333z" fill="#90caf9"></path><path d="M112,28c-28.34636,0 -51.33333,22.67708 -51.33333,50.64063c0,27.96354 51.33333,94.02604 51.33333,94.02604c0,0 51.33333,-66.0625 51.33333,-94.02604c0,-27.96354 -22.98698,-50.64063 -51.33333,-50.64063zM112,102.66667c-12.88802,0 -23.33333,-10.44531 -23.33333,-23.33333c0,-12.88802 10.44531,-23.33333 23.33333,-23.33333c12.88802,0 23.33333,10.44531 23.33333,23.33333c0,12.88802 -10.44531,23.33333 -23.33333,23.33333z" fill="#ff3d00"></path></g></g></g></svg>
                    <!--<img class="alignright img-responsive" src="images/landing/responsive.png" alt="">-->	
                </div>
                <div class="divider" data-animate="bounceInLeft"><i class="icon-circle"></i></div>
                <div class="clear"></div>
                                        
                                        
                                        
            <!--SECTION 4 PICTURE/DESC-->                                        
                <div class="col_one_third topmargin-sm center align-items-center" data-animate="bounceIn">
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                    width="250" height="250"
                    viewBox="0 0 224 224"
                    style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g id="Layer_1_1_"><circle cx="24" cy="24" transform="scale(4.66667,4.66667)" r="20" fill="#1565c0"></circle><path d="M150.73333,67.66667l-9.33333,7c-1.4,1.4 -1.86667,3.26667 -1.4,5.13333l4.2,5.13333c0.46667,1.86667 -0.46667,4.2 -2.33333,5.13333c-2.33333,1.4 -7.93333,4.2 -7.93333,5.6c0,3.26667 2.33333,11.2 4.2,11.2c14.46667,0 5.6,-6.53333 15.86667,-13.06667c6.06667,-3.73333 15.86667,-5.6 18.2,-5.6h9.33333v65.33333h-32.66667c0,0 5.6,8.86667 10.73333,18.66667c2.33333,4.66667 7.93333,14 7.93333,14c23.8,-18.2 37.33333,-45.73333 37.33333,-74.66667c0,-29.86667 -14.46667,-56.93333 -36.4,-73.73333l-13.53333,23.8c-0.93333,2.8 -2.33333,4.66667 -4.2,6.06667z" fill="#43a047"></path><path d="M65.8,116.66667c4.2,0 4.2,-3.73333 4.2,-9.33333c0,-5.6 -0.93333,-14 14,-14c6.06667,0 14,0 14,-9.33333c0,-3.73333 0,-18.66667 0,-23.33333c0,-1.4 -0.46667,-7.46667 -9.8,-7.46667c-7.93333,0 -14,-1.4 -14,-7c0,-4.2 3.26667,-14 9.8,-22.86667c-34.53333,10.73333 -59.73333,40.6 -64.4,77l-0.93333,7c10.26667,0 36.4,0 37.33333,0c7,0 5.6,9.33333 9.8,9.33333z" fill="#43a047"></path><path d="M107.33333,144.66667c-5.6,0 -9.33333,0 -15.86667,-7.93333c-2.8,-2.8 -7,-6.06667 -10.26667,-6.06667c-3.26667,0 -7.46667,0 -7.46667,0h-3.73333l-4.66667,4.66667l9.33333,32.66667v29.4c4.66667,1.86667 9.33333,3.73333 14,5.13333c1.86667,-14.46667 13.06667,-31.73333 14,-34.53333c0.93333,-1.86667 4.66667,-10.73333 4.66667,-10.73333z" fill="#43a047"></path><path d="M172.66667,88.66667c0,0 4.66667,4.66667 4.66667,18.66667c0,4.66667 -9.33333,9.33333 -23.33333,9.33333c-23.33333,0 -23.33333,13.53333 -23.33333,18.66667c0,16.8 18.66667,18.66667 18.66667,18.66667c0,0 18.66667,0 32.66667,18.66667l0.46667,0.46667c14,-16.33333 22.86667,-37.8 22.86667,-61.13333c0,-15.86667 -4.2,-31.26667 -11.2,-44.33333l-1.4,-2.33333h-1.4c-14,0 -12.6,18.66667 -18.66667,23.33333z" fill="#7cb342"></path><path d="M28,112c2.8,5.6 9.8,10.73333 15.4,14.93333c3.73333,2.8 17.26667,8.4 17.26667,17.73333c0,15.86667 9.33333,32.66667 9.33333,42v8.4l9.33333,4.2c0,0 0,-6.06667 0,-7.93333c0,-14 15.86667,-23.33333 23.33333,-23.33333c0,0 9.33333,-0.93333 9.33333,-16.33333c0,-1.86667 -1.86667,-7 -4.66667,-7c0,0 -2.33333,9.33333 -20.06667,9.33333c-9.33333,0 -17.73333,-5.13333 -17.73333,-23.33333h-8.86667c-2.8,0 -5.13333,-3.73333 -5.13333,-6.06667c0,-2.33333 -0.46667,-3.26667 -4.2,-3.26667c-2.8,0 -4.66667,-1.4 -4.66667,-3.73333c0,-1.86667 0,-4.2 0,-4.66667c0,-2.8 2.33333,-6.06667 9.33333,-6.06667c0,-8.4 -9.33333,-14 -9.33333,-21c0,-6.53333 -4.66667,-16.33333 -14,-21l-1.4,-0.46667c0,0 -9.8,16.8 -11.66667,35z" fill="#7cb342"></path><path d="M112,210c-54.13333,0 -98,-43.86667 -98,-98c0,-54.13333 43.86667,-98 98,-98c54.13333,0 98,43.86667 98,98c0,54.13333 -43.86667,98 -98,98zM112,28c-46.2,0 -84,37.8 -84,84c0,46.2 37.8,84 84,84c46.2,0 84,-37.8 84,-84c0,-46.2 -37.8,-84 -84,-84z" fill="#bbdefb"></path></g></g></svg>
                    <!--<img class="alignright img-responsive" src="images/landing/responsive.png" alt="">-->	
                </div>

                <div class="col_two_third  col_last" data-animate="">
                        <h3>How Can I Connect With The World?</h3>
                        <p align="justify">
The Internet is the next phase of communication after the telephone. Regardless of how much you use it, the Internet has become one of the household essentials today. Hence, make sure you have the best Internet in town. That is where we come in and search for you different types of ISPs in your area. The Internet types include DSL, fiber, and satellite connections so far. There is no fourth type yet! However, scientific advancements in the communications sector may introduce a fourth type in the future. An ideal situation here is to bundle up the Internet, TV, and phone services. A bundle sounds much affordable when you compare it with the services individually. The discounts are enough to create a long-term benefit for you in terms of both free equipment and monthly rates. Conclusively, we help you select the best package for home and office.
                        </p>
                </div>                                        
                                        
        </div>
                                    

					<!-- Slider negetive Box
					============================================= -->
<!--					<div class="row justify-content-center slider-box-wrap clearfix">
						<div class="col-10">
							<div class="slider-bottom-box">
								<div class="row align-items-center clearfix">
									<div class="col-lg-4 mb-3 mb-lg-0">
										<h2 class="mb-3 h1 t300">Features</h2>
										<p class="t400 text-muted mb-0">Enthusiastically orchestrate orthogonal core competencies before processes.</p>
									</div>
									<div class="col-lg-4 col-sm-6">
										<ul class="iconlist m-0">
											<li><img src="demos/hosting/images/svg/checked.svg" width="20" height="20" alt="" class="mr-2">Assertively Fabricate.</li>
											<li class="pt-3"><img src="demos/hosting/images/svg/checked.svg" width="20" height="20" alt="" class="mr-2">Distinctively disseminate.</li>
											<li class="pt-3"><img src="demos/hosting/images/svg/checked.svg" width="20" height="20" alt="" class="mr-2">Appropriately predominate.</li>
										</ul>
									</div>
									<div class="col-lg-4 col-sm-6">
										<ul class="iconlist m-0">
											<li class="pt-3 pt-lg-0"><img src="demos/hosting/images/svg/checked.svg" width="20" height="20" alt="" class="mr-2">Competently facilitate synergist</li>
											<li class="pt-3"><img src="demos/hosting/images/svg/checked.svg" width="20" height="20" alt="" class="mr-2">Front-end leadership skills.</li>
											<li class="pt-3"><img src="demos/hosting/images/svg/checked.svg" width="20" height="20" alt="" class="mr-2">Infomediaries with front-end</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>-->
</div>

                            
                            
<!-- PROMOBOX -->

   <div class="promo promo-dark promo-flat promo-full bottommargin text-center">
       <div class="container clearfix">
           <h3 data-animate="rubberBand" style="color: white">If there was a  <span>4th type</span> of Internet, <span>CableAgency.com</span> would have it.</h3>
               <!--<span>We strive to provide Our Customers with Top Notch Support to make their Theme Experience Wonderful</span>-->
               <!--<a href="#" class="button button-xlarge button-rounded">Start Now</a>-->
       </div>
   </div>   




<div class="container clearfix">                           
                            
                            
                            
                            
                            
                            
                            
                            
                            <!-- Features Section
					============================================= -->
					<div class="heading-block center topmargin-lg divcenter nobottomborder clearfix" style="max-width: 700px">
						<h2>We provide best services to you.</h2>
						<p>Competently recaptiualize multifunctional schemas without an expanded array of niches. Continually engage cooperative sources vis-a-vis web-enabled benefits.</p>
					</div>
					<div class="clear"></div>

					<div class="row grid-container" data-layout="masonry" style="overflow: visible">
						<div class="col-lg-4 mb-4" data-animate="bounceInLeft">
							<div class="flip-card text-center" >
								<div class="flip-card-front dark" style="background-image: url('images/later/flip_boxes/1.jpg')">
									<div class="flip-card-inner">
										<div class="card nobg noborder text-center">
											<div class="card-body">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                                                                            width="150" height="150"
                                                                                            viewBox="0 0 224 224"
                                                                                            style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g id="Layer_1"><g id="surface1"><path d="M97.06667,120.4c1.86667,-1.86667 4.66667,-1.86667 6.53333,0c1.86667,1.86667 1.86667,4.66667 0,6.53333l-6.53333,6.53333c-1.86667,1.86667 -4.66667,1.86667 -6.53333,0c-1.86667,-1.86667 -1.86667,-4.66667 0,-6.53333z" fill="#546e7a"></path><path d="M156.33333,173.13333l-33.13333,-33.13333l39.66667,-39.66667l33.13333,33.13333z" fill="#673ab7"></path><path d="M84,100.8l-33.13333,-33.13333l39.66667,-39.66667l33.13333,33.13333z" fill="#673ab7"></path><path d="M156.33333,160.06667l-6.53333,-6.53333l9.8,-9.8l6.53333,6.53333z" fill="#b39ddb"></path><path d="M143.26667,147l-6.53333,-7l9.8,-9.8l6.53333,6.53333z" fill="#b39ddb"></path><path d="M173.13333,143.26667l-6.53333,-6.53333l9.8,-9.8l6.53333,6.53333z" fill="#b39ddb"></path><path d="M159.6,130.2l-6.53333,-6.53333l9.8,-9.8l6.53333,6.53333z" fill="#b39ddb"></path><path d="M84,87.26667l-6.53333,-6.53333l9.8,-9.8l6.53333,6.53333z" fill="#b39ddb"></path><path d="M70.46667,74.2l-6.53333,-6.53333l9.8,-9.8l6.53333,6.53333z" fill="#b39ddb"></path><path d="M100.33333,70.93333l-6.53333,-6.53333l9.8,-9.8l6.53333,6.53333z" fill="#b39ddb"></path><path d="M87.26667,57.86667l-6.53333,-6.53333l9.8,-9.8l6.53333,6.53333z" fill="#b39ddb"></path><path d="M140,123.66667l-39.66667,-39.66667l6.53333,-6.53333l39.66667,39.66667z" fill="#78909c"></path><path d="M116.2,146.06667c1.86667,-10.26667 -0.93333,-21 -8.86667,-28.93333c-7.93333,-7.93333 -18.66667,-10.73333 -28.93333,-8.86667z" fill="#78909c"></path><path d="M93.33333,168.93333c-15.86667,0 -38.26667,-14.93333 -38.26667,-38.26667h9.33333c0,16.8 17.26667,28.93333 28.93333,28.93333z" fill="#90caf9"></path><path d="M93.33333,196c-35.93333,0 -65.33333,-29.4 -65.33333,-65.33333h9.33333c0,30.8 25.2,56 56,56z" fill="#90caf9"></path><path d="M133.46667,64.4c1.86667,-1.86667 4.66667,-1.86667 6.53333,0l19.6,19.6c1.86667,1.86667 1.86667,4.66667 0,6.53333l-36.4,36.4c-1.86667,1.86667 -4.66667,1.86667 -6.53333,0l-19.6,-19.6c-1.86667,-1.86667 -1.86667,-4.66667 0,-6.53333z" fill="#b0bec5"></path></g></g></g></svg>
                                                                                            <!--<i class="icon-line2-shuffle h1" ></i>-->
                                                                                            <h3 class="card-title">Satellite</h3>
                                                                                            <!--<p class="card-text t400">With supporting text below as a natural lead-in to additional content.</p>-->
											</div>
										</div>
									</div>
								</div>
								<div class="flip-card-back bg-danger no-after">
									<div class="flip-card-inner">
										<p class="mb-2 text-white">
Some of the top TV providers in any area offer satellite TV. The most talked-about companies offer grand TV viewing experience without wires. NFL Sunday Ticket option evenly challenges the ‘FREE premium channels’ option on cable TV. Non-stop connectivity to the world exceeds entertainment expectations.
                                                                                </p>
										<!--<button type="button" class="btn btn-outline-light mt-2">View Details</button>-->
									</div>
								</div>
							</div>
						</div>

						

                                            
                                            
                                                    <div class="col-lg-4 mb-4" data-animate="bounceInUp">
							<div class="flip-card text-center">
								<div class="flip-card-front dark" style="background-image: url('images/later/flip_boxes/2.jpg')">
									<div class="flip-card-inner">
										<div class="card nobg noborder text-center">
											<div class="card-body">
                                                                                            
                                                                                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                                                                            width="150" height="150 "
                                                                                            viewBox="0 0 224 224"
                                                                                            style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g id="Layer_1"><g id="surface1"><path d="M37.33333,130.66667h-9.33333c0,-18.66667 6.53333,-34.53333 19.6,-46.66667c35.93333,-33.6 108.26667,-28.46667 111.53333,-28l-0.46667,9.33333c-0.93333,0 -71.4,-4.66667 -104.53333,25.66667c-11.2,10.26667 -16.8,23.8 -16.8,39.66667z" fill="#1565c0"></path><path d="M77,177.33333c-27.06667,0 -49,-21.93333 -49,-49c0,-27.06667 21.93333,-49 49,-49c27.06667,0 49,21.93333 49,49c0,27.06667 -21.93333,49 -49,49zM77,88.66667c-21.93333,0 -39.66667,17.73333 -39.66667,39.66667c0,21.93333 17.73333,39.66667 39.66667,39.66667c21.93333,0 39.66667,-17.73333 39.66667,-39.66667c0,-21.93333 -17.73333,-39.66667 -39.66667,-39.66667z" fill="#1565c0"></path><path d="M95.66667,177.33333c-27.06667,0 -49,-21.93333 -49,-49c0,-27.06667 21.93333,-49 49,-49c27.06667,0 49,21.93333 49,49c0,27.06667 -21.93333,49 -49,49zM95.66667,88.66667c-21.93333,0 -39.66667,17.73333 -39.66667,39.66667c0,21.93333 17.73333,39.66667 39.66667,39.66667c21.93333,0 39.66667,-17.73333 39.66667,-39.66667c0,-21.93333 -17.73333,-39.66667 -39.66667,-39.66667z" fill="#1565c0"></path><path d="M186.66667,167.53333c-13.53333,7.93333 -40.13333,19.13333 -84,19.13333c-64.4,0 -65.33333,-53.66667 -65.33333,-56h-4.66667h-4.66667c0,0.46667 0.93333,65.33333 74.66667,65.33333c41.53333,0 68.13333,-9.33333 84,-17.73333c3.26667,-1.4 6.53333,-3.73333 9.33333,-6.06667v-12.6c-2.8,2.8 -5.6,5.6 -9.33333,7.93333z" fill="#1565c0"></path><path d="M196,46.66667l-32.66667,-18.66667l-4.66667,7.93333l18.66667,10.73333z" fill="#1565c0"></path><path d="M154,46.66667h42v28h-42z" fill="#2196f3"></path><path d="M46.66667,132.53333c0,-1.4 0,-2.8 0,-4.2c0,-1.4 0,-2.8 0,-4.2c1.86667,-23.8 20.53333,-42.46667 44.33333,-44.8c-1.4,0 -3.26667,-0.46667 -4.66667,-0.46667c-27.06667,0.46667 -49,22.4 -49,49.46667c0,27.06667 21.93333,49 49,49c1.4,0 3.26667,0 4.66667,-0.46667c-23.33333,-1.86667 -42,-21 -44.33333,-44.33333z" fill="#2196f3"></path><path d="M95.66667,88.66667c-1.4,0 -3.26667,0 -4.66667,0.46667c19.6,2.33333 35,19.13333 35,39.2c0,20.06667 -15.4,36.86667 -35,39.2c1.4,0 3.26667,0.46667 4.66667,0.46667c20.53333,0 37.33333,-15.86667 39.66667,-35.46667c0,-1.4 0,-2.8 0,-4.2c0,-1.4 0,-2.8 0,-4.2c-2.33333,-19.6 -19.13333,-35.46667 -39.66667,-35.46667z" fill="#2196f3"></path></g></g></g></svg>
                                                                                            <h3 class="card-title">Cable</h3>
                                                                                        </div>
										</div>
									</div>
								</div>
								<div class="flip-card-back bg-danger no-after">
									<div class="flip-card-inner">
										<p class="mb-2 text-white">
Many cable service providers offer conventional setups with high performances. The Triple Play packages are the most affordable and at the same time service-driven. Triple means three services combined into one. Sometimes you get a couple of FREE HD channels along with FREE installation on subscription.
                                                                                </p>
										<!--<button type="button" class="btn btn-outline-light mt-2">View Details</button>-->
									</div>
								</div>
							</div>
						</div>
                                            
                                            
                                            
                                            
                                            	<div class="col-lg-4 mb-4" data-animate="bounceInRight">
							<div class="flip-card text-center">
								<div class="flip-card-front dark" style="background-image: url('images/later/flip_boxes/3.jpg')">
									<div class="flip-card-inner">
										<div class="card nobg noborder text-center">
											<div class="card-body">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                                                                        width="150" height="150"
                                                                                        viewBox="0 0 224 224"
                                                                                        style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g><g id="surface1"><path d="M162.85938,135.80729c-12.59636,-12.61458 -33.1224,-12.61458 -45.71875,0l-18.66667,18.66667c-7.94792,7.92968 -21,7.92968 -28.94792,0c-7.92969,-7.94792 -7.92969,-21 0,-28.94792l21,-21l-13.05208,-13.05208l-21,21c-15.40365,15.38542 -15.40365,40.12239 0,55.05208c15.38542,15.40364 40.12239,15.40364 55.05208,0l18.66667,-18.66667c5.61458,-5.1224 14,-5.1224 19.61458,0c5.12239,5.61458 5.12239,14 0,19.61458l-23.80729,23.78906l13.07031,13.07031l23.78907,-23.80729c12.61458,-12.59636 12.61458,-33.1224 0,-45.71875z" fill="#37474f"></path><path d="M75.14063,106.85938c-13.07032,-13.05208 -13.07032,-34.05208 0,-46.66667l31.71875,-31.71875c13.07031,-13.07032 34.07031,-13.07032 46.66667,0c13.07031,13.05208 13.07031,34.05208 0,46.66667l-31.71875,31.71875c-13.07032,13.07031 -33.61458,13.07031 -46.66667,0z" fill="#0277bd"></path><path d="M149.33333,51.33333c0,10.31771 -8.34896,18.66667 -18.66667,18.66667c-10.31771,0 -18.66667,-8.34896 -18.66667,-18.66667c0,-10.31771 8.34896,-18.66667 18.66667,-18.66667c10.31771,0 18.66667,8.34896 18.66667,18.66667z" fill="#b3e5fc"></path></g></g></g></svg>
                                                                                        <h3 class="card-title">Fiber-optic</h3>
												<!--<p class="card-text t400">With supporting text below as a natural lead-in to additional content.</p>-->
											</div>
										</div>
									</div>
								</div>
								<div class="flip-card-back bg-danger no-after">
									<div class="flip-card-inner">
										<p class="mb-2 text-white">
Fiber TV and Internet provides more options than what they used to offer. Increase your channel count, hands-down HD options, and on-demand shows/movies according to pleasure and viewing capacity. Only the high-end digital services have the capacity to establish flawless fiber optic TV and Internet systems.
                                                                                </p>
										<!--<button type="button" class="btn btn-outline-light mt-2">View Details</button>-->
									</div>
								</div>
							</div>
						</div>
						

						<div class="divider"><i class="icon-circle"></i></div>

						
					</div>
                                        
                                        
                                        
                                        
                                        
                                        
<!--					<div class="row mt-4 clearfix">
						<div class="col-lg-4 col-md-6 mb-5">
							<div class="feature-box not-dark fbox-small">
								<div class="fbox-icon">
									<img src="demos/hosting/images/svg/web.svg" class="noradius nobg tleft" alt="" style="height: 52px;">
								</div>
								<h3 class="t500 text-dark">Web Hosting</h3>
								<p class="t300">Is a type of Internet hosting service that allows individuals and organizations to make their website accessible via the World Wide Web.</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 mb-5">
							<div class="feature-box not-dark fbox-small">
								<div class="fbox-icon">
									<img src="demos/hosting/images/svg/cloud.svg" class="noradius nobg tleft" alt="" style="height: 52px;">
								</div>
								<h3 class="t500 text-dark">Cloud Hosting</h3>
								<p class="t300">The major benefit cloud hosting offers over shared hosting is that it allows you to utilize the resources of multiple servers.</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 mb-5">
							<div class="feature-box not-dark fbox-small">
								<div class="fbox-icon">
									<img src="demos/hosting/images/svg/dedicated.svg" class="noradius nobg tleft" alt="" style="height: 52px;">
								</div>
								<h3 class="t500 text-dark">Dedicated Hosting</h3>
								<p class="t300">Dedicated server or managed hosting service is a type of Internet hosting in which the client leases an entire server not shared.</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="feature-box not-dark fbox-small mb-5">
								<div class="fbox-icon">
									<img src="demos/hosting/images/svg/shared.svg" class="noradius nobg tleft" alt="" style="height: 52px;">
								</div>
								<h3 class="t500 text-dark">Shared Hosting</h3>
								<p class="t300">Shared Web Hosting service refers to a web hosting service where many websites reside on one web server connected to the Internet.</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="feature-box not-dark fbox-small mb-5">
								<div class="fbox-icon">
									<img src="demos/hosting/images/svg/domain.svg" class="noradius nobg tleft" alt="" style="height: 52px;">
								</div>
								<h3 class="t500 text-dark">Domain Registration</h3>
								<p class="t300">Shared Web Hosting service refers to a web hosting service where many websites reside on one web server connected to the Internet.</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="feature-box not-dark fbox-small mb-5">
								<div class="fbox-icon">
									<img src="demos/hosting/images/svg/activation.svg" class="noradius nobg tleft" alt="" style="height: 52px;">
								</div>
								<h3 class="t500 text-dark">Instant Activation</h3>
								<p class="t300">Shared Web Hosting service refers to a web hosting service where many websites reside on one web server connected to the Internet.</p>
							</div>
						</div>
					</div>-->




                                    <!--SECTION 5 DESC/PICTURE-->
                                    
                                    <br>
                                    <br>
					<div class="col_two_third" data-animate="">
						<h3>Bundling Reduces Service Rates</h3>
                                                <p align="justify">
While some providers offer packages, others don’t! You will see ‘Internet only’ or ‘TV only’ deals with them. Nevertheless, when a company doesn’t offer bundles, it is likely that each of their services comes with customer-friendly features. For instance, with a satellite Internet connection comes an additional fiber optic line to save you time and money. CableAgency.com helps you fish out TV-only, Internet-only, and TV + Internet services in your area.
                                                </p>
                                        </div>

					<div class="col_one_third col_last center align-items-center" data-animate="bounceIn">
                                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                            width="200" height="200"
                                            viewBox="0 0 224 224"
                                            style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g><g id="surface1"><path d="M168,112v-93.33333h-112v93.33333h-37.33333l93.33333,93.33333l93.33333,-93.33333z" fill="#689f38"></path><path d="M118.81771,110.79688c0,-2.22396 -0.63802,-4.01042 -1.85938,-5.34115c-1.25781,-1.33073 -3.37239,-2.55208 -6.34375,-3.66406c-2.97136,-1.13021 -5.54167,-2.24219 -7.71094,-3.35417c-2.15104,-1.11198 -4.01042,-2.36979 -5.55989,-3.80989c-1.54948,-1.44011 -2.75261,-3.11719 -3.60937,-5.06771c-0.85677,-1.93229 -1.29427,-4.24739 -1.29427,-6.92708c0,-4.61198 1.47656,-8.40364 4.42969,-11.35677c2.95312,-2.95313 6.89062,-4.6849 11.77604,-5.17708v-8.76823h6.5625v8.89583c4.83073,0.67448 8.62239,2.69792 11.35677,6.05208c2.73437,3.35417 4.10156,7.69271 4.10156,13.01563h-11.84896c0,-3.28125 -0.69271,-5.74219 -2.04167,-7.36458c-1.34896,-1.62239 -3.15365,-2.44271 -5.43229,-2.44271c-2.24219,0 -3.97396,0.63802 -5.21354,1.91407c-1.22136,1.27604 -1.84115,3.02604 -1.84115,5.26823c0,2.07812 0.60157,3.75521 1.80469,4.99479c1.20313,1.25782 3.44531,2.55208 6.70833,3.86458c3.26302,1.3125 5.96094,2.55208 8.05729,3.71875c2.11458,1.14844 3.88281,2.46094 5.34114,3.9375c1.44011,1.45833 2.55208,3.13542 3.31771,4.99479c0.76563,1.87761 1.14844,4.04687 1.14844,6.54427c0,4.64843 -1.45833,8.42187 -4.35677,11.32031c-2.89843,2.89844 -6.89062,4.61198 -11.95833,5.14062v8.14844h-6.52604v-8.11198c-5.61458,-0.60156 -9.95313,-2.60677 -13.03386,-5.97917c-3.08073,-3.37239 -4.61198,-7.85677 -4.61198,-13.47136h11.84896c0,3.24479 0.78386,5.74219 2.33333,7.49219c1.53125,1.73177 3.75521,2.60677 6.65365,2.60677c2.40625,0 4.32031,-0.63802 5.70573,-1.91407c1.40364,-1.27604 2.09636,-2.98958 2.09636,-5.15885z" fill="#ffffff"></path></g></g></g></svg>
                                            <!--<img class="alignright img-responsive" src="images/landing/responsive.png" alt="">-->	
                                        </div>
					<div class="divider"><i class="icon-circle"></i></div>
                                        
                                        <div class="clear"></div>
                                        
                                        
                                        
                                    <!--SECTION 6 PICTURE/DESC-->                                        
					<div class="col_one_third center align-items-center" data-animate="bounceIn">
                                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                            width="150" height="150"
                                            viewBox="0 0 224 224"
                                            style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g id="Layer_1" fill="#9c27b0"><path d="M144.66667,32.66667l60.66667,7.868l-49.952,48.132z"></path><path d="M79.33333,191.324l-60.66667,-7.86333l49.952,-48.12733z"></path><path d="M32.66667,79.33333l7.868,-60.66667l48.132,49.952z"></path><path d="M191.324,144.66667l-7.868,60.66667l-48.12267,-49.952z"></path><path d="M112,186.66667c-4.774,0 -9.43133,-0.49467 -13.95333,-1.34867l-3.23867,18.074c5.88933,0.98 11.78333,1.46067 17.192,1.46067c26.52067,0 50.10133,-11.284 67.29333,-28.97533l-13.09467,-12.61867c-13.608,14.38267 -32.83,23.408 -54.19867,23.408z"></path><path d="M37.33333,112c0,-4.62467 0.48067,-9.128 1.28333,-13.524l-19.45067,3.71c-0.49933,3.43467 -0.49933,6.39333 -0.49933,9.828c0,22.60533 7.868,44.212 22.60533,60.90467l14.16333,-12.264c-11.256,-13.076 -18.102,-30.04867 -18.102,-48.65467z"></path><path d="M112,37.33333c4.774,0 9.43133,0.49467 13.958,1.34867l3.234,-18.046c-5.40867,-1.456 -11.30267,-1.95533 -17.192,-1.95533c-26.52067,0 -50.10133,11.30267 -67.29333,28.97533l13.09467,13.09467c13.608,-14.392 32.83,-23.41733 54.19867,-23.41733z"></path><path d="M184.20733,52.56533l-14.67667,11.844c10.70067,12.922 17.136,29.50267 17.136,47.59067c0,4.634 -0.48067,9.14667 -1.288,13.54733l19.45533,-3.71c0.49933,-3.43467 0.49933,-6.38867 0.49933,-9.82333c0,-21.60667 -7.36867,-42.73267 -21.126,-59.44867z"></path></g></g></svg>
                                            <!--<img class="alignright img-responsive" src="images/landing/responsive.png" alt="">-->	
                                        </div>
                                        
                                        <div class="col_two_third  col_last" data-animate="">
						<h3>How It Works?</h3>
Whether you are new to an area or you seek better options, CableAgency.com serves as a guide to broadcasting and Internet selection. The companies here are the cream of the crop with customers across the nation. Features like high-speed Internet, crisp HD TV, reliable landline, and unbreakable home security systems are only three steps away from you.
                                        </div>  
                                    
                                                                        
                                 
					<!--<div class="divider"><i class="icon-circle"></i></div>-->

				
 </div>
                            
					<!--<div class="line d-block d-md-none"></div>-->
                                              
<!--					<ul class="process-steps process-3 bottommargin clearfix">
						<li>
							<a href="#" class="i-circled i-alt divcenter">1</a>
							<h5>Enter the zip code</h5>
						</li>
						<li>
							<a href="#" class="i-circled i-alt divcenter">2</a>
							<h5>Select the company you want to subscribe to</h5>
						</li>
						<li class="active">
							<a href="#" class="i-circled i-alt divcenter bgcolor">3</a>
							<h5>Call to speak to one of our agents</h5>
						</li>   
					</ul>                               -->
                                        
                                        

<div class="container clearfix"> 
    <div class="col_one_third" data-animate="flipInY">
        <div class="feature-box fbox-center fbox-bg fbox-light fbox-effect">
            <div class="fbox-icon">
                <a href="#"><i class="i-alt">1</i></a>
            </div>
            <h3>Enter the zip code in text box<span class="subtitle">Step 1</span></h3>
        </div>
    </div>

    <div class="col_one_third" data-animate="flipInY" data-delay="500">
        <div class="feature-box fbox-center fbox-bg fbox-border fbox-effect">
            <div class="fbox-icon">
                <a href="#"><i>2</i></a>
            </div>
            <h3>Select the company you want to subscribe<span class="subtitle">Step 2</span></h3>
        </div>
    </div>

    <div class="col_one_third col_last" data-animate="flipInY" data-delay="1000">
        <div class="feature-box fbox-center fbox-bg fbox-outline fbox-dark fbox-effect">
            <div class="fbox-icon">
                <a href="#"><i class="i-alt">3</i></a>
            </div>
            <h3>Call to speak to one of our agents<span class="subtitle">Step 3</span></h3>
        </div>
    </div>
</div>                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
				<!-- Domain Section
				============================================= -->
<!--				<div class="section section-domain mt-4 mb-3" style="background: #F5F5F5 url('demos/hosting/images/pattern-dark.png') repeat center center;">
					<div class="container">
						<div class="row justify-content-between align-items-center">
							<div class="col-md-5">
								<div class="heading-block mb-0 nobottomborder clearfix">
									<h3 class="ls0 nott">Looking For Your Premium Domain?</h3>
									<p class="mb-0">Competently recaptiualize multifunctional schemas without an expanded array of niches. Continually engage cooperative.</p>
								</div>
							</div>
							<div class="col-md-6 mt-4 mt-lg-0">
								<div class="input-group input-group-lg">
									<input type="text" class="form-control" placeholder="Your New Domain Name" aria-label="Enter Domain Name">
									<div class="input-group-append">
										<select class="custom-select" id="inputGroupSelect01">
											<option value="com" selected>.com</option>
											<option value="org">.org</option>
											<option value="net">.net</option>
											<option value="info">.info</option>
											<option value="us">.us</option>
										</select>
									</div>
									<div class="input-group-append">
										<button class="btn bgcolor text-white dark">Search</button>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>-->

                              
				<!-- Loaction Section
				============================================= -->
<!--				<div class="section nomargin nobg">
					<div class="container clearfix">
						<div class="heading-block center mt-0 divcenter nobottomborder clearfix" style="max-width: 700px">
							<h2>We provide best services for you.</h2>
							<p>Competently recaptiualize multifunctional schemas without an expanded array of niches. Continually engage cooperative sources vis-a-vis web-enabled benefits.</p>
						</div>
                                            
						<div class="row">
							<div class="col-lg-5 col-md-6">
								<div class="heading-block mt-md-3 mt-0">
									<div class="before-heading color">Our Network</div>
									<h3 class=" nott ls0">16 TBPS Capacity and 140 Data Center Global Network</h3>
								</div>
								<div class="row">
									<div class="col-6">
										<ul class="iconlist ml-0">
											<li><img src="demos/hosting/images/flags/uk.png" alt=""><a href="#">United Kingdom</a></li>
											<li><img src="demos/hosting/images/flags/us.png" alt=""><a href="#">USA</a></li>
											<li><img src="demos/hosting/images/flags/br.png" alt=""><a href="#">Brazil</a></li>
											<li><img src="demos/hosting/images/flags/sa.png" alt=""><a href="#">South Africa</a></li>
											<li><img src="demos/hosting/images/flags/in.png" alt=""><a href="#">India</a></li>
										</ul>
									</div>
									<div class="col-6">
										<ul class="iconlist ml-0">
											<li><img src="demos/hosting/images/flags/si.png" alt=""><a href="#">Singapore</a></li>
											<li><img src="demos/hosting/images/flags/ja.png" alt=""><a href="#">Japan</a></li>
											<li><img src="demos/hosting/images/flags/au.png" alt=""><a href="#">Australia</a></li>
											<li><img src="demos/hosting/images/flags/ca.png" alt=""><a href="#">Canada</a></li>
											<li><a href="#">See More..</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-lg-7 col-md-6">
								<div id="hotspot-img" class="hotspot-img mt-2 responsive-hotspot-wrap">
							<img src="demos/hosting/images/svg/map.png" class="img-responsive" alt="">

							 Circle #1 
							<div class="hot-spot" x="1970" y="830">
								<div class="circle"></div>
								<div class="tooltip"><p>singapore</p></div>
							</div>
							 Circle #2 
							<div class="hot-spot" x="330" y="522">
								<div class="circle"></div>
								<div class="tooltip"><p>San Fransisco</p></div>
							</div>
							 Circle #3 
							<div class="hot-spot" x="2350" y="1110">
								<div class="circle"></div>
								<div class="tooltip"><p>Sydney</p></div>
							</div>
							 Circle #4 
							<div class="hot-spot" x="1222" y="395">
								<div class="circle"></div>
								<div class="tooltip"><p>London</p></div>
							</div>
							 Circle #5 
							<div class="hot-spot" x="1800" y="760">
								<div class="circle"></div>
								<div class="tooltip"><p>Bangalore</p></div>
							</div>
							 Circle #6 
							<div class="hot-spot" x="2250" y="560">
								<div class="circle"></div>
								<div class="tooltip"><p>Tokyo</p></div>
							</div>
							 Circle #7 
							<div class="hot-spot" x="1366" y="1134">
								<div class="circle"></div>
								<div class="tooltip"><p>Cape Town</p></div>
							</div>
							 Circle #8 
							<div class="hot-spot" x="880" y="990">
								<div class="circle"></div>
								<div class="tooltip"><p>Brasilla</p></div>
							</div>
							 Circle #8 
							<div class="hot-spot" x="710" y="395">
								<div class="circle"></div>
								<div class="tooltip"><p>Toronto</p></div>
							</div>
						</div>
							</div>
						</div>
					</div>
				</div>-->

				<!-- Pricing Section
				============================================= -->
<!--				<div class="section bg-angle notopmargin" style="background: #F9F9F9 url('demos/hosting/images/pattern-dark.png') repeat center center; padding: 80px 0">
					<div class="container clearfix">
						<div class="heading-block center mt-0 divcenter nobottomborder clearfix" style="max-width: 750px">
							<h2>Hosting that scales with your Business</h2>
							<p>Competently recaptiualize multifunctional schemas without an expanded array of niches. Continually engage cooperative sources vis-a-vis web-enabled benefits.</p>
						</div>

						<div class="row mb-2 align-items-stretch">

							<div class="col-lg-8 col-md-8">

								<div class="row align-items-stretch clearfix">
									<div class="col-md-12">
										 vCPU Section 
										<div class="row align-items-stretch">
											<div class="col-md-8 col-sm-8">
												<div class="card mb-4">
													<div class="card-body">
														<div class="d-flex justify-content-between">
															<h4 class="mb-4">vCPU</h4>
														</div>
														 CPU Slider
														============================================= 
														<input class="range-slider-cpu" />
													</div>
												</div>
											</div>
											<div class="col-md-4 col-sm-4 d-none d-sm-flex card bgcolor text-white text-center mb-4">
												<div class="card-body">
													 CPU Price Value
													============================================= 
													<h2 class="cpu-price text-white mb-0 t700"></h2>
													<p class="card-text mb-0" style="opacity: .8;">$10 <span class="font-italic">per vCPU</span></p>
												</div>
											</div>
										</div>

										 RAM Section 
										<div class="row align-items-stretch">
											<div class="col-md-8 col-sm-8">
												<div class="card mb-4">
													<div class="card-body">
														<div class="d-flex justify-content-between">
															<h4 class="mb-4">Ram</h4>
														</div>
														 RAM Slider
														============================================= 
														<input class="range-slider-ram" />
													</div>
												</div>
											</div>
											<div class="col-md-4 col-sm-4 d-none d-sm-flex card bgcolor text-white text-center mb-4">
												<div class="card-body">
													 RAM Proce Value
													============================================= 
													<h2 class="ram-price text-white mb-0 t700"></h2>
													<p class="card-text mb-0" style="opacity: .8;">$8 <span class="font-italic">per RAM</span></p>
												</div>
											</div>
										</div>

										 Storage Section 
										<div class="row align-items-stretch">
											<div class="col-md-8 col-sm-8">
												<div class="card">
													<div class="card-body">
														<div class="d-flex justify-content-between">
															<h4 class="mb-4">Storage</h4>
														</div>
														 Storage Slider
														============================================= 
														<input class="range-slider-storage" />
													</div>
												</div>
											</div>
											<div class="col-md-4 col-sm-4 d-none d-sm-flex card bgcolor text-white text-center">
												<div class="card-body">
													 Storage Price Value
													============================================= 
													<h2 class="storage-price text-white mb-0 t700"></h2>
													<p class="card-text mb-0" style="opacity: .8;">$.5 <span class="font-italic">per Storage</span></p>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>

							 Price Area 
							<div class="col-lg-4 col-md-4 mt-4 mt-md-0">
								<div class="card text-center">
									<div class="card-body py-4">
										<h3 class="card-title t400">Your Plan</h3>
										 Price Value 
										<div class="pricing-price t600 py-2"></div>
										<p class="card-text pricing-sub">Billed Monthly</p>
									</div>
									<div class="line my-1"></div>
									<div class="card-body py-4">
										<ul class="iconlist ml-0" style="opacity: .8">
											<li>Un-Mitered Bandwith</li>
											<li>2 Dedicated IPs</li>
											<li>Shared SSL certificate</li>
											<li>30 Days Money Back</li>
										</ul>
										<a href="#" class="button button-rounded button-large ls0 t400 m-0 nott">Order Now</a>
									</div>
								</div>
							</div>

						</div>

						<div class="clear"></div>

						<div class="row mt-5 clearfix">
							<div class="col-lg-3 col-sm-6">
								<h4 class="t500 mb-3">Free Includes:</h4>
								<ul class="iconlist price-compare mb-2">
									<li><a href="#" class="t300">Unmetered Mitigation of DDoS</a></li>
									<li><a href="#" class="t300">Global CDN</a></li>
									<li><a href="#" class="t300">Shared SSL certificate</a></li>
									<li><a href="#" class="t300">I'm Under Attack™ mode</a></li>
									<li><a href="#" class="t300">Access to account Audit Logs</a></li>
								</ul>
								<a href="#" class="btn btn-link mt-2 t400 color p-0" style="font-size: 14px">All Features &#8250;</a>
							</div>
							<div class="col-lg-3 col-sm-6 mt-4 mt-sm-0">
								<h4 class="t500 mb-3">Pro Includes:</h4>
								<ul class="iconlist price-compare mb-2">
									<li><a href="#" class="t300">100 GB of disk space</a></li>
									<li><a href="#" class="t300">1000 GB/mo data transfer</a></li>
									<li><a href="#" class="t300">1 domain + 100 subdomains</a></li>
									<li><a href="#" class="t300">10 site pages</a></li>
									<li><a href="#" class="t300">5 email addresses</a></li>
									<li><a href="#" class="t300">Site design tools</a></li>
									<li><a href="#" class="t300">Account access for colleagues/friends</a></li>
								</ul>
								<a href="#" class="btn btn-link mt-2 t400 color p-0" style="font-size: 14px">All Features &#8250;</a>
							</div>
							<div class="col-lg-3 col-sm-6 mt-4 mt-lg-0">
								<h4 class="t500 mb-3">Business Includes:</h4>
								<ul class="iconlist price-compare mb-2">
									<li><a href="#" class="t300">Unlimited site pages</a></li>
									<li><a href="#" class="t300">10 email addresses</a></li>
									<li><a href="#" class="t300">500 GB of disk space</a></li>
									<li><a href="#" class="t300">5000 GB/mo data transfer</a></li>
									<li><a href="#" class="t300">1 domain + 500 subdomains</a></li>
									<li><a href="#" class="t300">Secure FTP File Manager</a></li>
									<li><a href="#" class="t300">WordPress blogging</a></li>
									<li><a href="#" class="t300">PHP 5.3.6, Perl 5.8.7, MySQL 5.1</a></li>
								</ul>
								<a href="#" class="btn btn-link mt-2 t400 color p-0" style="font-size: 14px">All Features &#8250;</a>
							</div>
							<div class="col-lg-3 col-sm-6 mt-4 mt-lg-0">
								<h4 class="t500 mb-3">Advanced Includes:</h4>
								<ul class="iconlist price-compare mb-2">
									<li><a href="#" class="t300">20 email addresses</a></li>
									<li><a href="#" class="t300">Unlimited disk space</a></li>
									<li><a href="#" class="t300">Unlimited data transfer</a></li>
									<li><a href="#" class="t300">Private registration</a></li>
									<li><a href="#" class="t300">Named solution and customer success engineers
									Access to China data centers (additional cost)</a></li>
									<li><a href="#" class="t300">30-day satisfaction guarantee</a></li>
									<li><a href="#" class="t300">24/7 phone, chat and online support</a></li>

								</ul>
								<a href="#" class="btn btn-link mt-2 t400 color p-0" style="font-size: 14px">All Features &#8250;</a>
							</div>
						</div>
					</div>
				</div>-->

				<!-- Addition Service Section
				============================================= -->
<!--				<div class="container clearfix">
					<div class="row clearfix">
						<div class="col-md-12">
							<div class="before-heading">Other Additional Services</div>
							<h3>Highly Available Services</h3>
							<div class="row">
								<div class="col-md-6">
									<div class="feature-box p-4 media-box" style="border-radius: 6px; box-shadow: 0 2px 4px rgba(3,27,78,.1); border: 1px solid #e5e8ed;">
										<div class="fbox-media">
											<img src="demos/hosting/images/svg/balancing.svg" style="width: 42px;" alt="">
										</div>
										<div class="fbox-desc">
											<h3 class="ls0">Load Balancing</h3>
											<p class="mt-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita sint explicabo quis at voluptatum autem, cupiditate officiis maxime deserunt soluta! consectetur adipisicing elit.</p>
										</div>
										<a href="#" class="btn btn-link mt-3 t400 color p-0" style="font-size: 16px;">$15/month for Load Balancing <i class="icon-line-arrow-right position-relative" style="top: 2px"></i></a>
									</div>

									<div class="feature-box p-4 topmargin-sm media-box" style="border-radius: 6px; box-shadow: 0 2px 4px rgba(3,27,78,.1); border: 1px solid #e5e8ed;">
										<div class="fbox-media">
											<img src="demos/hosting/images/svg/location.svg" style="width: 42px;" alt="">
										</div>
										<div class="fbox-desc">
											<h3 class="ls0">Location Zone</h3>
											<p class="mt-2">Distinctively enhance front-end outsourcing after cross-platform synergy. Interactively implement an expanded array of collaboration and idea-sharing and innovative bandwidth.</p>
										</div>
										<a href="#" class="btn btn-link mt-3 t400 color p-0" style="font-size: 16px;">$5/month for Location <i class="icon-line-arrow-right position-relative" style="top: 2px"></i></a>
									</div>
								</div>

								<div class="col-md-6">
									<div class="feature-box p-4 mt-4 mt-sm-0 media-box" style="border-radius: 6px; box-shadow: 0 2px 4px rgba(3,27,78,.1); border: 1px solid #e5e8ed;">
										<div class="fbox-media">
											<img src="demos/hosting/images/svg/ssl.svg" style="width: 42px;" alt="">
										</div>
										<div class="fbox-desc">
											<h3 class="ls0">Dedicated SSL Certificate</h3>
											<p class="mt-2">Assertively harness stand-alone communities through front-end networks. Globally engage plug-and-play sources through multidisciplinary portals. Enthusiastically synergize orthogonal.</p>
										</div>
										<a href="#" class="btn btn-link mt-3 t400 color p-0" style="font-size: 16px;">$7/month for SSL Certificate <i class="icon-line-arrow-right position-relative" style="top: 2px"></i></a>
									</div>

									<div class="feature-box p-4 topmargin-sm media-box" style="border-radius: 6px; box-shadow: 0 2px 4px rgba(3,27,78,.1); border: 1px solid #e5e8ed;">
										<div class="fbox-media">
											<img src="demos/hosting/images/svg/team.svg" style="width: 42px;" alt="">
										</div>
										<div class="fbox-desc">
											<h3 class="ls0">Team Accounts</h3>
											<p class="mt-2">Uniquely harness prospective information through long-term high-impact portals. Rapidiously enable principle-centered users rather than inexpensive sources. Distinctively enhance front-end outsourcing.</p>
										</div>
										<a href="#" class="btn btn-link mt-3 t400 color p-0" style="font-size: 16px;">$4/month for Team <i class="icon-line-arrow-right position-relative" style="top: 2px"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>-->

				<!-- Testimonials Section
				============================================= -->
<!--				<div class="section">
					<div class="container">

						<div class="heading-block center mt-2 mb-3 divcenter nobottomborder clearfix" style="max-width: 700px">
							<div class="before-heading">See what Our Customers say</div>
							<h2>3,583 Happy Users &amp; Counting</h2>
						</div>

						 Owl Carousel
						============================================= 
						<div id="oc-testimonials" class="owl-carousel center testimonials-carousel testimonial-full carousel-widget mt-5" data-margin="0" data-items-xs="1" data-items-sm="1" data-items-md="1" data-items-lg="2" data-items-xl="3" data-center="true" data-loop="true" data-autoplay="4000" data-speed="500">

							 Item 1 
							<div class="oc-item">
								<div class="testimonial p-4 noborder noshadow">
									<div class="testi-image">
										<a href="#"><img src="demos/hosting/images/users/1.jpg" alt="Customer Testimonails"></a>
									</div>
									<ul class="list-unstyled ml-0 mt-4 mb-2"><li class="color"><i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star-half-full"></i></li></ul>
									<div class="testi-content">
										<p class="font-body font-normal">Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum repellendus!</p>
										<div class="testi-meta">
											John Doe
											<span>XYZ Inc.</span>
										</div>
									</div>
								</div>
							</div>

							 Item 2 
							<div class="oc-item">
								<div class="testimonial p-4 noborder noshadow">
									<div class="testi-image">
										<a href="#"><img src="demos/hosting/images/users/2.jpg" alt="Customer Testimonails"></a>
									</div>
									<ul class="list-unstyled ml-0 mt-4 mb-2"><li class="color"><i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star-empty"></i></li></ul>
									<div class="testi-content">
										<p class="font-body font-normal">Natus voluptatum enim quod necessitatibus quis expedita harum provident eos obcaecati id culpa corporis molestias.</p>
										<div class="testi-meta">
											Collis Ta'eed
											<span>Envato Inc.</span>
										</div>
									</div>
								</div>
							</div>

							 Item 3 
							<div class="oc-item">
								<div class="testimonial p-4 noborder noshadow">
									<div class="testi-image">
										<a href="#"><img src="demos/hosting/images/users/5.jpg" alt="Customer Testimonails"></a>
									</div>
									<ul class="list-unstyled ml-0 mt-4 mb-2"><li class="color"><i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star3"></i></li></ul>
									<div class="testi-content">
										<p class="font-normal font-body">Fugit officia dolor sed harum excepturi ex iusto magnam asperiores molestiae qui natus obcaecati facere sint amet.</p>
										<div class="testi-meta">
											Mary Jane
											<span>Google Inc.</span>
										</div>
									</div>
								</div>
							</div>

							 Item 4 
							<div class="oc-item">
								<div class="testimonial p-4 noborder noshadow">
									<div class="testi-image">
										<a href="#"><img src="demos/hosting/images/users/4.jpg" alt="Customer Testimonails"></a>
									</div>
									<ul class="list-unstyled ml-0 mt-4 mb-2"><li class="color"><i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star3"></i></li></ul>
									<div class="testi-content">
										<p class="font-normal font-body">Similique fugit repellendus expedita excepturi iure perferendis provident quia eaque. Repellendus, vero numquam?</p>
										<div class="testi-meta">
											Steve Jobs
											<span>Apple Inc.</span>
										</div>
									</div>
								</div>
							</div>

							 Item 5 
							<div class="oc-item">
								<div class="testimonial p-4 noborder noshadow">
									<div class="testi-image">
										<a href="#"><img src="demos/hosting/images/users/3.jpg" alt="Customer Testimonails"></a>
									</div>
									<ul class="list-unstyled ml-0 mt-4 mb-2"><li class="color"><i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star-empty"></i></li></ul>
									<div class="testi-content">
										<p class="font-normal font-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, perspiciatis illum totam dolore deleniti labore.</p>
										<div class="testi-meta">
											Jamie Morrison
											<span>Adobe Inc.</span>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>-->

				<!-- Trusted Client Section
				============================================= -->
<!--				<div class="section mt-3 mb-4 pt-0 nobg">
					<div class="container clearfix">
						<div class="divcenter" style="max-width: 1000px">
							<div class="heading-block center divcenter nobottomborder clearfix" style="max-width: 700px">
								<div class="before-heading">Already with Us</div>
								<h2>Trusted By</h2>
								<p>Competently recaptiualize multifunctional schemas without an expanded array of niches. Continually engage cooperative sources vis-a-vis web-enabled benefits.</p>
							</div>
							 Clients Images 
							<div class="row clearfix">
								<div class="col-sm-3 col-6 center"><a href="#"><img src="demos/hosting/images/clients/1.png" alt="" class="clients"></a></div>
								<div class="col-sm-3 col-6 center"><a href="#"><img src="demos/hosting/images/clients/2.png" alt="" class="clients"></a></div>
								<div class="col-sm-3 col-6 center"><a href="#"><img src="demos/hosting/images/clients/3.png" alt="" class="clients mt-5 mt-sm-0"></a></div>
								<div class="col-sm-3 col-6 center"><a href="#"><img src="demos/hosting/images/clients/4.png" alt="" class="clients mt-5 mt-sm-0"></a></div>
								<div class="col-sm-3 col-6 center"><a href="#"><img src="demos/hosting/images/clients/5.png" alt="" class="clients mt-5"></a></div>
								<div class="col-sm-3 col-6 center"><a href="#"><img src="demos/hosting/images/clients/6.png" alt="" class="clients mt-5"></a></div>
								<div class="col-sm-3 col-6 center"><a href="#"><img src="demos/hosting/images/clients/7.png" alt="" class="clients mt-5"></a></div>
								<div class="col-sm-3 col-6 center"><a href="#"><img src="demos/hosting/images/clients/8.png" alt="" class="clients mt-5"></a></div>
								<div class="col-sm-3 col-6 center"><a href="#"><img src="demos/hosting/images/clients/9.png" alt="" class="clients mt-5"></a></div>
								<div class="col-sm-3 col-6 center"><a href="#"><img src="demos/hosting/images/clients/10.png" alt="" class="clients mt-5"></a></div>
								<div class="col-sm-3 col-6 center"><a href="#"><img src="demos/hosting/images/clients/11.png" alt="" class="clients mt-5"></a></div>
								<div class="col-sm-3 col-6 center"><a href="#"><img src="demos/hosting/images/clients/12.png" alt="" class="clients mt-5"></a></div>
							</div>
						</div>
					</div>
				</div>-->

        <!-- Bottom Section
        ============================================= -->
        <div class="section bgcolor dark m-0">
            <div class="container">
                <div class="row center justify-content-center">
                    <div class="col-md-10" data-animate="fadeIn">
                            <h2 class="t700 text-white mb-3">With CableAgency.com, it is as simple as a click of a button!</h2>
                    </div>
                    <div class="col-md-8" data-animate="flipInX">
                        <a href="best-providers.php" class="button button-rounded button-reveal button-large button-white button-light tright" style="color: #44AAAC;"><i class="icon-line-arrow-right" style="color: #44AAAC;"></i><span>Best Providers</span></a>
                            <!--<a href="#" class="button center bg-white color button-light button-rounded button-large ml-0">Best Providers</a>-->
                    </div>
                </div>
            </div>
        </div>

			</div>

		</section><!-- #content end -->

<?php require_once 'views/footer.php'; ?>

<!--    <script type = "text/javascript">
         
            function Warn() {
               alert ("You have enter invalid zipcode!");
               document.write ("You have enter invalid zipcode!");
            }
         
    </script> -->