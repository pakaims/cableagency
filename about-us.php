<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,400i,700|Istok+Web:400,700" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="css/components/ion.rangeslider.css" type="text/css" />

	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<meta name='viewport' content='initial-scale=1, viewport-fit=cover'>

	<!-- Hosting Demo Specific Stylesheet -->
	<link rel="stylesheet" href="css/colors.php?color=44aaac" type="text/css" /> <!-- Theme Color -->
	<link rel="stylesheet" href="demos/hosting/css/fonts.css" type="text/css" />
	<link rel="stylesheet" href="demos/hosting/hosting.css" type="text/css" />
        
        
	<!-- / -->

	<!-- Document Title
	============================================= -->
	<title>Cable | About Us</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">
    <header>
        <div class="menu-toggle" id="hamburger">
            <i class="fas fa-bars"></i>
        </div>
        <div class="overlay"></div>
        <div class="container">
            <nav>
			<div id="logo">
						<a href="index.php" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="images/logo.png" alt="Canvas Logo"></a>
						<a href="index.php" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="images/logo@2x.png" alt="Canvas Logo"></a>
	</div>
                <ul>
                    <li class="current"><a href="index.php">Home</a></li>
                    <li><a href="best-providers.php">Providers</a></li>
                    <li><a href="about-us.php">About Us</a></li>
                    <div style="padding-left:25px;" ><a href="tel:18889309001"> (888) 930-9001</a></div>
                </ul>
            </nav>
        </div>
</header>
	<script>var open = document.getElementById('hamburger');
var changeIcon = true;

open.addEventListener("click", function(){

    var overlay = document.querySelector('.overlay');
    var nav = document.querySelector('nav');
    var icon = document.querySelector('.menu-toggle i');

    overlay.classList.toggle("menu-open");
    nav.classList.toggle("menu-open");

    if (changeIcon) {
        icon.classList.remove("fa-bars");
        icon.classList.add("fa-times");

        changeIcon = false;
    }
    else {
        icon.classList.remove("fa-times");
        icon.classList.add("fa-bars");
        changeIcon = true;
    }
});</script>
		
                

		<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-parallax page-title-dark" style="padding: 250px 0; background-image: url('images/later/about-us.jpg'); height:535px; background-size: cover; background-position: center center;" data-bottom-top="background-position:0px 400px;" data-top-bottom="background-position:0px -500px;">

			<div class="container clearfix">
                            <h1><strong>About Us</strong></h1>
                            <h3 class="text-white">Everything About Us is About You</h3>
<!--				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Pages</a></li>
					<li class="breadcrumb-item active" aria-current="page">About Us</li>
				</ol>-->
			</div>
                    
		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
<section id="content">
    <div class="container clearfix">
        <div class="nobottommargin">                            
                                    
            <!--SECTION 1 DESC/PICTURE-->
                <div class="col_two_third topmargin">
                        <h3>CableAgency.com Looks For the Most Relevant TV, Internet, and Phone Service Providers</h3>
                        <p align ="justify">
It is our job to guide you towards the best service providers, whether it is for cable TV, satellite, Internet or phone in your area. We work with prolific brands and highlight the packages they offer on the site. When you are deciding which service to choose, a comparison between services helps in finalizing them. When there is a list of brands and the updated prices for each of their services are right in front of you, it is only a matter of clicking the subscribe button. If you get confused in the way, we are there to help you get back on track. We leave our number throughout the website, so nothing gets in between us. In addition, we do the hard work for you to experience the best services in your area. The customers who purchase from us are both happy and content. Never have we received a complaint in terms of services or from their originating brands. Consider us as the mediator who settles for nothing less than a 100% customer satisfaction.
                        </p>
                </div>
                <div class="col_one_third col_last center align-items-center topmargin">
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                    width="300" height="300"
                    viewBox="0 0 224 224"
                    style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g><g id="surface1"><path d="M177.66146,100.93489c0,0 15.34896,0.76563 15.34896,-8.25781c0,-11.79427 -20.43489,-22.42187 -20.43489,-22.42187c0,0 3.20833,-6.79948 5.19531,-10.59115c2.00521,-3.79167 7.61979,-18.61198 8.13021,-22.0026c0.63802,-4.2474 -0.32813,-5.57813 -0.32813,-5.57813c-1.38542,9.11458 -16.22396,35.34636 -17.40886,36.23958c-14.47396,-6.78125 -34.34375,-8.67708 -34.34375,-8.67708c0,0 -19.46875,-40.97917 -37.77083,-40.97917c-18.15625,0 -18.0651,35.09114 -18.0651,35.09114c0,0 -5.1224,-9.95312 -11.55729,-9.95312c-9.40625,0 -12.50521,14.23698 -12.50521,29.67708c-18.57552,0 -34.21615,4.15625 -35.60157,4.55729c-1.40364,0.40104 -5.79687,3.59115 -3.80989,3.19011c4.06511,-1.29427 23.16927,-4.24739 39.88542,-2.80729c0.91146,14.67448 9.47917,33.77864 9.47917,33.77864c0,0 -18.35677,26.61458 -18.35677,45.60937c0,5.01302 1.85938,14.85677 15.05729,14.85677c11.04687,0 23.75261,-7.34636 26.08594,-8.69531c-2.04167,2.91667 -3.55469,8.44011 -3.55469,10.99219c0,1.18489 0.47396,3.6276 2.42448,5.50521c3.11719,-3.09896 6.17969,-6.17969 9.97136,-9.95312c-4.08333,-0.7474 -4.63021,-3.77344 -4.63021,-4.55729c0,-2.73437 2.13282,-5.97917 2.13282,-5.97917c0,0 9.93489,-6.70833 10.55468,-7.41927l7.32813,13.67188c0,0 -7.51042,4.44792 -13.39844,4.44792c-0.74739,0 -1.40364,-0.05469 -1.98698,-0.16406c-3.79167,3.77343 -6.85417,6.85417 -9.97136,9.95312c1.45833,1.40364 3.71875,2.47917 7.32813,2.47917c10.9375,0 23.16927,-8.38542 23.16927,-8.38542c0,0 11.53907,19.17708 21.38282,27.96354c2.66146,2.36979 5.21354,2.80729 5.21354,2.80729c0,0 -9.80729,-9.44271 -22.71354,-33.74219c11.99479,-7.41927 24.48177,-24.90104 24.48177,-24.90104c0,0 1.47657,0.03646 12.86979,0.03646c17.84636,0 43.20312,-3.75521 43.20312,-17.9375c0,-14.63802 -22.80468,-27.85417 -22.80468,-27.85417zM179.66667,92.11198c0,5.17708 -4.94011,5.1224 -4.94011,5.1224l-3.73698,0.23698l-11.41146,-5.48698c0,0 6.67187,-10.28125 8.23958,-13.16146c1.18489,0.69271 11.84896,7.36458 11.84896,13.28906zM71.73177,55.36198c2.66146,0 5.28646,3.24479 6.36198,5.9974c0,1.80468 0.94792,12.41406 0.94792,12.41406l-15.40364,-0.56511c0,-13.90886 5.43229,-17.84636 8.09375,-17.84636zM70.14583,151.84896c-8.42187,0 -10.15364,-4.6849 -10.15364,-8.91407c0,-9.57031 7.61979,-22.93229 7.61979,-22.93229c0,0 8.54948,17.99219 23.47917,25.57552c-7.40104,4.35677 -13.52604,6.27083 -20.94531,6.27083zM115.88281,163.22396c-3.59114,-6.28907 -6.23437,-12.85157 -6.23437,-12.85157c0,0 14.74739,0.96615 22.67708,-7.23698c-4.94011,2.22396 -12.81511,5.03125 -21.98438,4.17448l39.79427,-41.74479c-0.82031,-0.98438 -5.14062,-4.01042 -6.19792,-4.52083c-5.70573,6.87239 -27.89062,30.60677 -48.43489,42.34636c-26.01302,-14.18229 -31.48177,-55.94532 -32.02864,-64.60417l14.20052,1.36718c0,0 -5.34114,9.46094 -5.34114,16.42448c0,6.96354 0.83854,7.34635 0.83854,7.34635c0,0 -0.18229,-12.15885 7.30989,-21.52864c5.72396,30.40625 11.66667,45.99219 16.31511,55.28906c2.36979,-0.98437 6.78125,-2.93489 6.78125,-2.93489c0,0 -13.125,-37.84375 -12.39583,-63.45573c4.13802,-2.20573 9.67968,-4.44792 15.98698,-5.61458c-0.14583,-1.64063 -0.49219,-3.28125 -1.20313,-4.90364c-4.77604,1.11198 -9.89843,2.88021 -14.69271,5.70573c0.41927,-14.27344 5.21354,-27.17969 13.70833,-27.17969c8.38542,0 20.36198,19.79688 20.36198,19.79688c0,0 -8.84114,-0.78386 -19.37761,1.65885c0.71094,1.64063 1.05729,3.28125 1.20313,4.92188c2.77083,-0.51042 5.6875,-0.83854 8.71354,-0.83854c26.17708,0 47.21354,11.26563 47.21354,11.26563l-8.22136,11.50261c0,0 -7.34636,-13.28907 -17.70052,-15.65886c5.46875,4.06511 11.57552,9.46094 14.76563,17.20833c-21.69271,-8.49479 -47.86979,-12.96094 -56.27344,-13.94531c-0.72917,3.09896 -0.63802,7.52864 -0.63802,7.52864c0,0 35.10938,6.47136 60.64844,21.09114c-0.16406,31.99219 -34.98177,56.52864 -39.79427,59.39063zM149.13281,139.32552c0,0 10.91927,-14.30989 10.73698,-33.25c0,0 17.59114,10.90104 17.59114,21.54687c0.01823,11.88542 -28.32813,11.70313 -28.32813,11.70313z" fill="#0288d1"></path><path d="M83.10677,174.96354c0,-2.53386 1.51302,-8.07552 3.53646,-10.99219c3.44532,-1.73177 6.36198,-3.99219 6.36198,-3.99219c0,0 -2.15104,3.24479 -2.15104,5.97917c0,0.78386 0.54688,3.80989 4.63021,4.55729c0.58333,0.10937 1.23958,0.16406 1.98698,0.16406c5.90625,0 13.39844,-4.44792 13.39844,-4.44792c-24.09896,21.60156 -27.76302,9.04167 -27.76302,8.73177zM87.00781,137.19271l-0.29167,0.1276l-0.34635,0.21875c-1.36719,0.80208 -2.64323,1.45833 -3.90104,2.04167c2.58854,2.29688 5.45052,4.39323 8.62239,5.9974c1.36719,-0.65625 3.44532,-1.67708 4.41146,-2.16927c-3.11719,-1.71354 -5.94271,-3.82813 -8.49479,-6.21614zM62.54427,149.53386c-1.95052,-1.75 -2.55208,-4.22917 -2.55208,-6.59896c0,-9.57031 7.61979,-22.93229 7.61979,-22.93229l-3.73698,-7.78386c0,0 -18.88542,28.40104 -1.38542,37.44271c0.03646,-0.10938 0.01823,-0.01823 0.05469,-0.12761zM177.16927,129.66406c-3.0625,9.82552 -28.03646,9.66146 -28.03646,9.66146l-4.73958,7.36458c0,0 1.40365,0.03646 11.06511,0.05469c0,0 19.79688,-1.6224 21.71094,-17.08073zM155.07552,114.07813c0,0 1.60417,1.03906 3.73698,2.67968c0.67448,-3.33593 1.09375,-6.90885 1.05729,-10.68229c-0.92969,-0.56511 -3.29948,-1.82292 -4.17448,-2.22396c-0.01823,3.39063 -0.49218,6.67188 -1.22135,9.88021c0.23698,0.14583 0.45573,0.27344 0.60156,0.34636zM179.66667,92.11198c0,5.17708 -4.94011,5.1224 -4.94011,5.1224l-3.73698,0.23698l6.67188,3.44531c21.54687,-0.875 4.97656,-17.64583 -9.84375,-22.09375c1.18489,0.69271 11.84896,7.36458 11.84896,13.28906zM102.35677,39.77604c0.83854,-0.27344 1.69531,-0.45573 2.625,-0.45573c8.38542,0 20.36198,19.79688 20.36198,19.79688l8.47656,0.52864c0,0 -1.95052,-4.08333 -5.17708,-9.66146c-4.42968,-7.18229 -18.08333,-14.32813 -26.28646,-10.20833zM70.875,47.54167c-8.3125,-0.875 -8.05729,23.04167 -7.23698,25.66667c0,-13.89062 5.43229,-17.84636 8.09375,-17.84636c2.66146,0 5.28646,3.24479 6.36198,5.9974c-0.12761,-4.10157 -0.10937,-7.60157 -0.10937,-7.60157c0,0 -3.31771,-5.48698 -7.10938,-6.21614zM100.60677,66.86458c0.03646,-1.62239 0.20052,-3.15364 0.38281,-4.66667c-3.26302,1.05729 -6.5625,2.42448 -9.71614,4.28386c-0.09114,1.95052 0.01823,2.84375 -0.09114,4.83073c2.60677,-1.40364 5.83333,-2.77083 9.40625,-3.91927c0,-0.16406 0,-0.32812 0.01823,-0.52864zM115.79167,163.22396c-3.59114,-6.28907 -6.25261,-12.85157 -6.25261,-12.85157c0,0 14.76563,0.96615 22.69532,-7.23698c-4.95833,2.22396 -12.81511,5.03125 -22.00261,4.17448c-1.93229,2.02344 -4.26563,3.57292 -6.78125,5.25l7.32812,13.67188c0,0 5.57813,-3.35417 5.01302,-3.00781zM119.80208,171.59114c1.13021,-0.69271 -3.88281,2.97136 -3.88281,2.97136c0,0 11.52083,19.17708 21.38281,27.96354c2.66146,2.36979 5.19531,2.80729 5.19531,2.80729c0,0 -9.78906,-9.44271 -22.69531,-33.74219zM185.57292,32.06511c-1.38542,9.11458 -16.22396,35.36458 -17.39062,36.25781c-1.45833,-0.67448 4.41146,1.91406 4.41146,1.91406c0,0 3.19011,-6.78125 5.19531,-10.59114c1.98698,-3.79167 7.61979,-18.59375 8.11198,-21.98437c0.63802,-4.2474 -0.32813,-5.59636 -0.32813,-5.59636zM163.11458,76.125l-8.23958,11.50261c0,0 -7.34636,-13.28906 -17.70052,-15.65886c4.17448,3.11719 8.71354,7 12.04948,12.10417c0,-0.01823 0,-0.05469 0,-0.07292c0,0.01823 0,0.05469 0,0.07292c1.03906,1.58594 1.96875,3.26302 2.71614,5.10417c4.28386,1.62239 8.00261,2.95313 7.63802,2.80729c0,0 6.67187,-10.28125 8.23958,-13.16146c0.4375,0.25521 -4.70313,-2.69792 -4.70313,-2.69792zM79.04167,73.79167l-15.40364,-0.58333c-0.25521,1.54948 -0.16406,3.99219 -0.16406,5.57813l14.20052,1.36718c0,0 -5.32292,9.46094 -5.32292,16.44271c0,6.96354 0.82032,7.32812 0.82032,7.32812c0,0 -0.14583,-10.79167 6.10677,-19.88802c-0.01823,-0.01823 -0.03646,-0.01823 -0.03646,-0.03646c0.01823,0.01823 0.03646,0.03646 0.03646,0.03646c0.38282,-0.54688 0.78386,-1.09375 1.22136,-1.64063c-0.98437,-5.48698 -1.45833,-8.60417 -1.45833,-8.60417zM53.92188,73.48177c-18.57552,0 -34.21615,4.15625 -35.61979,4.55729c-1.40364,0.40104 -5.79687,3.59115 -3.79167,3.19011c4.06511,-1.29427 23.16927,-4.24739 39.88542,-2.78906c-0.34636,-3.09896 -0.47396,-4.95833 -0.47396,-4.95833z" fill="#01579b"></path></g></g></g></svg><!--<img class="alignright img-responsive" src="images/landing/responsive.png" alt="">-->	
                </div>
                <div class="divider"><i class="icon-circle"></i></div>
                <div class="clear"></div>
                                        
                         
            <!--SECTION 2 PICTURE/DESC-->                                        
                <div class="col_one_third center align-items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                    width="250" height="250"
                    viewBox="0 0 224 224"
                    style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g id="Layer_1"><g><path d="M32.66667,112c0,43.86667 35.46667,79.33333 79.33333,79.33333c43.86667,0 79.33333,-35.46667 79.33333,-79.33333c0,-43.86667 -35.46667,-79.33333 -79.33333,-79.33333c-43.86667,0 -79.33333,35.46667 -79.33333,79.33333" fill="#ffffff"></path><path d="M112,205.33333c-51.33333,0 -93.33333,-42 -93.33333,-93.33333c0,-51.33333 42,-93.33333 93.33333,-93.33333c21.93333,0 43.4,7.93333 60.2,22.4l-12.13333,14c-13.06667,-11.66667 -30.33333,-17.73333 -48.06667,-17.73333c-41.06667,0 -74.66667,33.6 -74.66667,74.66667c0,41.06667 33.6,74.66667 74.66667,74.66667z" fill="#dcedc8"></path><path d="M112,205.33333c-51.33333,0 -93.33333,-42 -93.33333,-93.33333c0,-21.93333 7.93333,-43.4 22.4,-60.2l14,12.13333c-11.66667,13.06667 -17.73333,30.33333 -17.73333,48.06667c0,41.06667 33.6,74.66667 74.66667,74.66667c41.06667,0 74.66667,-33.6 74.66667,-74.66667c0,-41.06667 -33.6,-74.66667 -74.66667,-74.66667v-18.66667c51.33333,0 93.33333,42 93.33333,93.33333c0,51.33333 -42,93.33333 -93.33333,93.33333z" fill="#7cb342"></path><path d="M126,56l-37.33333,-28l37.33333,-28z" fill="#7cb342"></path></g><path d="M84,70l8.4,-4.2l23.8,44.33333l-8.4,4.2z" fill="#000000"></path><path d="M108.26667,108.26667l7.93333,7.93333l-24.26667,23.8l-7.93333,-7.93333z" fill="#000000"></path><path d="M102.66667,112c0,5.13333 4.2,9.33333 9.33333,9.33333c5.13333,0 9.33333,-4.2 9.33333,-9.33333c0,-5.13333 -4.2,-9.33333 -9.33333,-9.33333c-5.13333,0 -9.33333,4.2 -9.33333,9.33333" fill="#000000"></path><path d="M107.33333,112c0,2.8 1.86667,4.66667 4.66667,4.66667c2.8,0 4.66667,-1.86667 4.66667,-4.66667c0,-2.8 -1.86667,-4.66667 -4.66667,-4.66667c-2.8,0 -4.66667,1.86667 -4.66667,4.66667" fill="#8bc34a"></path></g></g></svg>                    <!--<img class="alignright img-responsive" src="images/landing/responsive.png" alt="">-->	
                </div>

                <div class="col_two_third  col_last">
                        <h3 >History of the Company</h3>
                        <p align="justify">
We started recently in 2017 with a partnership with some big names in the telecommunication world. The companies had a successful run, after which, we decided to partner up with those brands. 
True success stops at nothing; therefore, we continued on the winning path to achieve both short-term and long-term goals. As the customers grew in number, it was one of our demands to expand the number of brands. Thereupon, we went on to explore further options and came up with this website. Customers are the number one priority, and profits come later. We now have frontline brands. It takes constant effort to be helpful to users, transparent on the selling market, and of more value to our partners.
                        </p>
                </div>                                        
                <!--<div class="divider"><i class="icon-circle"></i></div>-->

  </div></div>
    
        <div class="section nomargin">
                <div class="container clearfix">

                        <div class="col_one_fourth nobottommargin center" data-animate="bounceIn">
                                <i class="i-plain i-xlarge divcenter icon-line2-directions"></i>
                                <div class="counter counter-lined"><span data-from="0" data-to="3" data-refresh-interval="1" data-speed="500"></span>+</div>
                                <h5>Companies on board</h5>
                        </div>

                        <div class="col_one_fourth nobottommargin center" data-animate="bounceIn" data-delay="300">
                                <i class="i-plain i-xlarge divcenter nobottommargin icon-line2-graph"></i>
                                <div class="counter counter-lined"><span data-from="3000" data-to="15360" data-refresh-interval="100" data-speed="2500"></span>+</div>
                                <h5>Happy Customers</h5>
                        </div>

                        <div class="col_one_fourth nobottommargin center" data-animate="bounceIn" data-delay="900">
                                <i class="i-plain i-xlarge divcenter nobottommargin icon-line2-layers"></i>
                                <div class="counter counter-lined"><span data-from="0" data-to="50" data-refresh-interval="5" data-speed="2000"></span>*</div>
                                <h5>No. of Bundles</h5>
                        </div>

                        <div class="col_one_fourth nobottommargin center col_last" data-animate="bounceIn" data-delay="1200">
                                <i class="i-plain i-xlarge divcenter nobottommargin icon-line2-clock"></i>
                                <div class="counter counter-lined"><span data-from="0"data-to="24" data-refresh-interval="2" data-speed="1000"></span>+</div>
                                <h5>Hours Avaiable</h5>
                        </div>
                </div>
        </div>




                
    <div class="container clearfix">
        <div class="nobottommargin topmargin">                  
            <!--SECTION 3 DESC/PICTURE-->
                <div class="col_two_third">
                        <h3>Meet the Team</h3>
                        <p align="justify">
We are a team of developers, content creators, web designers, and digital marketers who work in conformity to meet 100% customer satisfaction level. The developers maintain the website so that you have a seamless browsing experience. Designers come up with staggering designs supported by content writers to guide users to their next cable TV and Internet destination. To maintain such a huge range of data requires us to never lose focus. If we lose focus, the customer suffers the most. Vigilance is the key as we stay on our toes until everything is in place. When you see a bunch of men and women with a passion for entertainment, news, and views, you may see us in their reflection. The honest opinions drive us every day one step closer to the purpose as we go guns blazing in its pursuit.
                        </p>
                </div>
                <div class="col_one_third col_last center align-items-center">
                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                width="250" height="250"
                viewBox="0 0 224 224"
                style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g id="Layer_1"><path d="M74.66667,168c0,0 -10.332,-18.66667 -37.33333,-18.66667c-27.00133,0 -37.33333,18.66667 -37.33333,18.66667v14h74.66667z" fill="#455a64"></path><path d="M224,168c0,0 -10.332,-18.66667 -37.33333,-18.66667c-27.00133,0 -37.33333,18.66667 -37.33333,18.66667v14h74.66667z" fill="#455a64"></path><path d="M46.67133,135.33333h-18.66667v14c0,0 -0.57867,9.33333 9.142,9.33333c9.72067,0 9.52467,-9.33333 9.52467,-9.33333z" fill="#ff9800"></path><path d="M196.00467,135.33333h-18.66667v14c0,0 -0.57867,9.33333 9.142,9.33333c9.72067,0 9.52467,-9.33333 9.52467,-9.33333z" fill="#ff9800"></path><path d="M37.33333,144.66667c-12.87067,0 -23.33333,-10.46733 -23.33333,-23.33333c0,-12.866 10.46267,-23.33333 23.33333,-23.33333c12.87067,0 23.33333,10.46733 23.33333,23.33333c0,12.866 -10.46267,23.33333 -23.33333,23.33333" fill="#ffb74d"></path><path d="M186.66667,144.66667c-12.87067,0 -23.33333,-10.46733 -23.33333,-23.33333c0,-12.866 10.46267,-23.33333 23.33333,-23.33333c12.87067,0 23.33333,10.46733 23.33333,23.33333c0,12.866 -10.46267,23.33333 -23.33333,23.33333" fill="#ffb74d"></path><path d="M36.31133,88.66667c-11.37267,1.022 -22.31133,9.00667 -22.31133,26.012v6.65467l4.66667,4.66667v-14l29.44667,-10.27133l7.88667,10.27133v14l4.66667,-4.66667v-5.60933c0,-11.26533 -0.63,-19.628 -14,-22.39067l-2.26333,-4.66667z" fill="#424242"></path><path d="M185.64467,88.66667c-11.37267,1.022 -22.31133,9.00667 -22.31133,26.012v6.65467l4.66667,4.66667v-14l29.44667,-10.27133l7.88667,10.27133v14l4.66667,-4.66667v-5.60933c0,-11.26533 -0.63,-19.628 -14,-22.39067l-2.26333,-4.66667z" fill="#424242"></path><path d="M74.66667,95.66667c0,-3.73333 3.26667,-7 7,-7c3.73333,0 7,3.26667 7,7c0,3.73333 -3.26667,7 -7,7c-3.73333,0 -7,-3.26667 -7,-7M135.33333,95.66667c0,3.73333 3.26667,7 7,7c3.73333,0 7,-3.26667 7,-7c0,-3.73333 -3.26667,-7 -7,-7c-3.73333,0 -7,3.26667 -7,7" fill="#ffa726"></path><path d="M93.33333,135.33333v-18.66667h37.33333v18.66667c0,0 0,18.66667 -18.66667,18.66667c-18.66667,0 -18.66667,-18.66667 -18.66667,-18.66667z" fill="#ff9800"></path><path d="M79.33333,98.46667c0,17.73333 14.46667,32.2 32.66667,32.2c18.2,0 32.66667,-14.46667 32.66667,-32.2v-20.53333c0,-17.73333 -65.33333,-27.53333 -65.33333,0z" fill="#ffb74d"></path><path d="M100.8,42l-3.73333,7.46667c-18.66667,3.73333 -22.4,20.53333 -22.4,35.46667v3.73333l9.33333,9.33333v-18.66667l13.06667,-14l42.93333,14v18.66667l9.33333,-9.33333v-8.4c0,-18.2 -14.46667,-38.26667 -37.33333,-38.26667z" fill="#424242"></path><path d="M102.66667,93.33333c0,2.8 -1.86667,4.66667 -4.66667,4.66667c-2.8,0 -4.66667,-1.86667 -4.66667,-4.66667c0,-2.8 1.86667,-4.66667 4.66667,-4.66667c2.8,0 4.66667,1.86667 4.66667,4.66667M130.66667,93.33333c0,-2.8 -1.86667,-4.66667 -4.66667,-4.66667c-2.8,0 -4.66667,1.86667 -4.66667,4.66667c0,2.8 1.86667,4.66667 4.66667,4.66667c2.8,0 4.66667,-1.86667 4.66667,-4.66667" fill="#784719"></path><path d="M94.73333,135.33333c0,0 -38.73333,7.46667 -38.73333,46.66667h112c0,-39.66667 -37.33333,-46.66667 -37.33333,-46.66667l-18.66667,9.33333l-18.66667,-9.33333" fill="#78909c"></path><path d="M130.66667,135.33333l-18.66667,4.66667l-18.66667,-4.66667l18.66667,42z" fill="#ffffff"></path><path d="M121.33333,149.33333l-9.33333,-9.33333l-9.33333,9.33333l4.66667,4.66667l-1.4,9.8l6.06667,13.53333l6.06667,-13.53333l-1.4,-9.8z" fill="#d32f2f"></path></g></g></svg>                    <!--<img class="alignright img-responsive" src="images/landing/responsive.png" alt="">-->	
                </div>
            </div>
        </div>
    <div class="clear-bottommargin"></div>

</section><!-- #content end -->
                


<!-- PROMOBOX START -->                
<div class="section clear-bottommargin-sm" style="background-color: #EBA74B">
    <div class="container">
        <div class="row center justify-content-center">
                <div class="col-md-auto">
                        <h1 class="t700 text-white mb-3">Everything About Us Is About You</h1>
                        <!--<h4 class="text-white">We provide you with plenty of information about the epic TV-services, So that, you make an educated choice.</h4>-->
                </div>
<!--            <div class="subscribe-widget clear-bottommargin align-items-center center">
                <div class="widget-subscribe-form-result"></div>
                <form id="zipform2" action="include/subscribe.php" method="get">
                    <input class="input-number–noSpinners form-control-lg float-left" type="text" name="zipcode" placeholder="Enter Zip Code">
                    <a style="margin-top:1px;" href="javascript:{}" onclick="document.getElementById('zipform2').submit();" class="button button-rounded button-reveal button-large button-dirtygreen"><i class="icon-map-marker2"></i><span>Find Area</span></a>
                </form>
            </div>-->

        </div>
    </div>
</div>
<!-- PROMOBOX END   --> 


<!-- Footer
============================================= -->
<?php require_once 'views/footer.php'; ?>