<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,400i,700|Istok+Web:400,700" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="style.css" type="text/css" />

	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="css/components/ion.rangeslider.css" type="text/css" />

	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<meta name='viewport' content='initial-scale=1, viewport-fit=cover'>

	<!-- Hosting Demo Specific Stylesheet -->
	<link rel="stylesheet" href="css/colors.php?color=44aaac" type="text/css" /> <!-- Theme Color -->
	<link rel="stylesheet" href="demos/hosting/css/fonts.css" type="text/css" />
	<link rel="stylesheet" href="demos/hosting/hosting.css" type="text/css" />
        
        
	<!-- / -->

	<!-- Document Title
	============================================= -->
	<title>Cable | Privacy Policy</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar
		============================================= -->
<!--		<div id="top-bar" class="center dark" style="background-color: #15888a">
			<p class="mb-0 text-white" style="font-size: 14px;">Holisticly cultivate multifunctional quality vectors after Mobile SDK.<a href="#" class="ml-2 font-primary t700 text-white"><u>Learn More</u> &#8250;</a></p>
		</div>-->

		<!-- Header
		============================================= -->
		<header id="header">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="index.php" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="images/logo.png" alt="Canvas Logo"></a>
						<a href="index.php" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="images/logo@2x.png" alt="Canvas Logo"></a>
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu" class="sub-title">

						<ul>
							<li><a href="index.php"><div>Home</div><span>Lets Start</span></a></li>
							<li><a href="best-providers.php"><div>Providers</div><span>Awesome deals</span></a></li>
                                                        <li><a href="about-us.php"><div>About us</div><span>what we do</span></a></li>
						</ul>

					</nav><!-- #primary-menu end -->

				</div>

			</div>

		</header>
                

		<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-parallax page-title-dark" style="padding: 250px 0; background-image: url('images/later/privacy.jpg'); background-size: cover; background-position: center center;" data-bottom-top="background-position:0px 400px;" data-top-bottom="background-position:0px -500px;">

			<div class="container clearfix">
                            <h1><strong>Privacy Policy</strong></h1>
                            <h3 class="text-white">Nothing to hide and everything to offer</h3>
<!--				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Pages</a></li>
					<li class="breadcrumb-item active" aria-current="page">About Us</li>
				</ol>-->
			</div>
                    
		</section><!-- #page-title end -->


                
		<!-- Content
		============================================= -->
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            
            <div class="heading-block">
                <h3 class="center">Privacy Policy</h3><br>
                <!--<p align="justify">-->

                    This privacy policy explains the privacy practices for CableAgency.com. It applies solely to information collected by this website. It holds the following information in black and white.
                <br>
                <br>
                    <div class="acc_content clearfix">
                        <ol class="iconlist-color nobottommargin">   
                            <li>What personally identifiable information is collected from you on this website, how is it utilized and to whom it may be shared with.</li>
                            <li>What choices do you have regarding the use of data?</li>
                            <li>The security procedures to protect your personal details such as credit card information.</li>
                            <li>How you can choose not to receive any promotional offers?</li>
                        </ol>
                    </div>
                    
                     
                    
                <!--</p>-->
            </div>
            <div id="side-navigation" class="tabs customjs">
                <div class="col_one_third nobottommargin">
                    <ul class="sidenav">
                        <li><a href="#snav-content1"><i class="icon-line-layout"></i>When You Visit the Site<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content2"><i class="icon-data"></i>Data Collection Process<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content3"><i class="icon-info-sign"></i>Type of Information<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content4"><i class="icon-lock3"></i>Security Provision<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content5"><i class="icon-question-sign"></i>Customer Info Usage<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content6"><i class="icon-remove"></i>No More Promotional Offers!<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content7"><i class="icon-banknote"></i>Sales & Marketing<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content8"><i class="icon-user4"></i>icon-newspaper<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content9"><i class="icon-line-paper"></i>Privacy Policy Updates<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content10"><i class="icon-line2-info"></i>Access Your Information<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content11"><i class="icon-map-marker2"></i>Statewise Exception<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content12"><i class="icon-group"></i>Customers in Washington<i class="icon-chevron-right"></i></a></li>
                    </ul>
                </div>

                <div class="col_two_third col_last nobottommargin">
                    
                    <div id="snav-content1">
                        <h3>When You Visit the Site</h3>
                        <p align="justify">
                            When a person visits a website, there is certain information that the site uses and stores on their behalf. If you want to have a better idea about that information, reach us at privacy@CableAgency.com. We will try to answer all your questions to the best of our ability. 
                        </p>
                    </div>

                    <div id="snav-content2">
                        <h3>Data Collection Process</h3>
                        <p align="justify">
                            We gather your information as you browse through the site. The collection occurs when you fill out online forms, contact customer support, request information/make the call and talk to our call center agents. The cookies and pixel tags are able to track your usage of the website. Information such as address-suite/level, phone number, and credit card data when you submit online order forms helps us process your orders. 
                            Furthermore, when you are in a live call with a customer care agent, the necessary information that you give to the agent on account of your purchase, the website saves it to meet the order requirements. Your personal information also helps us in maintaining the high service quality and assistive support services.
                        </p>
                    </div>

                    <div id="snav-content3">
                        <h3>Type of Information</h3>
                        <p align="justify">                        
                            We as a website collect many types of customer data. Below you’ll find a list that explains them in detail.
                        </p>
                    <div class="acc_content clearfix">
                        <ul class="iconlist iconlist-color nobottommargin">   
                            <li><i class="icon-plus-sign"></i><strong>Customer Contact Details </strong> – Customer information is the data that helps us get in touch with you to complete an order. Such data includes your name, address, fax number, email address, and phone number.</li>
                            <li><i class="icon-plus-sign"></i><strong>Personal Data </strong> – It is the kind of data that includes social security number, credit/debit card number, and bank ID to process an order in case you buy a service.</li>
                            <li><i class="icon-plus-sign"></i><strong>Customer Order Details </strong> – The order details include information such as your address, type of service, package name, phone number, fax number, as well as billing and shipping information.</li>
                            <li><i class="icon-plus-sign"></i><strong>Demographic Details </strong> – This category includes things like your age, income, properties in your name, family size. Such data is already public and helps us pitch to you better offers in terms of products and/or services in the future.</li>
                            <li><i class="icon-plus-sign"></i><strong>Website Use Details </strong> – We collect website use details through third-party cookies giving us an idea of how you navigate the website and what web page is most useful to you. </li>
                            <li><i class="icon-plus-sign"></i><strong>Proprietary Controls </strong> – The site uses Google Analytics to improve customer experience and enhance usability. We may record the amount of time you spend on our website, clicks on buttons, browser version, operating system, installed extensions, and other non-personally identifiable information. If you disable JavaScript or elect to use the private browser option, it will stop us to record all the above information. However, doing so may also disengage some of the site’s features.</li>
                        </ul>
                    </div>
                    </div>

                    <div id="snav-content4">
                        <h3>Security Provision</h3>
                        <p align="justify">
                            CableAgency.com implements modern encryption techniques to protect information stored on the servers from unauthorized access. The security technology couples with vigilance because of procedural safeguards. The employees or those associated with them on behalf of your order agree not to disclose any of your data to outsiders. We value your privacy.
                            We try to protect customer information that you deliver to us through customer support agents and the Internet. 
                        </p>
                    </div>

                    <div id="snav-content5">
                        <h3>Customer Info Usage</h3>
                        <p align="justify">
                            All the general and personal information that you provide goes into order processing, completing request inquiries and giving you the services, you choose to buy. Therefore, every inch of data that you submit to the site goes into improving customer experience in the present and in the future. We may pitch the information to third parties to complete the purchase process. 
                            In addition, the data may be useful to us in the form of showing you discounts and promotions in the future. We want to be right on the money, and it never goes wrong when we are aware of some part of your life. The points below disclose situations in which we may share your personal information with the third parties. 
                        </p>
                        
                        <div class="acc_content clearfix">
                            <ul class="iconlist iconlist-color nobottommargin">   
                                <li><i class="icon-plus-sign"></i><strong>With Third-Party Product or Service Providers </strong> – We share your information directly with the products and services providers, as needed. We are a third-party promoter and advertiser of various brands and services. The relevant information helps them process online orders conclusively. Once we pass on your information to them, your data enters the jurisdiction of their own privacy policy.</li>
                                <li><i class="icon-plus-sign"></i><strong>With Third-Party Promoters </strong>  – The marketers working on behalf of the company may get a hold of your personal information. It allows them to present mouth-watering deals and discounts to you to optimize your purchasing cycle. However, the way they use your information depends on their own privacy policies and regulations. You may opt out of it by following steps in the next segment.</li>
                                <li><i class="icon-plus-sign"></i><strong>With Third-Party Cookies </strong>  – We give permission to third parties to collect random information when you visit our website so that you may see the ads of our products and/or services when you visit other sites. The third parties may use your current browse history, habits, clicked buttons, time spent on a web page, browser type, navigational history to pitch creative ads to you. It may just grab your attention and persuade you to buy more products and/or services in the future. </li>
                            </ul>
                            
                        </div>
                        <p align="justify">These third parties may use a cookie or a beacon, or any other technology to collect your data.</p>                                    
                    </div>
                        
                    <div id="snav-content6">
                        <h3>No More Promotional Offers!</h3>
                        <div class="acc_content clearfix">
                            <ul class="iconlist iconlist-color nobottommargin">   
                                <li><i class="icon-plus-sign"></i>You may shoot us an email at privacy@CableAgency.com to stop receiving promotional messages and ad copies designed to persuade you to buy products and/or services.</li>
                                <li><i class="icon-plus-sign"></i>To opt out of the third party cookie option storing your information, again write us an email at privacy@CableAgency.com. </li>
                            </ul>
                            
                        </div>
                    </div>

                    <div id="snav-content7">
                        <h3>Sales & Marketing</h3>
                        <p align="justify">
                            <strong>IMPORTANT</strong> – By leaving your contact details such as your home/office address, phone number on the website, you grant us the permission to contact you even if you may have previously added your number to any DO-NOT-CALL list maintained by us or any state/federal government agency. Additionally, there may be a restriction on us calling you on the smartphone or landline under the law of the specific state. Those of you with mobile as the primary mode of communication can give us a call on our toll-free number in the US and Canada. Moreover, you give us the permission to call or text you at the number you provided. It can be by the use of automatic dialing technology or a prerecorded message. We don’t require your consent to sell you our products and/or services. 
                        </p>
                    </div>

                    <div id="snav-content8">
                        <h3>Arbitration Arrangement</h3>
                        <p align="justify">
                            If we find any misuse of the website on your behalf and it includes but not limited to the Telephone Consumer Protection Act (TCPA), is subject to binding arbitration rather than the court of law.                        </p>
                    </div>

                    <div id="snav-content9">
                        <h3>Privacy Policy Updates</h3>
                        <p align="justify">
                            We will update the privacy policy as per our business requirements with the passage of time. If there is a change in how we use customer information, we will highlight it on the website and keep you and the state/government agencies in the loop. 
                        </p>
                    </div>


                    <div id="snav-content10">
                        <h3>Access Your Information</h3>
                        <p align="justify">
                            For any questions that you may have about customer information that we have or you want to gain access to that information, please email us at privacy@CableAgency.com. You may also write a traditional mail to the mailing address as a second option.
                            We may charge you with a nominal fee to access that information stored on our files. Nevertheless, we will inform you about the charges before proceeding with the request.
                            If you believe the customer information we have is incorrect or incomplete, CableAgency.com will correct and complete it upon verification of this error by the owner of that information.
                        </p>
                    </div>
                    
                    
                    <div id="snav-content11">
                        <h3>Statewise Exception</h3>
                        <p align="justify">
                            If you are a resident of California or Delaware, in addition to the above-mentioned rights, you have the right to request information from us and ask the manner in which we share certain parts of your information to third parties. They use it to market products and/or services in a more user-friendly way.
                            The two states allow you to request the following information:
                        <ol>
                            <li>The information that we shared with the third parties for product and/or service promotions in the previous year.</li>
                            <li>The contact details of such third parties to which we shared your information with.</li>
                            <li>If a third party's name doesn’t explain the nature of the business, you can request to see some instances of their products and/or services on the web.</li>
                            <li>You have the right to receive data in a standardized format and it is not addressed to you, specifically. To gain rights of your data, the corresponding email address is privacy@CableAgency.com.</li>   
                        </ol>
                        </p>
                    </div>
                    
                    
                    <div id="snav-content12">
                        <h3>Customers in Washington</h3>
                        <p align="justify">
                            Sales tax or use tax applies to any product sold to people in the Washington State. The retailer may or may not gather sales tax on the purchase, in which case, the purchaser needs to file a tax return. If the retailer doesn’t include the tax within the purchase, it is their duty to inform the department and the purchaser about the liable sales tax on that purchase. 
                            The notice sets forth under the law L.2017 Section 205(3). To learn more about how to remit sales tax, you can visit Washington State Department of Revenue’s website (https://dor.wa.gov).
                            <br>
                            <span style="font-size: 10px;">Privacy Policy Effective Since December 28, 2018</span>
                        </p>

                    </div>                
                </div>
            </div>
        </div>
    </div>
</section><!-- #content end -->                

                
<!-- PROMOBOX START -->                
<div class="section clear-bottommargin-sm" style="background-color: #44AAAC">
    <div class="container">
        <div class="row center justify-content-center">
            <div class="col-md-auto">
                <h1 class="t700 text-white mb-3">Your data is safe with us!</h1>
            </div>
        </div>
    </div>
</div>
<!-- PROMOBOX END   --> 


<!-- Footer
============================================= -->
<?php require_once 'views/footer.php'; ?>

<script>
        $(function() {
                $( "#side-navigation" ).tabs({ show: { effect: "fade", duration: 400 } });
        });
</script>